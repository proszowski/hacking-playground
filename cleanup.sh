#!/bin/bash

docker kill $(docker ps -q)
docker rm $(docker ps -q)
yes | docker system prune
fuser 8080/tcp -k
fuser 8888/tcp -k
