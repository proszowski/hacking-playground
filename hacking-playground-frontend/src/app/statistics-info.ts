import { StatisticsChunk } from './statistics-chunk';

export class StatisticsInfo implements StatisticsChunk {
	executionDate: string;
	headers: {};
	uri: string;
	method: string;
	queryString: string;
	parameters: {};
	body: string;

	timestamp(): Date {
		return new Date(this.executionDate);
	}
}
