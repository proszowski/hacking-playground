import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChallengesComponent } from './challenges/challenges.component'
import { MainPageComponent } from './main-page/main-page.component'
import { AddDockerChallengeComponent } from './add-docker-challenge/add-docker-challenge.component'
import { AddChallengeComponent } from './add-challenge/add-challenge.component'
import { UsersRankingComponent } from './users-ranking/users-ranking.component'
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { StatisticsComponent } from './statistics/statistics.component';

const routes: Routes = [
	{ path: '', component: MainPageComponent },
	{ path: 'challenges', component: ChallengesComponent, canActivate: [AuthGuard] },
	{ path: 'add-challenge', component: AddChallengeComponent, canActivate: [AuthGuard] },
	{ path: 'users-ranking', component: UsersRankingComponent, canActivate: [AuthGuard] },
	{ path: 'statistics', children: [
		{ path: '', redirectTo: '', pathMatch: 'full'},
		{ path: ':id', component: StatisticsComponent}
	] },
	{ path: 'login', component: LoginComponent },
   { path: 'register', component: RegisterComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
