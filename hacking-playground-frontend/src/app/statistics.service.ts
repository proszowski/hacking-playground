import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Statistics } from './statistics';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

	constructor(private http: HttpClient) {}

	statisticsForGame(id: string): Observable<Statistics> {
			  return this.http.get<Statistics>('/statistics/' + id)
	}
}
