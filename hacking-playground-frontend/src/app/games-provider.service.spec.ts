import { TestBed } from '@angular/core/testing';

import { GamesProviderService } from './games-provider.service';

describe('GamesProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GamesProviderService = TestBed.get(GamesProviderService);
    expect(service).toBeTruthy();
  });
});
