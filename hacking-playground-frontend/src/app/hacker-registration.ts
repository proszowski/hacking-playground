export interface HackerRegistration {
	email: string,
	name: string,
	password: string
}
