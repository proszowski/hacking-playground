import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Hacker } from './hacker';
import { AuthenticationService } from './auth/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hacking-playground-frontend';
  currentHacker: Hacker;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.authenticationService.currentHacker.subscribe(x => this.currentHacker = x);
    }

}
