﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

import { HackerRegistration } from '../hacker-registration';
import { Hacker } from '../hacker';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentHackerSubject: BehaviorSubject<Hacker>;
    public currentHacker: Observable<Hacker>;

	 constructor(private http: HttpClient,
        			 private router: Router) {
        this.currentHackerSubject = new BehaviorSubject<Hacker>(JSON.parse(localStorage.getItem('currentHacker')));
        this.currentHacker = this.currentHackerSubject.asObservable();
    }

	 public get currentHackerValue(): Hacker {
        return this.currentHackerSubject.value;
    }

	 register(hackerRegistration: HackerRegistration): Observable<void>{
			return this.http.post<any>('/auth/register', hackerRegistration)
	 }

	 login(hackerName, password): Observable<Observable<Hacker>> {

				const httpOptions = {
						  observe: 'response' as 'body'
				};

				let userLogin: UserLogin = { username: hackerName, password: password };
				return this.http.post<any>('/login', userLogin, httpOptions)
            .pipe(map(response => {
					 let token = response.headers.get('Authorization');
					return token;
				}))
				.pipe(token => {
					 return token.pipe(
						map(t => {
					 		let httpHeaders = new HttpHeaders({
								'Authorization': t
					 		});
					 		return this.http.get<any>('/auth/currentUser', {headers: httpHeaders }).pipe(
										map(response => {
										  		let hacker: Hacker = {
													 		id: response.id,
													 		name: response.name,
													 		token: t
										  		};
                					  		// store user details and jwt token in local storage to keep user logged in between page refreshes
										  		localStorage.setItem('currentHacker', JSON.stringify(hacker));
										  		this.currentHackerSubject.next(hacker);
										  		return hacker;
										}));
		
								})
					 );
				});

	 }

    logout() {
        localStorage.removeItem('currentHacker');
        this.currentHackerSubject.next(null);
		  this.router.navigate(['/login']);
    }
}

interface UserLogin {
	username: string;
	password: string;
}
