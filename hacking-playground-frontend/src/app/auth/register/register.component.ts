import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import { AlertService } from '../../alert.service';
import { HackerRegistration } from '../../hacker-registration';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  loading = false;

  constructor(private authenticationService: AuthenticationService,
				  private alertService: AlertService,
				  private router: Router) { }

  ngOnInit() {
  }

  email = new FormControl('', [Validators.required, Validators.email]);
  hackerName = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required, Validators.minLength(8)]);

  getErrorMessageForEmail(): string {
	return this.email.hasError('required') ? 'You must enter an email'
			 : this.email.hasError('email')	? 'Invalid email' : '';
  }

  getErrorMessageForHackerName(): string {
  	 return this.hackerName.hasError('required') ? 'You must enter a hacker name' : '';
  }

  getErrorMessageForPassword(): string {
	 return this.password.hasError('required') ? 'You must enter a password'
			  : 'Password has to have at least 8 characters';
  }

  register(){
	 this.email.markAsTouched();
	 this.hackerName.markAsTouched();
	 this.password.markAsTouched();
	 if(this.email.invalid || this.hackerName.invalid || this.password.invalid || this.loading){
		return;
	 }
	 this.loading = true;
	 const hackerRegistration: HackerRegistration = {
				email: this.email.value,
				name: this.hackerName.value,
				password: this.password.value
	 };
	 this.authenticationService.register(hackerRegistration).subscribe(
		success => {
			this.alertService.success('New account has been created', true);
			this.loading = false;
		   this.router.navigate(['/login']);
		},
		e => {
			this.alertService.error(e.error);
			this.loading = false;
		}
	 );
  }

}
