import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';
import { AlertService } from '../../alert.service';

import { AuthenticationService } from '../authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loading = false;

  constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
		  private alertService: AlertService
	) {
		if (this.authenticationService.currentHackerValue) {
	 		this.router.navigate(['/']);
		}
  }

  ngOnInit() {
  }

  hackerName = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);

  getErrorMessageForHackerName(): string {
  	 return this.hackerName.hasError('required') ? 'You must enter a hacker name' : '';
  }

  getErrorMessageForPassword(): string {
  	 return this.password.hasError('required') ? 'You must enter a password' : '';
  }

  logIn(){
	 this.hackerName.markAsTouched();
	 this.password.markAsTouched();
    if(this.hackerName.invalid || this.password.invalid, this.loading){
	 	return;
	 }
	 this.loading = true;
    this.authenticationService.login(this.hackerName.value, this.password.value)
        .subscribe(
            success => {
					success.subscribe(
						s => this.router.navigateByUrl('/')
					);
            },
            error => {
					this.alertService.error('Wrong username or password', false);
            	this.loading = false;
            });
  }

}
