import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Challenge } from './challenge'
import { DockerChallengeCreation } from './docker-challenge-creation'
import { DockerComposeChallengeCreation } from './docker-compose-challenge-creation'
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChallengesProviderService {

  constructor(private http: HttpClient) {
  }

  getChallenges(): Observable<Challenge[]>{
			 //return this.mockupChallenges();
			 return this.http.get<Challenge[]>('/challenge/all') 
  }

  getChallenge(challengeId: string): Observable<Challenge>{
			 return this.http.get<Challenge>('/challenge/' + challengeId) 
  }

  mockupChallenges(): Observable<Challenge[]>{
  		const challenge = {
  		id: 'random',
  		name: 'random name',
  		description: 'random description',
		difficulty: 500,
  		tags: ['XSS', 'SQL Injection']
  		};

  		const anotherChallenge = {
  		id: 'random',
  		name: 'pretty long name becuse I really need it for testing',
  		description: 'and even longer description and even longer description and even longer description and even longer description and even longer description and even longer description and even longer description and even longer description and even longer description ',
		difficulty: 500,
  		tags: ['XSS', 'SQL Injection']
  		};
  		const challenges: Observable<Challenge[]> = of([challenge, anotherChallenge]);
  		return challenges;
  }

  addDockerChallenge(challengeCreation: DockerChallengeCreation): Observable<HttpResponse<void>>{
	 const httpOptions = { observe: 'response' as 'body' };

	 return this.http.post<HttpResponse<void>>('/challenge/new-docker', challengeCreation, httpOptions);
  }

  addDockerComposeChallenge(challengeCreation: DockerComposeChallengeCreation): Observable<HttpResponse<void>>{
	 const httpOptions = { observe: 'response' as 'body' };

	 return this.http.post<HttpResponse<void>>('/challenge/new-docker-compose', challengeCreation, httpOptions);
  }
}
