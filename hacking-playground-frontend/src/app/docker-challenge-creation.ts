export interface DockerChallengeCreation {
    challengeName: string;
    description: string;
    sourceCodeUrl: string;
    writeUpUrl: string;
    suggestedTags: string[];
    flags: string[];
    imagePort: number;
    imageName: string;
}
