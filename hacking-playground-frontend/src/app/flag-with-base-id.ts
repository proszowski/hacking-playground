export interface FlagWithBaseId {
	flag: string;
	baseId: string;
}
