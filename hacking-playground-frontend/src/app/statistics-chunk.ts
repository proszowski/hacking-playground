export interface StatisticsChunk {
    timestamp(): Date;
}