import { Component, OnInit, Input, Inject } from '@angular/core';
import { Challenge } from '../challenge'
import { GamesProviderService } from '../games-provider.service'
import { FlagsProviderService } from '../flags-provider.service'
import { ChallengesProviderService } from '../challenges-provider.service'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { AlertService } from '../alert.service';
import { Router } from '@angular/router';



export interface DialogData {
  flag: string;
  challenge: Challenge;
}

@Component({
  selector: 'app-challenge-card',
  templateUrl: './challenge-card.component.html',
  styleUrls: ['./challenge-card.component.css']
})
export class ChallengeCardComponent implements OnInit {
  @Input() challenge: Challenge;

  totalNumberOfFlags: number;
  numberOfFlagsGotByHacker: number;
  loadingGame = false;
  restartingGame = false;
  challengeNameTooLong = false;
  challengeDescriptionTooLong = false;

  constructor(private gamesProvider: GamesProviderService,
				  private flagsProvider: FlagsProviderService,
				  private challengesProvider: ChallengesProviderService,
				  private alertService: AlertService,
				  public dialog: MatDialog,
				  private router: Router) { }

  ngOnInit() {
	 this.challengeNameTooLong = this.challenge.name.length > 35;
	 this.challengeDescriptionTooLong = this.challenge.description.length > 80;
	this.flagsProvider.howMuchFlags(this.challenge.id).subscribe(
			  howMuch => this.totalNumberOfFlags = howMuch
	);
  	this.flagsProvider.howMuchFlagsIHave(this.challenge.id).subscribe(
			  howMuch => this.numberOfFlagsGotByHacker = howMuch 
	);

  }

  goToLink(url: string){
   window.open(url, "_blank");
  }

  runGame(){
	this.alertService.clear();
	this.loadingGame = true;
  	this.gamesProvider.getGame(this.challenge.id)
	 	.subscribe(link => {
			this.loadingGame = false;
		 	this.goToLink(`/game/${link}`);
		},
		error => {
			this.alertService.error(`Couldn't start the game`);
		});
  }

  restartGame(){
	this.alertService.clear();
	this.restartingGame = true;
  	this.gamesProvider.getGame(this.challenge.id)
		.subscribe(link => {
			  this.gamesProvider.restartGame(link)
				  .subscribe(e => {
				  		this.restartingGame = false;
						this.alertService.success('Game restarted successfully')
				  });
		});
  }

  submitFlag(){
	this.alertService.clear();
	const dialogRef = this.dialog.open(SubmitFlagDialog, {
      width: '20vw',
		data: {flag: '', challenge: this.challenge}
    });
	 dialogRef.afterClosed().subscribe(result => {
				if(result == 'SUCCESS'){
					this.challengesProvider.getChallenge(this.challenge.id).subscribe(
						challenge => this.challenge = challenge
					);
					this.numberOfFlagsGotByHacker = this.numberOfFlagsGotByHacker + 1;
				}
	 });
  }

  showStatistics(){
  	this.gamesProvider.getGame(this.challenge.id)
		.subscribe(link => {
			this.router.navigateByUrl('/statistics/' + link);
		});
  }

}

@Component({
  selector: 'submit-flag-dialog',
  templateUrl: 'submit-flag-dialog.html',
  styleUrls: ['./challenge-card.component.css'],
})
export class SubmitFlagDialog {

  constructor(public dialogRef: MatDialogRef<SubmitFlagDialog>,
				  private alertService: AlertService,
				  @Inject(MAT_DIALOG_DATA) public data: DialogData,
				  private flagsProvider: FlagsProviderService) {}

  flag = new FormControl('', [Validators.required]);
  errorMessage: string;

  submitFlag(): void{
	 this.flag.markAsTouched();
	 if(!this.flag.valid){
		return;
	 }
	 this.flagsProvider.captureTheFlag(this.data.challenge.id, this.flag.value)
	 	.subscribe(response => {
		 	if(response.body == 'SUCCESS'){
				this.alertService.success('Flag has been submitted successfully')
    			this.dialogRef.close('SUCCESS');
		 	}else if(response.body == 'FAILURE'){
				this.errorMessage = 'Wrong flag';
				this.alertService.error('Wrong flag has been submitted')
			}else {
				this.errorMessage = "You've already submitted this flag";
				this.alertService.error("You've already submitted this flag")
			}
		});
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getErrorMessage(){
	 return this.flag.hasError('required') ? 'You must provide flag' : this.errorMessage;
  }

}
