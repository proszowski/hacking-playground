export interface DockerComposeChallengeCreation {
    challengeName: string;
    description: string;
    sourceCodeUrl: string;
    writeUpUrl: string;
    suggestedTags: string[];
    flags: string[];
    exposedPort: number;
    exposedServiceName: string;
    dockerComposeYaml: string;
}
