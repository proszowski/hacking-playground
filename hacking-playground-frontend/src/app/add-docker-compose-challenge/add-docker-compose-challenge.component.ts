import { Component, OnInit, Inject } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { FormControl, Validators } from '@angular/forms';
import { ChallengesProviderService } from '../challenges-provider.service';
import { DockerComposeChallengeCreation } from '../docker-compose-challenge-creation'
import { AlertService } from '../alert.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


export interface Tag {
  name: string;
}

export interface Flag {
  name: string;
}

export interface DialogData {
  message: string;
}

@Component({
  selector: 'app-add-docker-compose-challenge',
  templateUrl: './add-docker-compose-challenge.component.html',
  styleUrls: ['./add-docker-compose-challenge.component.css']
})
export class AddDockerComposeChallengeComponent implements OnInit {

  constructor(private challengesProvider:ChallengesProviderService,
				  private alertService: AlertService,
				  public dialog: MatDialog) { }

  ngOnInit() {
  }

  //response
  loading = false;

  //form controls
  challengeName = new FormControl('', [Validators.required]);
  description = new FormControl('', []);
  sourceCodeUrl = new FormControl('', []);
  writeUpUrl = new FormControl('', []);
  exposedServiceName = new FormControl('', [Validators.required]);
  imagePort = new FormControl('', [Validators.required, Validators.min(0), Validators.max(65535)]);

  dockerComposeYaml: string;
  submitted:boolean;

  //chips
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  tags: Tag[] = [];
  flags: Flag[] = [];

  addFlag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our flag
    if ((value || '').trim()) {
      this.flags.push({name: value.trim()});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our tag
    if ((value || '').trim()) {
      this.tags.push({name: value.trim()});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeTag(tag: Tag): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  removeFlag(flag: Flag): void {
    const index = this.flags.indexOf(flag);

    if (index >= 0) {
      this.flags.splice(index, 1);
    }
  }

  getErrorMessageForChallengeName(): string {
  	 return this.challengeName.hasError('required') ? 'You must enter a challenge name' : '';
  }

  getErrorMessageForExposedServiceName(): string {
  	 return this.exposedServiceName.hasError('required') ? 'You must enter a name of exposed service name' : '';
  }

  getErrorMessageForImagePort(): string {
	 return this.imagePort.hasError('required') ? 'You must enter an exposed port'
				: this.imagePort.hasError('min') ? 'Port must be positive'
	 		   : this.imagePort.hasError('max') ? 'Port must be equal to or lesser than 65535' : '';
  }

  getErrorMessage(): string {
	 return "You must provide a valid docker-compose.yml";
  }

  addChallenge() {
		this.submitted = true;
  		this.markAllFormControlsAsTouched();
		this.alertService.clear();
  		if(!this.allFormControlsAreValid() || this.loading == true) {
	 		return;
  		}
  		this.loading = true;
		const challengeCreation: DockerComposeChallengeCreation = {
    		challengeName: this.challengeName.value,
    		description: this.description.value,
    		sourceCodeUrl: this.sourceCodeUrl.value,
    		writeUpUrl: this.writeUpUrl.value,
			suggestedTags: this.tags.map(tag => tag.name),
			flags: this.flags.map(flag => flag.name),
    		exposedPort: this.imagePort.value,
    		exposedServiceName: this.exposedServiceName.value,
    		dockerComposeYaml: this.dockerComposeYaml
		};
  		this.challengesProvider.addDockerComposeChallenge(challengeCreation).subscribe(
			 		v => {
				 		this.loading = false;
				 		this.alertService.success(`You've successfully added a challenge!`);
			 		},
			 		e => {
				 		this.loading = false;
				 		this.alertService.error(e.error);
			 		}
  		);
  }

  allFormControlsAreValid(): boolean {
	 return this.challengeName.valid &&
	 this.description.valid &&
	 this.imagePort.valid &&
	 this.exposedServiceName.valid &&
	 this.dockerComposeYaml != undefined;
  }

  markAllFormControlsAsTouched(){
	 this.challengeName.markAsTouched();
	 this.description.markAsTouched();
	 this.sourceCodeUrl.markAsTouched();
	 this.writeUpUrl.markAsTouched();
	 this.imagePort.markAsTouched();
	 this.exposedServiceName.markAsTouched();
  }

  addDockerComposeYaml(){
    	const dialogRef = this.dialog.open(AddDockerComposeYamlDialog, {
      	width: '60vw',
      	height: '60vh',
			data: {message: this.dockerComposeYaml}
    	});

		dialogRef.afterClosed().subscribe(result => {
				  if(result != undefined && result != null){
				  	 this.dockerComposeYaml = result
				  }
		});

  }

}

@Component({
  selector: 'add-docker-compose-dialog',
  templateUrl: 'add-docker-compose-dialog.html',
  styleUrls: ['./add-docker-compose-dialog.css'],
})
export class AddDockerComposeYamlDialog {

  dockerComposeYaml = new FormControl(this.data.message, [Validators.required]);

  constructor(public dialogRef: MatDialogRef<AddDockerComposeYamlDialog>,
				  private alertService: AlertService,
				  @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  submitDockerComposeYaml(): void {
	this.dockerComposeYaml.markAsTouched();
	if(this.dockerComposeYaml.valid) {
		this.dialogRef.close(this.dockerComposeYaml.value);
	}
  }

  getErrorMessageForDockerComposeYaml(): string {
  	 return this.dockerComposeYaml.hasError('required') ? 'You must enter a docker-compose configuration' : '';
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


}

