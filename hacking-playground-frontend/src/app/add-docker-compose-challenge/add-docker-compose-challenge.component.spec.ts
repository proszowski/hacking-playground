import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDockerComposeChallengeComponent } from './add-docker-compose-challenge.component';

describe('AddDockerComposeChallengeComponent', () => {
  let component: AddDockerComposeChallengeComponent;
  let fixture: ComponentFixture<AddDockerComposeChallengeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddDockerComposeChallengeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDockerComposeChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
