import { Component, OnInit } from '@angular/core';
import { StatisticsService } from '../statistics.service'
import { ActivatedRoute } from '@angular/router';
import { Statistics } from '../statistics';
import { StatisticsChunk } from '../statistics-chunk'
import { StatisticsInfo } from '../statistics-info';
import { FlagSubmissionInfo } from '../flag-submission-info';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  panelOpenState = false;
  statisticChunks: StatisticsChunk[]

  constructor(private statisticsService: StatisticsService, private route: ActivatedRoute) { }

  ngOnInit() {

    const id = this.route.snapshot.paramMap.get('id');
    this.statisticsService.statisticsForGame(id).subscribe(statistics => {
      this.statisticChunks = this.asStatisticChunks(statistics);
      console.log(this.statisticChunks);
    });
  }


    asStatisticChunks(statistics: Statistics): StatisticsChunk[] {
        return (<StatisticsChunk[]>statistics.statisticInfos.map(a => Object.assign(new StatisticsInfo(), a)))
                .concat(statistics.flagSubmissionInfos.map(a => Object.assign(new FlagSubmissionInfo(), a)))
                .sort( (a,b) => a.timestamp().getTime() - b.timestamp().getTime())
    }

    isStatisticsInfo(statisticChunk: StatisticsChunk){
      return statisticChunk instanceof StatisticsInfo
    }

    isFlagSubmissionInfo(statisticChunk: StatisticsChunk){
      return statisticChunk instanceof FlagSubmissionInfo
    }

}
