import { Component, OnInit } from '@angular/core';
import { MatChipInputEvent } from '@angular/material/chips';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { FormControl, Validators } from '@angular/forms';
import { ChallengesProviderService } from '../challenges-provider.service';
import { DockerChallengeCreation } from '../docker-challenge-creation'
import { AlertService } from '../alert.service';



export interface Tag {
  name: string;
}

export interface Flag {
  name: string;
}

@Component({
  selector: 'app-add-docker-challenge',
  templateUrl: './add-docker-challenge.component.html',
  styleUrls: ['./add-docker-challenge.component.css']
})
export class AddDockerChallengeComponent implements OnInit {

  constructor(private challengesProvider:ChallengesProviderService,
				  private alertService: AlertService) { }

  ngOnInit() {
  }

  //response
  loading = false;

  //form controls
  challengeName = new FormControl('', [Validators.required]);
  description = new FormControl('', []);
  sourceCodeUrl = new FormControl('', []);
  writeUpUrl = new FormControl('', []);
  dockerImageName = new FormControl('', [Validators.required]);
  imagePort = new FormControl('', [Validators.required, Validators.min(0), Validators.max(65535)]);

  //chips
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  tags: Tag[] = [];
  flags: Flag[] = [];

  addFlag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our flag 
    if ((value || '').trim()) {
      this.flags.push({name: value.trim()});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  addTag(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our tag
    if ((value || '').trim()) {
      this.tags.push({name: value.trim()});
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeTag(tag: Tag): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  removeFlag(flag: Flag): void {
    const index = this.flags.indexOf(flag);

    if (index >= 0) {
      this.flags.splice(index, 1);
    }
  }

  getErrorMessageForChallengeName(): string {
  	 return this.challengeName.hasError('required') ? 'You must enter a challenge name' : '';
  }

  getErrorMessageForDockerImageName(): string {
  	 return this.dockerImageName.hasError('required') ? 'You must enter a name of docker image' : '';
  }

  getErrorMessageForImagePort(): string {
	 return this.imagePort.hasError('required') ? 'You must enter an exposed port of docker image'
				: this.imagePort.hasError('min') ? 'Port must be positive'
	 		   : this.imagePort.hasError('max') ? 'Port must be equal to or lesser than 65535' : '';
  }

  addChallenge() {
  		this.markAllFormControlsAsTouched();
		this.alertService.clear();
  		if(!this.allFormControlsAreValid() || this.loading == true) {
	 		return;
  		}
  		const dockerDockerChallengeCreation: DockerChallengeCreation = {
	 		challengeName: this.challengeName.value,
	 		description: this.description.value,
	 		sourceCodeUrl: this.sourceCodeUrl.value,
	 		writeUpUrl: this.writeUpUrl.value,
	 		suggestedTags: this.tags.map(tag => tag.name),
	 		flags: this.flags.map(flag => flag.name),
	 		imagePort: this.imagePort.value,
	 		imageName: this.dockerImageName.value
  		};
  		this.loading = true;
  		this.challengesProvider.addDockerChallenge(dockerDockerChallengeCreation).subscribe(
			 		v => {
				 		this.loading = false;
				 		this.alertService.success(`You've successfully added a challenge!`);
			 		},
			 		e => {
				 		this.loading = false;
				 		this.alertService.error(e.error);
			 		}
  		);
  }

  allFormControlsAreValid(): boolean {
	 return this.challengeName.valid &&
	 this.description.valid &&
	 this.imagePort.valid &&
	 this.dockerImageName.valid;
  }

  markAllFormControlsAsTouched(){
	 this.challengeName.markAsTouched();
	 this.description.markAsTouched();
	 this.sourceCodeUrl.markAsTouched();
	 this.writeUpUrl.markAsTouched();
	 this.imagePort.markAsTouched();
	 this.dockerImageName.markAsTouched();
  }

}
