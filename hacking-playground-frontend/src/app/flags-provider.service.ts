import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { FlagWithBaseId } from './flag-with-base-id'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FlagsProviderService {

  constructor(private http: HttpClient) {
  }

  howMuchFlags(baseId: string): Observable<number> {
		return this.http.get<number>('/flags/howMuch/' + baseId) 
  }

  howMuchFlagsIHave(baseId: string): Observable<number> {
		return this.http.get<number>('/flags/howMuchIHave/' + baseId) 
  }

  captureTheFlag(baseId: string, flag: string): Observable<HttpResponse<string>> {
	 	const httpOptions = { observe: 'response' as 'body' };
		const flagWithBaseId: FlagWithBaseId = {
			flag: flag,
			baseId: baseId
		};
		return this.http.post<HttpResponse<string>>('/flags/captureTheFlag/', flagWithBaseId, httpOptions);
  }

}
