import { Injectable } from '@angular/core';
import { HackerWithScore } from './hacker-with-score';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UsersRankingService {

  constructor(private http: HttpClient) { }

  usersByScoreDescending(): Observable<HackerWithScore[]>{
	 return this.http.get<HackerWithScore[]>('/users-ranking/by-score-descending')
  }
}

