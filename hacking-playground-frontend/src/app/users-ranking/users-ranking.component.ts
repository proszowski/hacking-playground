import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { HackerWithScore } from '../hacker-with-score';
import { UsersRankingService } from '../users-ranking.service';



@Component({
  selector: 'app-users-ranking',
  templateUrl: './users-ranking.component.html',
  styleUrls: ['./users-ranking.component.css']
})
export class UsersRankingComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name', 'score'];
  dataSource = new MatTableDataSource<HackerWithScore>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  constructor(private usersRankingService: UsersRankingService) { }

  ngOnInit() {
  	 this.dataSource.paginator = this.paginator;
	 this.usersRankingService.usersByScoreDescending().subscribe(
	 	users => this.dataSource.data = users
	 );
  }

}
