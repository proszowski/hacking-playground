import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../auth/authentication.service';
import { Router } from '@angular/router';
import { Hacker } from "../hacker";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  currentHacker: Hacker;

  constructor(private authenticationService: AuthenticationService, private router: Router) {
        this.authenticationService.currentHacker.subscribe(x => this.currentHacker = x);
   }

  ngOnInit() {
  }

  logout(){
			 this.authenticationService.logout();
  }

  routeToMainPage(){
		this.router.navigate(['/']);

  }

}
