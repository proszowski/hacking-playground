import { TestBed } from '@angular/core/testing';

import { FlagsProvider.TsService } from './flags-provider.ts.service';

describe('FlagsProvider.TsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlagsProvider.TsService = TestBed.get(FlagsProvider.TsService);
    expect(service).toBeTruthy();
  });
});
