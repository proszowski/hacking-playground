export interface HackerWithScore {
	id: string;
	name: string;
	score: number;
}
