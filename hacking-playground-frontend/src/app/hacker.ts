export interface Hacker {
	id: string;
	name: string;
	token: string;
}
