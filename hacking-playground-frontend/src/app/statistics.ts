import { StatisticsInfo } from './statistics-info'
import { FlagSubmissionInfo } from './flag-submission-info'

export class Statistics {
    statisticInfos: StatisticsInfo[];
    flagSubmissionInfos: FlagSubmissionInfo[];
}
