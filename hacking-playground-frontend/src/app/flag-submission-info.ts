import { StatisticsChunk } from './statistics-chunk';

export class FlagSubmissionInfo implements StatisticsChunk {
    date: string;
    flag: string;
    flagSubmissionResult: string;
    hackerId: string;
    baseId: string;

    timestamp(): Date{
        return new Date(this.date);
    }
}
