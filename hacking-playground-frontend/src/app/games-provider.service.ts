import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GamesProviderService {

  constructor(private http: HttpClient) {
  }

  getGame(challengeId: string): Observable<string>{
		return this.http.get('/create-game/' + challengeId, {responseType: 'text'});
  }

  restartGame(gameUrl: string): Observable<void>{
			 return this.http.get<void>('/restart-game/' + gameUrl);
  }
}
