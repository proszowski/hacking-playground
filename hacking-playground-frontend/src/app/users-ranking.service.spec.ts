import { TestBed } from '@angular/core/testing';

import { UsersRankingService } from './users-ranking.service';

describe('UsersRankingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsersRankingService = TestBed.get(UsersRankingService);
    expect(service).toBeTruthy();
  });
});
