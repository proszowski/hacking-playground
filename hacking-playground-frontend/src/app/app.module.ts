import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { ChallengeCardComponent, SubmitFlagDialog } from './challenge-card/challenge-card.component';
import { MenuComponent } from './menu/menu.component';
import { ChallengesComponent } from './challenges/challenges.component';
import { HttpClientModule } from '@angular/common/http';
import { AddDockerChallengeComponent } from './add-docker-challenge/add-docker-challenge.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AddChallengeComponent } from './add-challenge/add-challenge.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { UsersRankingComponent } from './users-ranking/users-ranking.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { MainPageComponent } from './main-page/main-page.component';
import { JwtInterceptor } from './auth/jwt.interceptor';
import { AlertComponent } from './alert/alert.component';
import { AddDockerComposeChallengeComponent, AddDockerComposeYamlDialog } from './add-docker-compose-challenge/add-docker-compose-challenge.component';
import { StatisticsComponent } from './statistics/statistics.component';


@NgModule({
  declarations: [
    AppComponent,
	 ChallengeCardComponent,
	 MenuComponent,
	 ChallengesComponent,
	 AddDockerChallengeComponent,
	 SubmitFlagDialog,
	 UsersRankingComponent,
	 LoginComponent,
	 RegisterComponent,
	 MainPageComponent,
	 AlertComponent,
	 AddChallengeComponent,
	 AddDockerComposeChallengeComponent,
	 AddDockerComposeYamlDialog,
	 StatisticsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
	 MaterialModule,
    HttpClientModule,
	 FlexLayoutModule,
	 FormsModule,
	 ReactiveFormsModule,
	 MatDialogModule,
	 MatTableModule,
	 MatPaginatorModule
  ],
  entryComponents: [
	 SubmitFlagDialog,
	 AddDockerComposeYamlDialog
  ],
  providers: [
			 {
			 	provide: HTTP_INTERCEPTORS,
			 	useClass: JwtInterceptor,
			 	multi: true
			 }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
