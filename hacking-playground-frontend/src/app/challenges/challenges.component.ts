import { Component } from '@angular/core';
import { ChallengesProviderService } from '../challenges-provider.service'
import { Observable } from 'rxjs';
import { Challenge } from '../challenge'

@Component({
  selector: 'app-challenges',
  templateUrl: './challenges.component.html',
  styleUrls: ['./challenges.component.css']
})
export class ChallengesComponent {

  challenges: Challenge[]

  constructor(private challengesProvider: ChallengesProviderService){
  		this.challengesProvider
  							.getChallenges()
							.subscribe(challenges => {
									  this.challenges = challenges;
							});
  }

  title = 'hacking-playground-frontend';
}

