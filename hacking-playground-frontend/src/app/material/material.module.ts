import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatDividerModule } from '@angular/material/divider';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatChipsModule } from '@angular/material/chips';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatSliderModule } from '@angular/material/slider';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
	 MatCardModule,
	 MatButtonModule,
	 MatListModule,
	 MatDividerModule,
	 MatGridListModule,
	 MatChipsModule,
	 MatMenuModule,
	 MatToolbarModule,
	 MatInputModule,
	 MatSliderModule,
	 MatIconModule,
	 MatFormFieldModule,
	 MatProgressSpinnerModule,
	 MatTabsModule,
	 MatExpansionModule
  ],
  exports: [
    CommonModule,
	 MatCardModule,
	 MatButtonModule,
	 MatListModule,
	 MatDividerModule,
	 MatGridListModule,
	 MatChipsModule,
	 MatMenuModule,
	 MatToolbarModule,
	 MatInputModule,
	 MatSliderModule,
	 MatIconModule,
	 MatFormFieldModule,
	 MatProgressSpinnerModule,
	 MatTabsModule,
	 MatExpansionModule
  ]
})
export class MaterialModule { }
