export interface Challenge {
	id: string;
	name: string;
	description: string;
	tags: string[];
	difficulty: number;
}
