import { TestBed } from '@angular/core/testing';

import { ChallengesProviderService } from './challenges-provider.service';

describe('ChallengesProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChallengesProviderService = TestBed.get(ChallengesProviderService);
    expect(service).toBeTruthy();
  });
});
