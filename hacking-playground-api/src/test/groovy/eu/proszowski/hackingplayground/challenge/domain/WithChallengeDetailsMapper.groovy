package eu.proszowski.hackingplayground.challenge.domain


import eu.proszowski.hackingplayground.WithDockerClient
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeDetailsMapper
import eu.proszowski.hackingplayground.common.RuntimeParameters

trait WithChallengeDetailsMapper implements WithDockerClient {

    ChallengeDetailsMapper dockerChallengeDetailsMapper(){
        return dockerChallengeDetailsMapper(false)
    }

    ChallengeDetailsMapper dockerChallengeDetailsMapper(boolean shouldPullImage){
        return new DockerChallengeDetailsMapper(dockerClient(), new RuntimeParameters(shouldPullImage, true, "", "", "", "", "", false));
    }
}