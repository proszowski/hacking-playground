package eu.proszowski.hackingplayground.challenge


import eu.proszowski.hackingplayground.TestTags
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId
import eu.proszowski.hackingplayground.challenge.domain.WithChallengeDetailsMapper
import eu.proszowski.hackingplayground.challenge.domain.WithTestChallenge
import eu.proszowski.hackingplayground.challenge.domain.WithTestChallengeFactory
import eu.proszowski.hackingplayground.challenge.domain.WithTestInstallationCommands
import eu.proszowski.hackingplayground.challenge.domain.exception.FailToPullImageException
import eu.proszowski.hackingplayground.challenge.domain.exception.ImageAlreadyInstalledException
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository
import eu.proszowski.hackingplayground.challenge.infrastracture.WithTestChallengeRepository
import eu.proszowski.hackingplayground.flags.WithFlagsFacade
import eu.proszowski.hackingplayground.flags.domain.BaseId
import eu.proszowski.hackingplayground.flags.domain.DefaultBaseId
import eu.proszowski.hackingplayground.flags.domain.Flag
import eu.proszowski.hackingplayground.flags.domain.FlagSubmissionResult
import eu.proszowski.hackingplayground.flags.domain.FlagsFacade
import eu.proszowski.hackingplayground.flags.domain.WithTestFlags
import eu.proszowski.hackingplayground.game.domain.ports.Game
import eu.proszowski.hackingplayground.hacker.domain.Hacker
import eu.proszowski.hackingplayground.hacker.domain.WithTestHacker
import eu.proszowski.hackingplayground.statistics.StatisticsFacade
import org.junit.jupiter.api.Tag
import org.mockito.Mockito
import org.springframework.core.io.ClassPathResource
import spock.lang.Specification

import static eu.proszowski.hackingplayground.TestImageNames.*

@Tag(TestTags.INTERNET_CONNECTION_NEEDED)
class ChallengeFacadeTest extends Specification implements WithTestChallengeFactory, WithTestChallengeRepository, WithTestChallenge, WithTestHacker, WithTestInstallationCommands, WithTestChallengeCreation, WithChallengeDetailsMapper, WithFlagsFacade, WithTestFlags {

    private static final String EXISTING_WORKING_IMAGE_IN_DOCKER_HUB = PHP_EXAMPLE
    private static final String IMAGE_NOT_EXISTING_IN_DOCKER_HUB = "powder13/the-hacking-playground:not-existing"
    private static final int PORT_OF_EXISTING_WORKING_IMAGE = 80;
    private static final int PORT_OF_NOT_EXISTING_IMAGE = 8080;

    void "should create challenge with single docker image"() {
        given:
        ChallengeRepository challengeRepository = inMemoryChallengeRepository();
        ChallengeFacade challengeFacade = new ChallengeFacade(simpleChallengeFactory(challengeRepository), challengeRepository, dockerChallengeDetailsMapper(), flagsFacade())
        Challenge challenge = simpleChallengeWithDockerImageExistingInDockerHubRegistry();
        Hacker hacker = thereIsAHacker()

        when:
        ChallengeId challengeId = challengeFacade.createNewChallenge(hacker.getId(), fromDockerChallenge(challenge))

        then:
        challengeRepository.findById(challengeId).isPresent()
    }

    void "should create challenge with docker compose image"() {
        given:
        ChallengeRepository challengeRepository = inMemoryChallengeRepository();
        ChallengeFacade challengeFacade = new ChallengeFacade(simpleChallengeFactory(challengeRepository), challengeRepository, dockerChallengeDetailsMapper(), flagsFacade())

        String path = "src/test/resources/example-docker-compose.yml";
        File dockerComposeConfiguration = new File(path);

        def exposedServiceName = "server"
        def exposedPort = 5000

        Challenge challenge = challengeWithDockerComposeImage(dockerComposeConfiguration, exposedServiceName, exposedPort)
        Hacker hacker = thereIsAHacker()

        when:
        ChallengeId challengeId = challengeFacade.createNewChallenge(hacker.getId(), fromDockerComposeChallenge(challenge, exposedServiceName, exposedPort, dockerComposeConfiguration))

        then:
        challengeRepository.findById(challengeId).isPresent()
    }

    void "should not be able to install the same image two times"() {
        given:
        ChallengeRepository challengeRepository = inMemoryChallengeRepository();
        ChallengeFacade challengeFacade = new ChallengeFacade(simpleChallengeFactory(challengeRepository), challengeRepository, dockerChallengeDetailsMapper(), flagsFacade())
        Challenge challenge = challengeWithGivenNameAndPortAndInstallationCommands(EXISTING_WORKING_IMAGE_IN_DOCKER_HUB, PORT_OF_EXISTING_WORKING_IMAGE, dockerInstallationCommands(EXISTING_WORKING_IMAGE_IN_DOCKER_HUB))
        Hacker hacker = thereIsAHacker()

        when:
        ChallengeId challengeId = challengeFacade.createNewChallenge(hacker.getId(), fromDockerChallenge(challenge))
        Challenge installedChallenge = challengeRepository.findById(challengeId).get()
        installedChallenge.getDetails().getImage().install()

        then:
        thrown(ImageAlreadyInstalledException.class)
    }

    void "should throw an exception when it is not able to pull the image"() {
        given:
        ChallengeRepository challengeRepository = inMemoryChallengeRepository()
        ChallengeFacade challengeFacade = new ChallengeFacade(simpleChallengeFactory(challengeRepository), challengeRepository, dockerChallengeDetailsMapper(true), flagsFacade())
        Challenge challenge = challengeWithGivenNameAndPortAndInstallationCommands(IMAGE_NOT_EXISTING_IN_DOCKER_HUB, PORT_OF_NOT_EXISTING_IMAGE, dockerInstallationCommands(IMAGE_NOT_EXISTING_IN_DOCKER_HUB))
        Hacker hacker = thereIsAHacker()

        when:
        challengeFacade.createNewChallenge(hacker.getId(), fromDockerChallenge(challenge))

        then:
        thrown(FailToPullImageException.class)
    }


    void "should decrease challenge difficulty once it is solved by user"(){
        given:
        ChallengeRepository challengeRepository = inMemoryChallengeRepository()
        Hacker hacker = thereIsAHacker()
        StatisticsFacade statisticsFacade = Mockito.mock(StatisticsFacade)
        FlagsFacade flagsFacade = flagsFacade(statisticsFacade)
        Challenge challenge = challengeWithGivenNameAndPortAndInstallationCommands(EXISTING_WORKING_IMAGE_IN_DOCKER_HUB, PORT_OF_EXISTING_WORKING_IMAGE, dockerInstallationCommands(EXISTING_WORKING_IMAGE_IN_DOCKER_HUB))
        ChallengeFacade challengeFacade = new ChallengeFacade(simpleChallengeFactory(challengeRepository), challengeRepository, dockerChallengeDetailsMapper(), flagsFacade)
        Flag flag = testFlag()
        ChallengeId challengeId = challengeFacade.createNewChallenge(hacker.getId(), fromDockerChallenge(challenge, [flag.getRaw()]))
        BaseId baseId = DefaultBaseId.fromUUID(challengeId.getRaw())
        flagsFacade.associateBaseIdWithFlags(baseId, [flag] as Set<Flag>)

        when:
        FlagSubmissionResult submissionResult = flagsFacade.captureTheFlag(hacker.getId(), baseId, flag)

        then:
        submissionResult == FlagSubmissionResult.SUCCESS
        flagsFacade.howManyFlagsHaveBeenFoundByGivenHackerInGivenBase(hacker.getId(), baseId) == 1
        challengeFacade.getAllChallenges().count{ it.id == baseId.getRaw() && it.difficulty == 995} == 1
    }

}
