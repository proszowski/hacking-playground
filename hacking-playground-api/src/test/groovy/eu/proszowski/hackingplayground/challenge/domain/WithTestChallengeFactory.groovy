package eu.proszowski.hackingplayground.challenge.domain


import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeFactory
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository
import eu.proszowski.hackingplayground.challenge.infrastracture.WithTestChallengeRepository

trait WithTestChallengeFactory implements WithTestChallengeRepository {

    ChallengeFactory simpleChallengeFactory(ChallengeRepository challengeRepository) {
        return DefaultChallengeFactory.builder()
                .challengeRepository(challengeRepository)
                .build()
    }

}