package eu.proszowski.hackingplayground.challenge.domain

import com.spotify.docker.client.DefaultDockerClient
import com.spotify.docker.client.DockerClient
import eu.proszowski.hackingplayground.challenge.domain.exception.FailedToInstallChallengeException
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationCommands
import org.springframework.core.io.ClassPathResource
import spock.lang.Ignore
import spock.lang.Specification

import java.nio.file.Files
import java.nio.file.Paths


@Ignore
class DockerComposeInstallationCommandsTest extends Specification {

    public static final String LOCALHOST_DOCKER = "unix:///var/run/docker.sock";

    void "docker compose file without services defined should fail during installation"(String pathToFile) {
        given:
        DockerClient dockerClient = new DefaultDockerClient(LOCALHOST_DOCKER)

        File dockerComposeFile = new ClassPathResource(pathToFile).getFile()
        String dockerComposeConfiguration = Files.readString(Paths.get(dockerComposeFile.getPath()))

        final InstallationCommands installationCommands = DockerComposeInstallationCommands.builder()
                .challengeName("DUMMY")
                .dockerComposeConfiguration(dockerComposeConfiguration)
                .shouldPullImage(true)
                .dockerClient(dockerClient)
                .build()

        when:
        installationCommands.run()

        then:
        thrown(FailedToInstallChallengeException.class)

        where:
        pathToFile << ["example-docker-compose-without-services.yml"]
    }
}
