package eu.proszowski.hackingplayground.challenge.infrastracture

import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository
import eu.proszowski.hackingplayground.challenge.infrastracture.db.InMemoryChallengeRepository

trait WithTestChallengeRepository {

    ChallengeRepository inMemoryChallengeRepository() {
        return InMemoryChallengeRepository.builder()
                .build()
    }

}