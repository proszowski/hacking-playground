package eu.proszowski.hackingplayground.challenge.domain.adapters


import com.spotify.docker.client.DockerClient
import eu.proszowski.hackingplayground.TestTags
import eu.proszowski.hackingplayground.WithDockerClient
import org.junit.jupiter.api.Tag
import spock.lang.Specification

import static eu.proszowski.hackingplayground.TestImageNames.FLANKI

@Tag(TestTags.EXPLORATORY)
@Tag(TestTags.INTERNET_CONNECTION_NEEDED)
class DockerInstallationCommandsTest extends Specification implements WithDockerClient {

    private static final String imageName = FLANKI

    void "pull docker image"() {
        DockerClient dockerClient = dockerClient()
        dockerClient.pull(imageName)
        expect:
        1 == 1
    }
}
