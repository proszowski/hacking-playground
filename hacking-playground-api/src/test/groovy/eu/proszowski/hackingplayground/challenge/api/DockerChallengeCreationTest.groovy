package eu.proszowski.hackingplayground.challenge.api

import com.fasterxml.jackson.databind.ObjectMapper
import eu.proszowski.hackingplayground.TestImageNames
import org.junit.jupiter.api.Tag
import spock.lang.Specification


@Tag(eu.proszowski.hackingplayground.TestTags.EXPLORATORY)
class DockerChallengeCreationTest extends Specification {

    void "sample challenge creation"() {
        def challengeCreation = DockerChallengeCreation.builder()
                .challengeName("sample application")
                .description("very easy to find vulnerability")
                .sourceCodeUrl("")
                .writeUpUrl("")
                .suggestedTags(Collections.emptyList())
                .flags(Collections.emptyList())
                .imagePort(80)
                .imageName(TestImageNames.PHP_EXAMPLE)
                .build()
        println(challengeCreation)
        println(new ObjectMapper().writeValueAsString(challengeCreation))
        expect:
        challengeCreation
    }
}
