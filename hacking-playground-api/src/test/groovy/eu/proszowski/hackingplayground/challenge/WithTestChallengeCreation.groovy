package eu.proszowski.hackingplayground.challenge

import eu.proszowski.hackingplayground.challenge.api.DockerChallengeCreation
import eu.proszowski.hackingplayground.challenge.api.DockerComposeChallengeCreation
import eu.proszowski.hackingplayground.challenge.domain.DockerComposeImage
import eu.proszowski.hackingplayground.challenge.domain.DockerImage
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge

import java.nio.file.Files
import java.nio.file.Paths

trait WithTestChallengeCreation {

    DockerChallengeCreation fromDockerChallenge(Challenge challenge) {
        return fromDockerChallenge(challenge, [])
    }

    DockerChallengeCreation fromDockerChallenge(Challenge challenge, List<String> flags) {
        DockerImage dockerImage = (DockerImage) challenge.getDetails().getImage();

        return DockerChallengeCreation.builder()
                .challengeName(challenge.getName())
                .description(challenge.getDescription())
                .sourceCodeUrl("dummy source code url")
                .writeUpUrl("dummy write up url")
                .suggestedTags(challenge.getTags().collect { tag -> tag.getName() })
                .imagePort(dockerImage.getPort())
                .imageName(dockerImage.getName())
                .flags(flags)
                .build()
    }

    DockerComposeChallengeCreation fromDockerComposeChallenge(Challenge challenge, String exposedServiceName, int exposedPort, File dockerComposeConfiguration) {
        String configuration = Files.readString(Paths.get(dockerComposeConfiguration.getPath()))

        return DockerComposeChallengeCreation.builder()
                .challengeName(challenge.getName())
                .description(challenge.getDescription())
                .sourceCodeUrl("dummy source code url")
                .writeUpUrl("dumm write up url")
                .suggestedTags(challenge.getTags().collect { tag -> tag.getName() })
                .flags([])
                .exposedServiceName(exposedServiceName)
                .exposedPort(exposedPort)
                .dockerComposeYaml(configuration)
                .build()
    }
}