package eu.proszowski.hackingplayground.challenge.domain


import eu.proszowski.hackingplayground.WithDockerClient
import eu.proszowski.hackingplayground.challenge.domain.adapters.DockerInstallationCommands
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationCommands

trait WithTestInstallationCommands implements WithDockerClient {

    InstallationCommands dockerInstallationCommands(String imageName) {
        return DockerInstallationCommands.builder()
                .dockerClient(dockerClient())
                .challengeName("DUMMY CHALLENGE NAME")
                .imageName(imageName)
                .build()
    }

    InstallationCommands dockerComposeInstallationCommands(String dockerComposeConfiguration) {
        return DockerComposeInstallationCommands.builder()
                .dockerClient(dockerClient())
                .challengeName("DUMMY CHALLENGE NAME")
                .dockerComposeConfiguration(dockerComposeConfiguration)
                .build()
    }
}