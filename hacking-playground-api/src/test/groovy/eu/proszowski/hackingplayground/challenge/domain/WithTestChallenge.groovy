package eu.proszowski.hackingplayground.challenge.domain

import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeDetails
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationCommands
import org.springframework.core.io.ClassPathResource

import java.nio.file.Files
import java.nio.file.Paths

import static eu.proszowski.hackingplayground.TestImageNames.*


trait WithTestChallenge implements WithTestInstallationCommands {

    private static final String EXISTING_IMAGE_IN_DOCKER_HUB = PHP_EXAMPLE
    private static final String DUMMY_NAME = "DUMMY_NAME"
    private static final String DUMMY_DESCRIPTION = "DUMMY_DESCRIPTION"

    Challenge simpleChallengeWithNameAndPort(String name, int port) {
        return challengeWithGivenNameAndPortAndInstallationCommands(name, port, dockerInstallationCommands(name))
    }


    Challenge simpleChallengeWithDockerImageExistingInDockerHubRegistry() {
        return challengeWithGivenNameAndPortAndInstallationCommands(EXISTING_IMAGE_IN_DOCKER_HUB, 80, dockerInstallationCommands(EXISTING_IMAGE_IN_DOCKER_HUB))
    }

    static Challenge challengeWithGivenNameAndPortAndInstallationCommands(String name, int port, InstallationCommands installationCommands) {
        ChallengeDetails challengeDetails = DefaultChallengeDetails.builder()
                .image(DockerImage.builder()
                        .name(name)
                        .port(port)
                        .installationCommands(installationCommands)
                        .build())
                .writeUp(new UrlWriteUp(""))
                .sourceCode(new UrlSourceCode(""))
                .build()

        return DefaultChallenge.builder()
                .id(ChallengeId.fromUUID(UUID.randomUUID()))
                .details(challengeDetails)
                .name(DUMMY_NAME)
                .description(DUMMY_DESCRIPTION)
                .build()
    }

    Challenge challengeWithDockerComposeImage(File dockerComposeConfiguration, String exposedService, int exposedPort){

        String configuration = Files.readString(Paths.get(dockerComposeConfiguration.getPath()))

        ChallengeDetails challengeDetails = DefaultChallengeDetails.builder()
                .image(DockerComposeImage.builder()
                        .dockerComposeConfig(configuration)
                        .installationCommands(dockerComposeInstallationCommands(configuration))
                        .exposedPort(exposedPort)
                        .exposedServiceName(exposedService)
                        .build())
                .writeUp(new UrlWriteUp(""))
                .sourceCode(new UrlSourceCode(""))
                .build()

        return DefaultChallenge.builder()
                .id(ChallengeId.fromUUID(UUID.randomUUID()))
                .details(challengeDetails)
                .name(DUMMY_NAME)
                .description(DUMMY_DESCRIPTION)
                .build()
    }

    Challenge simpleChallengeWithDockerComposeImage(){
        String path = "src/test/resources/example-docker-compose.yml";
        File file = new File(path);
        return challengeWithDockerComposeImage(file, "server", 5000)
    }
}