package eu.proszowski.hackingplayground.hacker.domain


trait WithTestHacker {

    Hacker thereIsAHacker() {
        return DefaultHacker.builder()
                .id(HackerId.fromUUID(UUID.randomUUID()))
                .name("example hacker name")
                .build()
    }
}