package eu.proszowski.hackingplayground.hacker

import eu.proszowski.hackingplayground.hacker.domain.DefaultHackerFactory
import eu.proszowski.hackingplayground.hacker.domain.HackerFacade
import eu.proszowski.hackingplayground.ranking.WithHackersRepository

trait WithHackerFacade implements WithHackersRepository {
    HackerFacade hackerFacade(){
        return new HackerFacade(inMemoryhackersRepository(), new DefaultHackerFactory())
    }
}