package eu.proszowski.hackingplayground.hacker.domain

import eu.proszowski.hackingplayground.hacker.HackerCreation
import eu.proszowski.hackingplayground.hacker.db.InMemoryHackerRepository
import spock.lang.Specification


class HackerFacadeTest extends Specification {

    void "should be able to create new hacker"() {
        given:
        HackerFacade hackerFacade = new HackerFacade(new InMemoryHackerRepository(), new DefaultHackerFactory())
        HackerCreation hackerCreation = HackerCreation.builder()
                .id(HackerId.fromUUID(UUID.randomUUID()))
                .name("Example name")
                .build()

        when:
        HackerId hackerId = hackerFacade.save(hackerCreation)

        then:
        List<Hacker> hackers = hackerFacade.getAllHackers()
        hackers.collect { it.id }.contains(hackerId)
        hackers.size() == 1
    }

    void "should not be able to create hacker with the name which already is taken"() {
        given:
        HackerFacade hackerFacade = new HackerFacade(new InMemoryHackerRepository(), new DefaultHackerFactory())
        HackerCreation hackerCreation = HackerCreation.builder()
                .id(HackerId.fromUUID(UUID.randomUUID()))
                .name("Example name")
                .build()

        HackerCreation secondHackerCreation = HackerCreation.builder()
                .id(HackerId.fromUUID(UUID.randomUUID()))
                .name("EXAMPLE NAME")
                .build()

        when:
        hackerFacade.save(hackerCreation)
        hackerFacade.save(secondHackerCreation)

        then:
        thrown(HackerNameIsTakenException.class)
    }

}
