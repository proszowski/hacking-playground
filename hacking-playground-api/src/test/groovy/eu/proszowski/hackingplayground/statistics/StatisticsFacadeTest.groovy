package eu.proszowski.hackingplayground.statistics

import eu.proszowski.hackingplayground.challenge.domain.WithTestChallenge
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge
import eu.proszowski.hackingplayground.flags.WithFlagsFacade
import eu.proszowski.hackingplayground.flags.domain.DefaultBaseId
import eu.proszowski.hackingplayground.flags.domain.DefaultFlag
import eu.proszowski.hackingplayground.flags.domain.FlagsFacade
import eu.proszowski.hackingplayground.game.WithTestGame
import eu.proszowski.hackingplayground.game.domain.ports.Game
import eu.proszowski.hackingplayground.game.domain.ports.GameAssociationsRepository
import eu.proszowski.hackingplayground.game.domain.ports.GameRepository
import eu.proszowski.hackingplayground.game.infrastracture.db.InMemoryGameAssociationsRepository
import eu.proszowski.hackingplayground.game.infrastracture.db.WithTestGameRepository
import eu.proszowski.hackingplayground.hacker.domain.Hacker
import eu.proszowski.hackingplayground.hacker.domain.WithTestHacker
import eu.proszowski.hackingplayground.statistics.db.InMemoryStatisticsDatabase
import org.assertj.core.api.WithAssertions
import org.springframework.http.HttpMethod
import org.testcontainers.shaded.com.google.common.collect.ImmutableMap
import spock.lang.Specification

class StatisticsFacadeTest extends Specification implements WithTestGame, WithAssertions, WithTestGameRepository, WithFlagsFacade, WithTestHacker, WithTestChallenge {

    void "should save flag submission properly"() {
        given:
        GameAssociationsRepository gameAssociationsRepository = new InMemoryGameAssociationsRepository()
        GameRepository gameRepository = gameRepository()
        StatisticsFacade statisticsFacade = new StatisticsFacade(gameRepository, new InMemoryStatisticsDatabase(), gameAssociationsRepository)
        Hacker hacker = thereIsAHacker()
        Challenge challenge = simpleChallengeWithDockerComposeImage()
        FlagsFacade flagsFacade = flagsFacade(statisticsFacade)
        String dummyGameUrl = "URI"
        Game game = simpleGame(dummyGameUrl)
        gameAssociationsRepository.createAssociation(challenge.getId(), hacker.getId(), game.getId())
        gameRepository.save(game)


        when:
        flagsFacade.captureTheFlag(hacker.getId(), DefaultBaseId.fromUUID(challenge.getId().getRaw()), DefaultFlag.fromString("dummy"))

        then:
        statisticsFacade.getStatisticsForGame(dummyGameUrl).getFlagSubmissionInfos().size() == 1
    }

    void "should save statistics properly"() {
        given:
        GameRepository gameRepository = gameRepository()
        String urlPrefix = "/DUMMY/"
        String dummyGameUrl = "URI"
        gameRepository.save(simpleGame(dummyGameUrl))
        GameAssociationsRepository gameAssociationsRepository = new InMemoryGameAssociationsRepository()
        StatisticsFacade statisticsFacade = new StatisticsFacade(gameRepository, new InMemoryStatisticsDatabase(), gameAssociationsRepository)
        StatisticInfo statisticInfo = dummyStatisticInfo(urlPrefix + dummyGameUrl)

        when:
        statisticsFacade.notifyAboutRequest(statisticInfo)

        then:
        assertThat(statisticsFacade.getStatisticsForGame(dummyGameUrl).getStatisticInfos()).containsExactly(statisticInfo)
    }

    StatisticInfo dummyStatisticInfo(String uri) {
        Map<String, String> headers = ImmutableMap.<String, String> builder()
                .put("DummyKey", "DummyValue")
                .build();

        Map<String, String[]> parameters = ImmutableMap.<String, String[]> builder()
                .put("DummyKey", Arrays.asList("dummy", "keys").toArray(new String[0]))
                .build();

        StatisticInfo.builder()
                .body("dummy body")
                .headers(headers)
                .method(HttpMethod.GET)
                .parameters(parameters)
                .queryString("dummy=dummy")
                .uri(uri)
                .build();
    }

}
