package eu.proszowski.hackingplayground.flags.domain


trait WithTestFlags {

    Flag testFlag(){
        return DefaultFlag.fromString("example test flag")
    }
}