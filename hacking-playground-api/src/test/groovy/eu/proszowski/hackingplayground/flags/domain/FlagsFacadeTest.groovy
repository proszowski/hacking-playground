package eu.proszowski.hackingplayground.flags.domain


import eu.proszowski.hackingplayground.hacker.domain.Hacker
import eu.proszowski.hackingplayground.hacker.domain.WithTestHacker
import eu.proszowski.hackingplayground.statistics.StatisticsFacade
import org.mockito.Mockito
import spock.lang.Specification


class FlagsFacadeTest extends Specification implements WithTestFlags, WithFlagsRepository, WithTestHacker {

    FlagsRepository flagsRepository = flagsRepository()

    StatisticsFacade statisticsFacade = Mockito.mock(StatisticsFacade)

    FlagsFacade flagsFacade = new FlagsFacade(flagsRepository, statisticsFacade)

    void "should associate flags with given base"(){
        given:
        BaseId baseId = DefaultBaseId.fromUUID(UUID.randomUUID())
        Set<Flag> flags = [testFlag()]

        when:
        flagsFacade.associateBaseIdWithFlags(baseId, flags)

        then:
        flagsFacade.howManyFlags(baseId) == 1
    }

    void "should ignore duplicates, every flag should be unique"(){
        given:
        BaseId baseId = DefaultBaseId.fromUUID(UUID.randomUUID())
        Set<Flag> flags = [testFlag(), testFlag()]

        when:
        flagsFacade.associateBaseIdWithFlags(baseId, flags)

        then:
        flagsFacade.howManyFlags(baseId) == 1
    }

    void "hacker should be able to capture the flag"(){
        given:
        Flag flag = testFlag()
        Hacker hacker = thereIsAHacker()
        BaseId baseId = DefaultBaseId.fromUUID(UUID.randomUUID())
        Set<Flag> flags = [flag]
        flagsFacade.associateBaseIdWithFlags(baseId, flags)
        flagsFacade.subscribeForBaseCapture(baseId.getRaw(), { a -> 1 })

        when:
        FlagSubmissionResult submissionResult = flagsFacade.captureTheFlag(hacker.getId(), baseId, flag)

        then:
        submissionResult == FlagSubmissionResult.SUCCESS
        int numberOfFoundFlags =
                flagsFacade.howManyFlagsHaveBeenFoundByGivenHackerInGivenBase(hacker.getId(), baseId)
        numberOfFoundFlags == 1
    }

    void "hacker should not be able to submit the same flag twice"(){
        given:
        Flag flag = testFlag()
        Hacker hacker = thereIsAHacker()
        BaseId baseId = DefaultBaseId.fromUUID(UUID.randomUUID())
        Set<Flag> flags = [flag]
        flagsFacade.associateBaseIdWithFlags(baseId, flags)
        flagsFacade.subscribeForBaseCapture(baseId.getRaw(), { a -> 1 })

        when:
        flagsFacade.captureTheFlag(hacker.getId(), baseId, flag)
        FlagSubmissionResult submissionResult = flagsFacade.captureTheFlag(hacker.getId(), baseId, flag)

        then:
        submissionResult == FlagSubmissionResult.ALREADY_SUBMITTED
    }

    void "hacker should not be able to submit wrong flag"(){
        given:
        Flag flag = testFlag()
        Hacker hacker = thereIsAHacker()
        BaseId baseId = DefaultBaseId.fromUUID(UUID.randomUUID())
        Set<Flag> flags = [flag]
        flagsFacade.associateBaseIdWithFlags(baseId, flags)
        flagsFacade.subscribeForBaseCapture(baseId.getRaw(), { a -> 1 })

        when:
        FlagSubmissionResult submissionResult = flagsFacade.captureTheFlag(hacker.getId(), baseId, {name: "wrong_flag"})

        then:
        submissionResult == FlagSubmissionResult.FAILURE
        flagsFacade.howManyFlagsHaveBeenFoundByGivenHackerInGivenBase(hacker.getId(), baseId) == 0
    }
}
