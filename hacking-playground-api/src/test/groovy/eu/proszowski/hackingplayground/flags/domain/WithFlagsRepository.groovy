package eu.proszowski.hackingplayground.flags.domain

import eu.proszowski.hackingplayground.flags.infrastracture.db.InMemoryFlagsRepository


trait WithFlagsRepository {
    FlagsRepository flagsRepository(){
        return new InMemoryFlagsRepository();
    }
}