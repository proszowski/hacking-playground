package eu.proszowski.hackingplayground.flags

import eu.proszowski.hackingplayground.flags.domain.FlagsFacade
import eu.proszowski.hackingplayground.flags.domain.WithFlagsRepository
import eu.proszowski.hackingplayground.game.infrastracture.db.InMemoryGameAssociationsRepository
import eu.proszowski.hackingplayground.game.infrastracture.db.InMemoryGameRepository
import eu.proszowski.hackingplayground.statistics.db.InMemoryStatisticsDatabase
import eu.proszowski.hackingplayground.statistics.StatisticsFacade

trait WithFlagsFacade implements WithFlagsRepository{
    FlagsFacade flagsFacade(){
        return new FlagsFacade(flagsRepository(), new StatisticsFacade(new InMemoryGameRepository(), new InMemoryStatisticsDatabase(), new InMemoryGameAssociationsRepository()))
    }

    FlagsFacade flagsFacade(StatisticsFacade statisticsFacade){
        return new FlagsFacade(flagsRepository(), statisticsFacade)
    }
}