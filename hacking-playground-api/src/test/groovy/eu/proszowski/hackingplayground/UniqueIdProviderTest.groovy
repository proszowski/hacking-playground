package eu.proszowski.hackingplayground

import spock.lang.Specification

import java.util.stream.IntStream


class UniqueIdProviderTest extends Specification {

    void "should produce not unique ids"(){
        def size = 100000

        when:
        def uniqueIds = IntStream.range(0, size)
                .collect { UniqueIdProvider.next() }

        then:
        uniqueIds.size() == size
    }
}
