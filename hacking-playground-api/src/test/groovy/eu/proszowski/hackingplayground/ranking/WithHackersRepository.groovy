package eu.proszowski.hackingplayground.ranking

import eu.proszowski.hackingplayground.hacker.domain.HackerRepository
import eu.proszowski.hackingplayground.hacker.db.InMemoryHackerRepository


trait WithHackersRepository {

    HackerRepository inMemoryhackersRepository(){
        return new InMemoryHackerRepository();
    }

}