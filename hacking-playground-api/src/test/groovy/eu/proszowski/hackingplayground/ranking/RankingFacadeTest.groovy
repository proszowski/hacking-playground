package eu.proszowski.hackingplayground.ranking

import eu.proszowski.hackingplayground.challenge.ChallengeFacade
import eu.proszowski.hackingplayground.challenge.WithTestChallengeCreation
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId
import eu.proszowski.hackingplayground.challenge.domain.WithTestChallenge
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge
import eu.proszowski.hackingplayground.flags.WithFlagsFacade
import eu.proszowski.hackingplayground.flags.domain.BaseId
import eu.proszowski.hackingplayground.flags.domain.DefaultBaseId
import eu.proszowski.hackingplayground.flags.domain.DefaultFlag
import eu.proszowski.hackingplayground.flags.domain.Flag
import eu.proszowski.hackingplayground.flags.domain.FlagsFacade
import eu.proszowski.hackingplayground.flags.domain.WithTestFlags
import eu.proszowski.hackingplayground.hacker.domain.DefaultHackerFactory
import eu.proszowski.hackingplayground.hacker.domain.Hacker
import eu.proszowski.hackingplayground.hacker.domain.HackerFacade
import eu.proszowski.hackingplayground.hacker.domain.HackerRepository
import eu.proszowski.hackingplayground.hacker.domain.WithTestHacker
import eu.proszowski.hackingplayground.statistics.StatisticsFacade
import org.mockito.Mockito
import spock.lang.Specification


class RankingFacadeTest extends Specification implements WithFlagsFacade, WithTestHacker, WithTestFlags, WithHackersRepository, WithChallengeFacade, WithTestChallenge, WithTestChallengeCreation {

    void "should return hackers by score in descending order"(){
        given:
        StatisticsFacade statisticsFacade = Mockito.mock(StatisticsFacade)
        HackerRepository hackerRepository = inMemoryhackersRepository()
        FlagsFacade flagsFacade = flagsFacade(statisticsFacade)
        ChallengeFacade challengeFacade = challengeFacade(flagsFacade)
        HackerFacade hackerFacade = new HackerFacade(hackerRepository, new DefaultHackerFactory())

        Hacker firstHacker = thereIsAHacker()
        Hacker secondHacker = thereIsAHacker()
        Hacker thirdHacker = thereIsAHacker()
        hackerRepository.save(firstHacker)
        hackerRepository.save(secondHacker)
        hackerRepository.save(thirdHacker)

        RankingFacade rankingFacade = new RankingFacade(hackerFacade, flagsFacade, challengeFacade)

        Flag flag = DefaultFlag.fromString("first flag")
        Flag secondFlag = DefaultFlag.fromString("second flag")

        Challenge challenge = simpleChallengeWithDockerImageExistingInDockerHubRegistry()
        ChallengeId challengeId =
                challengeFacade.createNewChallenge(firstHacker.getId(), fromDockerChallenge(challenge, [flag.getRaw(), secondFlag.getRaw()]))
        BaseId baseId = DefaultBaseId.fromUUID(challengeId.getRaw())

        flagsFacade.associateBaseIdWithFlags(baseId, [flag, secondFlag] as Set<Flag>)
        flagsFacade.captureTheFlag(thirdHacker.getId(), baseId, flag)
        flagsFacade.captureTheFlag(thirdHacker.getId(), baseId, secondFlag)
        flagsFacade.captureTheFlag(secondHacker.getId(), baseId, secondFlag)

        when:
        List<HackerDto> hackersWithPosition = rankingFacade.getHackersByScoreDescending()

        then:
        hackersWithPosition.size() == 3
        hackersWithPosition.get(0).getId() == thirdHacker.getId().getRaw()
        hackersWithPosition.get(1).getId() == secondHacker.getId().getRaw()
        hackersWithPosition.get(2).getId() == firstHacker.getId().getRaw()
    }

}
