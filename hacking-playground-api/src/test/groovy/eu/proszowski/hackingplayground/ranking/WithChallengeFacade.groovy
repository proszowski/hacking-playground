package eu.proszowski.hackingplayground.ranking

import eu.proszowski.hackingplayground.challenge.ChallengeFacade
import eu.proszowski.hackingplayground.challenge.domain.WithChallengeDetailsMapper
import eu.proszowski.hackingplayground.challenge.domain.WithTestChallengeFactory
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository
import eu.proszowski.hackingplayground.challenge.infrastracture.WithTestChallengeRepository
import eu.proszowski.hackingplayground.flags.domain.FlagsFacade

trait WithChallengeFacade implements WithTestChallengeFactory, WithChallengeDetailsMapper, WithTestChallengeRepository {

    ChallengeFacade challengeFacade(FlagsFacade flagsFacade){
        ChallengeRepository challengeRepository = inMemoryChallengeRepository();
        return new ChallengeFacade(simpleChallengeFactory(challengeRepository), challengeRepository, dockerChallengeDetailsMapper(), flagsFacade)
    }

}