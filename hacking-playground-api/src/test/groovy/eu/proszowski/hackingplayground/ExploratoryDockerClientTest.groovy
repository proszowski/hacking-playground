package eu.proszowski.hackingplayground

import com.spotify.docker.client.DockerClient
import com.spotify.docker.client.messages.ContainerConfig
import com.spotify.docker.client.messages.ContainerCreation
import org.junit.jupiter.api.Tag
import spock.lang.Specification

import static TestImageNames.FLANKI

@Tag(TestTags.EXPLORATORY)
class ExploratoryDockerClientTest extends Specification implements WithDockerClient {

    void "should set proper ports when creating docker container"() {
        DockerClient dockerClient = dockerClient();

        ContainerConfig containerConfig = ContainerConfig.builder()
                .image(FLANKI)
                .addVolume('/tmp')
                .portSpecs("8080")
                .exposedPorts("8080")
                .build()
        ContainerCreation containerCreation = dockerClient.createContainer(containerConfig)
        dockerClient.startContainer(containerCreation.id())

        expect:
        1 == 1
    }
}
