package eu.proszowski.hackingplayground.authentication


trait WithUserRepository {

    UserRepository inMemoryUserRepository(){
        return new InMemoryUserRepository()
    }
}