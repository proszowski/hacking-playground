package eu.proszowski.hackingplayground.authentication

import eu.proszowski.hackingplayground.authentication.exception.EmailIsAlreadyTakenException
import eu.proszowski.hackingplayground.authentication.exception.PasswordTooShortException
import eu.proszowski.hackingplayground.authentication.exception.UserNameIsAlreadyTakenException
import eu.proszowski.hackingplayground.hacker.WithHackerFacade
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import spock.lang.Specification


class AuthenticationFacadeTest extends Specification implements WithUserRepository, WithHackerFacade {

    void "should be able to register new user"(){
        given:
        AuthenticationFacade authenticationFacade = new AuthenticationFacade(inMemoryUserRepository(), new DefaultUserFactory(new BCryptPasswordEncoder()), hackerFacade())

        def userName = "Simple name"
        UserRegister userRegister = new UserRegister(userName, "email@email.com", "simplePassword")

        when:
        authenticationFacade.registerUser(userRegister)

        then:
        UserDto userDto = authenticationFacade.getUserByName(userName)
        userDto.getName() == userName
        userDto.getId()
    }

    void "should not be able to register new user when username already exists"(){
        given:
        AuthenticationFacade authenticationFacade = new AuthenticationFacade(inMemoryUserRepository(), new DefaultUserFactory(new BCryptPasswordEncoder()), hackerFacade())

        def userName = "Simple name"
        UserRegister userRegister = new UserRegister(userName, "email@email.com", "simplePassword")
        UserRegister userRegister2 = new UserRegister(userName, "email2@email.com", "simplePassword")

        when:
        authenticationFacade.registerUser(userRegister)
        authenticationFacade.registerUser(userRegister2)

        then:
        thrown(UserNameIsAlreadyTakenException.class)
    }

    void "should not be able to register new user when email already exists"(){
        given:
        AuthenticationFacade authenticationFacade = new AuthenticationFacade(inMemoryUserRepository(), new DefaultUserFactory(new BCryptPasswordEncoder()), hackerFacade())

        def email = "email@email.com"
        UserRegister userRegister = new UserRegister("simple username", email, "simplePassword")
        UserRegister userRegister2 = new UserRegister("simple username2", email, "simplePassword")

        when:
        authenticationFacade.registerUser(userRegister)
        authenticationFacade.registerUser(userRegister2)

        then:
        thrown(EmailIsAlreadyTakenException.class)
    }

    void "should not be able to register new user when password length is less than 8"(){
        given:
        AuthenticationFacade authenticationFacade = new AuthenticationFacade(inMemoryUserRepository(), new DefaultUserFactory(new BCryptPasswordEncoder()), hackerFacade())

        UserRegister userRegister = new UserRegister("simple username", "email@email.com", "simp")

        when:
        authenticationFacade.registerUser(userRegister)

        then:
        thrown(PasswordTooShortException.class)
    }
}
