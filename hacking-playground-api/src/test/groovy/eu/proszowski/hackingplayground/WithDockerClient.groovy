package eu.proszowski.hackingplayground

import com.spotify.docker.client.DefaultDockerClient
import com.spotify.docker.client.DockerClient


trait WithDockerClient {
    private static final String LOCALHOST_DOCKER = "unix:///var/run/docker.sock";

    DockerClient dockerClient(){
        return new DefaultDockerClient(LOCALHOST_DOCKER)
    }
}
