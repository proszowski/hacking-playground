package eu.proszowski.hackingplayground

import org.junit.jupiter.api.Tag


class TestTags {

    public static final String EXPLORATORY = "Exploratory";
    public static final String INTERNET_CONNECTION_NEEDED = "Internet Connection Needed";
}
