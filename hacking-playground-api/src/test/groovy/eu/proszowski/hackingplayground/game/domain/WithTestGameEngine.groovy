package eu.proszowski.hackingplayground.game.domain


import eu.proszowski.hackingplayground.WithDockerClient
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository
import eu.proszowski.hackingplayground.common.RuntimeParameters
import eu.proszowski.hackingplayground.game.domain.ports.GameEngine
import eu.proszowski.hackingplayground.game.domain.ports.GameRepository
import eu.proszowski.hackingplayground.game.infrastracture.db.WithTestGameAssociationsRepository
import org.springframework.web.client.RestTemplate

import static org.mockito.Mockito.mock

trait WithTestGameEngine implements WithTestGameAssociationsRepository, WithDockerClient {

    GameEngine dockerGameEngine(ChallengeRepository challengeRepository, GameRepository gameRepository) {
        RestTemplate restTemplate = mock(RestTemplate.class)
        return dockerGameEngine(challengeRepository, gameRepository, restTemplate)
    }

    GameEngine dockerGameEngine(ChallengeRepository challengeRepository, GameRepository gameRepository, RestTemplate restTemplate) {
        return new DockerGameEngine(challengeRepository,
                gameRepository,
                { UUID.randomUUID().toString() },
                inMemoryGameAssociationsRepository(),
                dockerClient(),
                new GamesProxyConfig(new GameUrlResolver(), restTemplate, new RuntimeParameters(false, false, "", "", "", "", "", false))
                )
    }

    GameEngine dockerComposeGameEngine(ChallengeRepository challengeRepository, GameRepository gameRepository) {
        RestTemplate restTemplate = mock(RestTemplate.class)
        return dockerComposeGameEngine(challengeRepository, gameRepository, restTemplate)
    }

    GameEngine dockerComposeGameEngine(ChallengeRepository challengeRepository, GameRepository gameRepository, RestTemplate restTemplate) {
        return new DockerComposeGameEngine(challengeRepository,
                gameRepository,
                { UUID.randomUUID().toString() },
                dockerClient(),
                inMemoryGameAssociationsRepository(),
                new GamesProxyConfig(new GameUrlResolver(), restTemplate, new RuntimeParameters(false, false, "", "", "", "", "", false))
        )
    }

}