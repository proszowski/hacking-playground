package eu.proszowski.hackingplayground.game


import eu.proszowski.hackingplayground.challenge.domain.ChallengeId
import eu.proszowski.hackingplayground.challenge.domain.WithTestChallenge
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository
import eu.proszowski.hackingplayground.challenge.infrastracture.WithTestChallengeRepository
import eu.proszowski.hackingplayground.game.domain.WithTestGameEngine
import eu.proszowski.hackingplayground.game.domain.WithTestGameRequest
import eu.proszowski.hackingplayground.game.domain.exception.UserNotAuthorizedToRestartGameException
import eu.proszowski.hackingplayground.game.domain.ports.Game
import eu.proszowski.hackingplayground.game.domain.ports.GameRepository
import eu.proszowski.hackingplayground.game.domain.ports.GameStatus
import eu.proszowski.hackingplayground.game.infrastracture.db.WithTestGameRepository
import eu.proszowski.hackingplayground.hacker.domain.Hacker
import eu.proszowski.hackingplayground.hacker.domain.HackerId
import eu.proszowski.hackingplayground.hacker.domain.WithTestHacker
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import spock.lang.Specification

import java.lang.invoke.MethodHandles

class GameFacadeTest extends Specification implements WithTestGameEngine, WithTestGameRepository, WithTestChallengeRepository, WithTestChallenge, WithTestHacker, WithTestGameRequest {

    public static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass())

    void "should be able to create game, then stop it and then remove"(Challenge challenge) {
        given:
        ChallengeRepository challengeRepository = inMemoryChallengeRepository()
        ChallengeId challengeId = challengeRepository.save(challenge)
        Hacker hacker = thereIsAHacker()
        GameRepository gameRepository = gameRepository();

        GameFacade gameFacade = new GameFacade(
                Arrays.asList(dockerGameEngine(challengeRepository, gameRepository), dockerComposeGameEngine(challengeRepository, gameRepository)),
                challengeRepository,
                gameRepository
        )

        when:
        Game game = gameFacade.getInstanceOf(challengeId, hacker.getId())

        then:
        game.getStatus() == GameStatus.STARTED
        game.getUrl() != null
        game.stop()
        game.remove()

        where:
        challenge << [simpleChallengeWithDockerImageExistingInDockerHubRegistry(), simpleChallengeWithDockerComposeImage()]
    }

    void "should be able to reset game"(Challenge challenge) {
        given:
        ChallengeRepository challengeRepository = inMemoryChallengeRepository()
        ChallengeId challengeId = challengeRepository.save(challenge)
        Hacker hacker = thereIsAHacker()
        GameRepository gameRepository = gameRepository();

        GameFacade gameFacade = new GameFacade(
                Arrays.asList(dockerGameEngine(challengeRepository, gameRepository), dockerComposeGameEngine(challengeRepository, gameRepository)),
                challengeRepository,
                gameRepository
        )

        when:
        Game game = gameFacade.getInstanceOf(challengeId, hacker.getId())
        Game restartedGame = gameFacade.restartMachine(game.getUrl(), hacker.getId())

        then:
        restartedGame.getStatus() == GameStatus.RESTARTED
        restartedGame.getRestartsCount() == 1
        restartedGame.stop()
        restartedGame.remove()

        where:
        challenge << [simpleChallengeWithDockerImageExistingInDockerHubRegistry(), simpleChallengeWithDockerComposeImage()]
    }

    void "should not be able to reset game if is not authorized for that"(Challenge challenge) {
        given:
        ChallengeRepository challengeRepository = inMemoryChallengeRepository()
        ChallengeId challengeId = challengeRepository.save(challenge)
        Hacker hacker = thereIsAHacker()
        GameRepository gameRepository = gameRepository();

        GameFacade gameFacade = new GameFacade(
                Arrays.asList(dockerGameEngine(challengeRepository, gameRepository), dockerComposeGameEngine(challengeRepository, gameRepository)),
                challengeRepository,
                gameRepository
        )

        when:
        Game game = gameFacade.getInstanceOf(challengeId, hacker.getId())
        Game restartedGame = gameFacade.restartMachine(game.getUrl(), HackerId.fromUUID(UUID.randomUUID()))

        then:
        thrown(UserNotAuthorizedToRestartGameException.class)

        where:
        challenge << [simpleChallengeWithDockerImageExistingInDockerHubRegistry(), simpleChallengeWithDockerComposeImage()]
    }

    def cleanupSpec() {
        def dockerClient = dockerClient()
        def containerIds = dockerClient.listContainers().collect { it.id() }
        containerIds.each {
            try {
                dockerClient.killContainer(it)
                dockerClient.removeContainer(it)
            } catch (final Exception ignored) {
                LOGGER.info("Couldn't remove containers - already removed by testcontainers?")
                if(dockerClient.listContainers().collect { it.id() }.contains(it)){
                    dockerClient.removeContainer(it)
                }
            }
        }

        def networksIds = dockerClient.listNetworks().collect { it.id() }
        networksIds.each {
            try{
                dockerClient.removeNetwork(it)
            }catch (final Exception ignored){
                LOGGER.info("Couldn't remove containers - already removed by testcontainers?")
            }
        }

        dockerClient.close()
    }
}
