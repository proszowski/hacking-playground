package eu.proszowski.hackingplayground.game.domain


import eu.proszowski.hackingplayground.game.domain.ports.GameRequest
import eu.proszowski.hackingplayground.game.domain.ports.GameRequestFactory
import org.springframework.http.HttpMethod
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap


trait WithTestGameRequest {

    GameRequestFactory gameRequestFactory = new DefaultGameRequestFactory()

    GameRequest gameRequest() {
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>()
        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>()
        final HttpMethod httpMethod = HttpMethod.GET
        final String body = null
        gameRequestFactory.create(params, headers, httpMethod, body)
    }

    GameRequest gameRequestWithNestedPath() {
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>()
        final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>()
        final HttpMethod httpMethod = HttpMethod.GET
        final String body = null
        gameRequestFactory.create('/static/styles/styles.css', params, headers, httpMethod, body)
    }

}