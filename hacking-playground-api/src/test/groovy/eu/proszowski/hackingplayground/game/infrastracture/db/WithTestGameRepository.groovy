package eu.proszowski.hackingplayground.game.infrastracture.db

import eu.proszowski.hackingplayground.game.domain.ports.GameRepository

trait WithTestGameRepository {
    GameRepository gameRepository() {
        return new InMemoryGameRepository()
    }
}