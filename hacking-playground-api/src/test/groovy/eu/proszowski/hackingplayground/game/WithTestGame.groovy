package eu.proszowski.hackingplayground.game

import eu.proszowski.hackingplayground.WithDockerClient
import eu.proszowski.hackingplayground.challenge.domain.WithTestChallenge
import eu.proszowski.hackingplayground.game.domain.DockerGame
import eu.proszowski.hackingplayground.game.domain.GameId
import eu.proszowski.hackingplayground.game.domain.ports.Game
import eu.proszowski.hackingplayground.hacker.domain.HackerId

trait WithTestGame implements WithTestChallenge, WithDockerClient {

    public String DUMMY_NAME = "dummy"
    public int DUMMY_PORT = 9000

    Game simpleGame(String url) {
        DockerGame.builder()
                .id(GameId.fromUUID(UUID.randomUUID()))
                .dockerClient(dockerClient())
                .url(url)
                .challenge(simpleChallengeWithNameAndPort(DUMMY_NAME, DUMMY_PORT))
                .creatorId(HackerId.fromUUID(UUID.randomUUID()))
                .containerId(UUID.randomUUID().toString())
                .build()
    }

}