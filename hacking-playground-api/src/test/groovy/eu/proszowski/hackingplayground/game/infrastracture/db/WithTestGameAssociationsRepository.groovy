package eu.proszowski.hackingplayground.game.infrastracture.db

import eu.proszowski.hackingplayground.game.domain.ports.GameAssociationsRepository


trait WithTestGameAssociationsRepository {

    GameAssociationsRepository inMemoryGameAssociationsRepository() {
        return new InMemoryGameAssociationsRepository()
    }
}