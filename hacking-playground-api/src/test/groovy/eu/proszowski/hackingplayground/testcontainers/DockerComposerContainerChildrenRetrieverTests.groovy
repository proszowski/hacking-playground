package eu.proszowski.hackingplayground.testcontainers

import eu.proszowski.hackingplayground.TestTags
import eu.proszowski.hackingplayground.game.domain.DockerComposeContainerChildrenRetriever
import org.junit.jupiter.api.Tag
import org.springframework.core.io.ClassPathResource
import org.testcontainers.containers.Container
import org.testcontainers.containers.DockerComposeContainer
import org.testcontainers.spock.Testcontainers
import spock.lang.Specification

@Tag(TestTags.EXPLORATORY)
@Tag(TestTags.INTERNET_CONNECTION_NEEDED)
@Testcontainers
class DockerComposerContainerChildrenRetrieverTests extends Specification {

    void "should create and run container from docker-compose and be able to get info about them"() {
        given:
        String path = "src/test/resources/example-docker-compose.yml";
        File file = new File(path);
        DockerComposeContainer dockerComposeContainer = new DockerComposeContainer(file)

        when:
        dockerComposeContainer.start()
        List<Container> childContainers = DockerComposeContainerChildrenRetriever.getChildContainers(dockerComposeContainer)

        then:
        childContainers.size() == 2
        dockerComposeContainer.stop()

    }
}
