package eu.proszowski.hackingplayground

import org.junit.jupiter.api.Tag
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification


@Tag(TestTags.INTERNET_CONNECTION_NEEDED)
@SpringBootTest
class HackingPlaygroundApplicationTest extends Specification {

    void "Context loads"() {
        expect:
        1 == 1
    }
}
