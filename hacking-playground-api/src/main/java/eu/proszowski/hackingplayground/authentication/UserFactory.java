package eu.proszowski.hackingplayground.authentication;

public interface UserFactory {
    User createUser(UserRegister userRegister);
}
