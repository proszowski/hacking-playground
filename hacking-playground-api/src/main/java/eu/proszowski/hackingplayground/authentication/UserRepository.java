package eu.proszowski.hackingplayground.authentication;

import java.util.Optional;

public interface UserRepository {
    boolean existsByName(String name);

    boolean existsByEmail(String email);

    void save(User user);

    Optional<User> findByUsername(String username);
}
