package eu.proszowski.hackingplayground.authentication.exception;

public class UserNameIsAlreadyTakenException extends AuthenticationDomainException {
    public UserNameIsAlreadyTakenException(final String userName) {
        super(String.format("Username %s is already taken", userName));
    }
}
