package eu.proszowski.hackingplayground.authentication.db;

import eu.proszowski.hackingplayground.authentication.User;
import eu.proszowski.hackingplayground.authentication.UserRepository;

import java.util.Optional;

public class PostgresUserRepository implements UserRepository {

    private final JpaUserRepository jpaUserRepository;

    public PostgresUserRepository(final JpaUserRepository jpaUserRepository) {
        this.jpaUserRepository = jpaUserRepository;
    }

    @Override
    public boolean existsByName(final String name) {
        return this.jpaUserRepository.existsByName(name);
    }

    @Override
    public boolean existsByEmail(final String email) {
        return this.jpaUserRepository.existsByEmail(email);
    }

    @Override
    public void save(final User user) {
        final UserDbDto userDbDto = UserDbDto.builder()
                .email(user.getEmail())
                .name(user.getName())
                .password(user.getPassword())
                .id(user.getId())
                .build();
        this.jpaUserRepository.save(userDbDto);
    }

    @Override
    public Optional<User> findByUsername(final String username) {
        return this.jpaUserRepository.findByName(username)
                .map(UserDbDto::toModel);
    }
}
