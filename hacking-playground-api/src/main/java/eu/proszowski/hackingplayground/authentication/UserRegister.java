package eu.proszowski.hackingplayground.authentication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRegister {

    @NonNull
    private String name;

    @Email
    @NonNull
    private String email;

    @NonNull
    @Length(min = 8)
    private String password;
}
