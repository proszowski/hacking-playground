package eu.proszowski.hackingplayground.authentication;

import eu.proszowski.hackingplayground.authentication.db.JpaUserRepository;
import eu.proszowski.hackingplayground.authentication.db.PostgresUserRepository;
import eu.proszowski.hackingplayground.hacker.domain.HackerFacade;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class AuthenticationConfig {

    @Bean
    AuthenticationService authenticationService(final UserRepository userRepository, final AuthenticationFacade authenticationFacade) {
        return new DefaultAuthenticationService(userRepository, authenticationFacade);
    }

    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    UserRepository userRepository(final JpaUserRepository jpaUserRepository){
        return new PostgresUserRepository(jpaUserRepository);
    }

    @Bean
    AuthenticationFacade authenticationFacade(final UserRepository userRepository,
                                              final BCryptPasswordEncoder bCryptPasswordEncoder,
                                              final HackerFacade hackerFacade){
        return new AuthenticationFacade(userRepository, new DefaultUserFactory(bCryptPasswordEncoder), hackerFacade);
    }

}
