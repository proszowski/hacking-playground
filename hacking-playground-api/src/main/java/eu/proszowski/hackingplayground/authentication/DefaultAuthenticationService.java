package eu.proszowski.hackingplayground.authentication;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import java.util.UUID;

public class DefaultAuthenticationService implements AuthenticationService {

    private final UserRepository userRepository;
    private final AuthenticationFacade authenticationFacade;

    public DefaultAuthenticationService(final UserRepository userRepository, final AuthenticationFacade authenticationFacade) {
        this.userRepository = userRepository;
        this.authenticationFacade = authenticationFacade;
    }

    @Override
    public UUID getUserId() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = (String) authentication.getPrincipal();
        return authenticationFacade.getUserByName(username).getId();
    }
}
