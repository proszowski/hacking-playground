package eu.proszowski.hackingplayground.authentication;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import javax.validation.Valid;

@Component
public class DefaultUserAuthenticationController implements UserAuthenticationController {

    private final AuthenticationFacade authenticationFacade;

    public DefaultUserAuthenticationController(final AuthenticationFacade authenticationFacade) {
        this.authenticationFacade = authenticationFacade;
    }


    @Override
    public void register(final @Valid UserRegister userRegister) {
        authenticationFacade.registerUser(userRegister);
    }

    @Override
    public UserDto getCurrentUser() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authenticationFacade.getUserByName((String) authentication.getPrincipal());
    }

}
