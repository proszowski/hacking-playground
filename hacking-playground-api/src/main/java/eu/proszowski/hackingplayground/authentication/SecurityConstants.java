package eu.proszowski.hackingplayground.authentication;

class SecurityConstants {
    static final String SECRET = "SecretKeyToGenJWTs";
    static final long EXPIRATION_TIME = 864_000_000; // 10 days
    static final String TOKEN_PREFIX = "Bearer ";
    static final String HEADER_AUTHORIZATION_STRING = "Authorization";
    static final String HEADER_USER_ID_STRING = "User Id";
    static final String HEADER_USER_NAME_STRING = "User Name";
    static final String RUNTIME_JS = "/runtime-es2015.js";
    static final String POLYFILLS_JS = "/polyfills-es2015.js";
    static final String FAVICON  = "/favicon.ico";
    static final String STYLES_JS = "/styles-es2015.js";
    static final String VENDOR_JS = "/vendor-es2015.js";
    static final String MAIN_JS = "/main-es2015.js";
}
