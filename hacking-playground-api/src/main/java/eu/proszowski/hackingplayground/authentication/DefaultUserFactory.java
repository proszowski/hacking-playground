package eu.proszowski.hackingplayground.authentication;

import eu.proszowski.hackingplayground.UniqueIdProvider;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import java.util.UUID;

public class DefaultUserFactory implements UserFactory{

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public DefaultUserFactory(final BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public User createUser(final UserRegister userRegister) {
        final UUID uniqueId = UniqueIdProvider.next();

        return User.builder()
                .id(uniqueId)
                .name(userRegister.getName())
                .email(userRegister.getEmail())
                .password(bCryptPasswordEncoder.encode(userRegister.getPassword()))
                .build();
    }
}
