package eu.proszowski.hackingplayground.authentication;

import eu.proszowski.hackingplayground.authentication.exception.AuthenticationDomainException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.lang.invoke.MethodHandles;

@ControllerAdvice
public class AuthenticationExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());


    @ExceptionHandler(value = {AuthenticationDomainException.class} )
    protected ResponseEntity<Object> handleConflict(final RuntimeException ex,
                                                    final WebRequest request) {

        final String bodyOfResponse = ex.getMessage();
        logger.info("{}", bodyOfResponse);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}
