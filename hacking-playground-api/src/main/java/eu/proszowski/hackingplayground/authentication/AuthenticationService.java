package eu.proszowski.hackingplayground.authentication;

import java.util.UUID;

public interface AuthenticationService {
    UUID getUserId();
}
