package eu.proszowski.hackingplayground.authentication.exception;

public class UserNotFoundException extends AuthenticationDomainException {
    public UserNotFoundException(final String username) {
        super(String.format("User with username %s could not be found", username));
    }
}
