package eu.proszowski.hackingplayground.authentication.exception;

public class EmailIsAlreadyTakenException extends AuthenticationDomainException {
    public EmailIsAlreadyTakenException(final String email) {
        super(String.format("Email %s is already taken", email));
    }
}
