package eu.proszowski.hackingplayground.authentication;

import lombok.NonNull;
import lombok.Value;
import java.util.UUID;

@Value
public class UserDto {

    @NonNull
    private UUID id;

    @NonNull
    private String name;

    static UserDto fromUser(final User user) {
        return new UserDto(user.getId(), user.getName());
    }
}
