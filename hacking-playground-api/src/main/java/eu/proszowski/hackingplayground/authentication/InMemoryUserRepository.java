package eu.proszowski.hackingplayground.authentication;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class InMemoryUserRepository implements UserRepository{

    private final Map<UUID, User> users = new HashMap<>();

    @Override
    public boolean existsByName(final String name) {
        return users.values().stream()
                .map(User::getName)
                .anyMatch(n -> n.equalsIgnoreCase(name));
    }

    @Override
    public boolean existsByEmail(final String email) {
        return users.values().stream()
                .map(User::getEmail)
                .anyMatch(e -> e.equalsIgnoreCase(email));
    }

    @Override
    public void save(final User user) {
        users.put(user.getId(), user);
    }

    @Override
    public Optional<User> findByUsername(final String username) {
        return users.values().stream()
                .filter(user -> user.getName().equalsIgnoreCase(username))
                .findFirst();
    }
}
