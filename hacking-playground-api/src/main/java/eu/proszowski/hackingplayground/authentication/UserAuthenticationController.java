package eu.proszowski.hackingplayground.authentication;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("auth")
public interface UserAuthenticationController {

    @PostMapping("register")
    void register(@RequestBody UserRegister userRegister);

    @GetMapping("currentUser")
    UserDto getCurrentUser();

}
