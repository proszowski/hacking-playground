package eu.proszowski.hackingplayground.authentication.exception;

public class PasswordTooShortException extends AuthenticationDomainException {
    public PasswordTooShortException(final String password) {
        super(String.format("Minimum password length is 8 but was %s", password.length()));
    }
}
