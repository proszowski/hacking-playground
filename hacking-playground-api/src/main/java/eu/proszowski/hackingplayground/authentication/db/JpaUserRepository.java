package eu.proszowski.hackingplayground.authentication.db;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface JpaUserRepository extends JpaRepository<UserDbDto, UUID> {
    boolean existsByName(String name);

    boolean existsByEmail(String email);

    Optional<UserDbDto> findByName(String username);
}
