package eu.proszowski.hackingplayground.authentication.db;

import eu.proszowski.hackingplayground.authentication.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
@Builder
public class UserDbDto {

    @NotNull
    @Id
    private UUID id;

    @NotNull
    private String name;

    @NotNull
    private String email;

    @NotNull
    private String password;

    static User toModel(final UserDbDto userDbDto) {
        return User.builder()
                .email(userDbDto.getEmail())
                .name(userDbDto.getName())
                .password(userDbDto.getPassword())
                .id(userDbDto.getId())
                .build();
    }
}
