package eu.proszowski.hackingplayground.authentication.exception;

public class AuthenticationDomainException extends RuntimeException{
    AuthenticationDomainException(final String message) {
        super(message);
    }
}
