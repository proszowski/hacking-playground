package eu.proszowski.hackingplayground.authentication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLogin {

    @NonNull
    private String username;

    @NonNull
    private String password;
}
