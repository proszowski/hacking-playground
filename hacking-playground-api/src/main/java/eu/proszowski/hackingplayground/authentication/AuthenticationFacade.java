package eu.proszowski.hackingplayground.authentication;

import com.google.common.base.Preconditions;
import eu.proszowski.hackingplayground.hacker.HackerCreation;
import eu.proszowski.hackingplayground.hacker.domain.HackerFacade;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import eu.proszowski.hackingplayground.authentication.exception.EmailIsAlreadyTakenException;
import eu.proszowski.hackingplayground.authentication.exception.PasswordTooShortException;
import eu.proszowski.hackingplayground.authentication.exception.UserNameIsAlreadyTakenException;
import eu.proszowski.hackingplayground.authentication.exception.UserNotFoundException;

public class AuthenticationFacade {
    private final UserRepository userRepository;
    private final UserFactory userFactory;
    private final HackerFacade hackerFacade;

    public AuthenticationFacade(final UserRepository userRepository,
                                final UserFactory userFactory,
                                final HackerFacade hackerFacade) {
        this.userRepository = userRepository;
        this.userFactory = userFactory;
        this.hackerFacade = hackerFacade;
    }

    public void registerUser(final UserRegister userRegister) {

        validate(userRegister);

        final User user = userFactory.createUser(userRegister);
        userRepository.save(user);

        final HackerId hackerId = HackerId.fromUUID(user.getId());
        final HackerCreation hackerCreation = HackerCreation.builder()
                .id(hackerId)
                .name(user.getName())
                .build();

        hackerFacade.save(hackerCreation);
    }

    private void validate(final UserRegister userRegister) {
        Preconditions.checkArgument(userRegister.getEmail() != null);
        Preconditions.checkArgument(userRegister.getName() != null);
        Preconditions.checkArgument(userRegister.getPassword() != null);

        if(userRepository.existsByName(userRegister.getName())){
            throw new UserNameIsAlreadyTakenException(userRegister.getName());
        }

        if(userRepository.existsByEmail(userRegister.getEmail())){
            throw new EmailIsAlreadyTakenException(userRegister.getEmail());
        }

        if(userRegister.getPassword().length() < 8){
            throw new PasswordTooShortException(userRegister.getPassword());
        }
    }

    UserDto getUserByName(final String name) {
        return userRepository.findByUsername(name)
                .map(UserDto::fromUser)
                .orElseThrow(() -> new UserNotFoundException(name));
    }
}
