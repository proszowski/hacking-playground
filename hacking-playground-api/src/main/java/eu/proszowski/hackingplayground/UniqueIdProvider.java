package eu.proszowski.hackingplayground;

import com.google.common.collect.Sets;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;
import org.springframework.stereotype.Component;
import java.util.Set;
import java.util.UUID;

@Component
public class UniqueIdProvider {

    private static final Set<UUID> uuidSet = Sets.newConcurrentHashSet();

    private UniqueIdProvider() {
    }

    public static UUID next() {
        return Failsafe.with(retryPolicy())
                .get(() -> {
                    final UUID id = UUID.randomUUID();
                    if (uuidSet.contains(id)) {
                        throw new NotUniqueIdException();
                    }
                    uuidSet.add(id);
                    return id;
                });
    }

    private static RetryPolicy<Object> retryPolicy() {
        return new RetryPolicy<>()
                .withMaxRetries(Integer.MAX_VALUE)
                .handle(NotUniqueIdException.class);
    }

    private static class NotUniqueIdException extends RuntimeException {

    }
}
