package eu.proszowski.hackingplayground.hacker.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HackerConfig {

    @Bean
    HackerFacade hackerFacade(final HackerRepository hackerRepository){
        return new HackerFacade(hackerRepository, new DefaultHackerFactory());
    }
}
