package eu.proszowski.hackingplayground.hacker.domain;

import java.util.List;

public interface HackerRepository {
    List<Hacker> findAll();

    void save(Hacker hacker);

    boolean existsByName(String name);
}
