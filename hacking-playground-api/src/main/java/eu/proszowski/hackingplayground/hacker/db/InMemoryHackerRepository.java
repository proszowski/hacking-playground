package eu.proszowski.hackingplayground.hacker.db;

import eu.proszowski.hackingplayground.hacker.domain.Hacker;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import eu.proszowski.hackingplayground.hacker.domain.HackerRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryHackerRepository implements HackerRepository {

    private final Map<HackerId, Hacker> hackers = new HashMap<>();

    @Override
    public List<Hacker> findAll() {
        return new ArrayList<>(hackers.values());
    }

    @Override
    public void save(final Hacker hacker) {
        hackers.put(hacker.getId(), hacker);
    }

    @Override
    public boolean existsByName(final String name) {
        return hackers.values().stream()
                .map(Hacker::getName)
                .anyMatch(hackerName -> hackerName.equalsIgnoreCase(name));
    }
}
