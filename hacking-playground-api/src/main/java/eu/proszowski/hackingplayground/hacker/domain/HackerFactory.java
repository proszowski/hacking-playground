package eu.proszowski.hackingplayground.hacker.domain;

import eu.proszowski.hackingplayground.hacker.HackerCreation;

public interface HackerFactory {
    Hacker create(HackerCreation hackerCreation);
}
