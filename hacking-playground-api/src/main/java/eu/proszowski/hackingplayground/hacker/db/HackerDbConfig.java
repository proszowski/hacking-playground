package eu.proszowski.hackingplayground.hacker.db;

import eu.proszowski.hackingplayground.hacker.domain.HackerRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HackerDbConfig {

    @Bean
    HackerRepository hackerRepository(final JpaHackerRepository jpaHackerRepository){
        return new PostgresHackerRepository(jpaHackerRepository);
    }
}
