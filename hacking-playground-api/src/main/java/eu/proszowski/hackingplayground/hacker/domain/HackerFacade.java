package eu.proszowski.hackingplayground.hacker.domain;

import eu.proszowski.hackingplayground.hacker.HackerCreation;
import java.util.List;

public class HackerFacade {

    private final HackerRepository hackerRepository;
    private final HackerFactory hackerFactory;

    public HackerFacade(final HackerRepository hackerRepository,
                        final HackerFactory hackerFactory) {
        this.hackerRepository = hackerRepository;
        this.hackerFactory = hackerFactory;
    }

    public List<Hacker> getAllHackers() {
        return hackerRepository.findAll();
    }

    public HackerId save(final HackerCreation hackerCreation) {
        if (hackerRepository.existsByName(hackerCreation.getName())) {
            throw new HackerNameIsTakenException(hackerCreation.getName());
        }

        final Hacker defaultHacker = hackerFactory.create(hackerCreation);
        hackerRepository.save(defaultHacker);
        return defaultHacker.getId();
    }
}
