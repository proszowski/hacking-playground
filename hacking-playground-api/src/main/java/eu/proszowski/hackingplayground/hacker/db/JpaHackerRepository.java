package eu.proszowski.hackingplayground.hacker.db;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface JpaHackerRepository extends JpaRepository<HackerDbDto, UUID> {
    boolean existsByName(String name);
}
