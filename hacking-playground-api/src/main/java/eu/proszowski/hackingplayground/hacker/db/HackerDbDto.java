package eu.proszowski.hackingplayground.hacker.db;

import eu.proszowski.hackingplayground.hacker.domain.Hacker;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "hackers")
public class HackerDbDto {

    @Id
    private UUID id;
    private String name;

    public static HackerDbDto fromModel(final Hacker hacker) {
        return new HackerDbDto(hacker.getId().getRaw(), hacker.getName());
    }
}
