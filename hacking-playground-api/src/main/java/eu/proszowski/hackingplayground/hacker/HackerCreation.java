package eu.proszowski.hackingplayground.hacker;

import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import javax.validation.Valid;

@Value
@Builder(toBuilder = true)
public class HackerCreation {

    @Valid
    @NonNull
    private HackerId id;

    @NonNull
    private String name;

}
