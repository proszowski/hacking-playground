package eu.proszowski.hackingplayground.hacker;

import lombok.NonNull;

public class HackerLogin {

    @NonNull
    private String name;

    @NonNull
    private String password;
}
