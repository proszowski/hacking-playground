package eu.proszowski.hackingplayground.hacker.domain;

import lombok.NonNull;
import lombok.Value;
import java.util.UUID;

@Value(staticConstructor = "fromUUID")
public class HackerId {
    @NonNull
    private UUID raw;
}
