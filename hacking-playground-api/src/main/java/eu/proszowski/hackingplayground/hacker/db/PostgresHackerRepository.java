package eu.proszowski.hackingplayground.hacker.db;

import eu.proszowski.hackingplayground.hacker.domain.*;

import java.util.List;
import java.util.stream.Collectors;

public class PostgresHackerRepository implements HackerRepository {

    private final JpaHackerRepository jpaHackerRepository;

    public PostgresHackerRepository(final JpaHackerRepository jpaHackerRepository) {
        this.jpaHackerRepository = jpaHackerRepository;
    }

    @Override
    public List<Hacker> findAll() {
        return jpaHackerRepository.findAll()
                .stream()
                .map(this::createHacker)
                .collect(Collectors.toList());
    }

    private DefaultHacker createHacker(final HackerDbDto dto) {
        return DefaultHacker.builder()
                .id(HackerId.fromUUID(dto.getId()))
                .name(dto.getName())
                .build();
    }

    @Override
    public void save(final Hacker hacker) {
        jpaHackerRepository.save(HackerDbDto.fromModel(hacker));
    }

    @Override
    public boolean existsByName(final String name) {
        return jpaHackerRepository.existsByName(name);
    }
}
