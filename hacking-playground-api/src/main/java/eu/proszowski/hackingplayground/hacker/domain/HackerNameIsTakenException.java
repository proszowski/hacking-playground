package eu.proszowski.hackingplayground.hacker.domain;

public class HackerNameIsTakenException extends RuntimeException{
    HackerNameIsTakenException(final String name) {
        super(String.format("There is already registred hacker with name %s", name));
    }
}
