package eu.proszowski.hackingplayground.hacker.domain;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import javax.validation.Valid;

@Value
@Builder
public class DefaultHacker implements Hacker {

    @Valid
    @NonNull
    private HackerId id;

    @NonNull
    private String name;
}
