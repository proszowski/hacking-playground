package eu.proszowski.hackingplayground.hacker.domain;

public interface Hacker {
    HackerId getId();

    String getName();
}
