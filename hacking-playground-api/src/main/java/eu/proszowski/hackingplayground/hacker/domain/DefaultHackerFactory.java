package eu.proszowski.hackingplayground.hacker.domain;

import eu.proszowski.hackingplayground.hacker.HackerCreation;

public class DefaultHackerFactory implements HackerFactory {
    @Override
    public Hacker create(final HackerCreation hackerCreation) {
        return DefaultHacker.builder()
                .id(hackerCreation.getId())
                .name(hackerCreation.getName())
                .build();
    }
}
