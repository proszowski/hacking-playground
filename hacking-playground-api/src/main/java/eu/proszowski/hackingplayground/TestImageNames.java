package eu.proszowski.hackingplayground;

public class TestImageNames {
    public static final String FLANKI = "powder13/the-hacking-playground:flanki";
    public static final String PHP_EXAMPLE = "powder13/the-hacking-playground:php-example";
    public static final String PHP_HELLO_WORLD = "powder13/the-hacking-playground:php-helloworld";
}
