package eu.proszowski.hackingplayground.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LinksConverter {

    public static String getHtmlWithFixedUrls(final String html, final String baseUrl) {
        final Document doc = Jsoup.parse(html);

        final Elements links = doc.select("a[href]");
        final Elements media = doc.select("[src]");
        final Elements styles = doc.select("link");
        final Elements forms = doc.select("form");

        links.forEach( element -> fixUrl(baseUrl, element, "href"));
        media.forEach( element -> fixUrl(baseUrl, element, "src"));
        styles.forEach( element -> fixUrl(baseUrl, element, "href"));
        forms.forEach( element -> fixUrl(baseUrl, element, "action"));

        return doc.toString();
    }

    private static void fixUrl(final String baseUrl, final Element element, final String attributeName) {
        final String currentLink = element.attr(attributeName);
        if (doesNotlinkToExternalResource(currentLink) && !currentLink.contains(baseUrl)) {
            if (currentLink.startsWith("/")) {
                element.attr(attributeName, baseUrl + currentLink);
            } else {
                element.attr(attributeName, baseUrl + "/" + currentLink);
            }
        }
    }

    private static boolean doesNotlinkToExternalResource(final String currentLink) {
        return !currentLink.contains("http://") && !currentLink.contains("https://");
    }
}

