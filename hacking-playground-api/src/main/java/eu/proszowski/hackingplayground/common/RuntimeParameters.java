package eu.proszowski.hackingplayground.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
@AllArgsConstructor
public class RuntimeParameters {

    @Getter
    @Accessors(fluent = true)
    @Value("${shouldPullImages}")
    private boolean shouldPullImages;

    @Getter
    @Accessors(fluent = true)
    @Value("${shouldInitializeMockupScenario}")
    private boolean shouldInitializeMockupScenario;

    @Getter
    @Value("${refreshEndpoint}")
    private String refreshEndpoint;

    @Getter
    @Value("${configurationFileName}")
    private String configurationFileName;

    @Getter
    @Value("${git.password}")
    private String gitPassword;

    @Getter
    @Value("${git.email}")
    private String gitEmail;

    @Getter
    @Value("${git.repository}")
    private String gitRepository;

    @Getter
    @Accessors(fluent = true)
    @Value("${runFromJar}")
    private boolean runFromJar;
}
