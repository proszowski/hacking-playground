package eu.proszowski.hackingplayground.challenge.domain.exception;

public class UnableToStartContainerException extends ChallengeDomainException {
    public UnableToStartContainerException(final String containerId) {
        super(String.format("Unable to start container with id %s", containerId));
    }
}
