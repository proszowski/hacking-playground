package eu.proszowski.hackingplayground.challenge.domain;

import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeDetails;
import eu.proszowski.hackingplayground.challenge.domain.ports.Image;
import eu.proszowski.hackingplayground.challenge.domain.ports.SourceCode;
import eu.proszowski.hackingplayground.challenge.domain.ports.WriteUp;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import java.time.Instant;

@Value
@Builder(toBuilder = true)
public class DefaultChallengeDetails implements ChallengeDetails {

    @NonNull
    private Image image;

    @NonNull
    private SourceCode sourceCode;

    @NonNull
    private WriteUp writeUp;

    @NonNull
    @Builder.Default
    private Instant creationDate = Instant.now();
}
