package eu.proszowski.hackingplayground.challenge.domain.exception;

public class ImageAlreadyInstalledException extends ChallengeDomainException {

    public ImageAlreadyInstalledException() {
        super("Image already installed");
    }

    public ImageAlreadyInstalledException(final String imageName) {
        super(String.format("Image with challengeName %s has been already installed", imageName));
    }
}
