package eu.proszowski.hackingplayground.challenge.domain.exception;

import eu.proszowski.hackingplayground.challenge.domain.ports.ImageType;

public class ImageTypeNotRecognizedException extends ChallengeDomainException {
    public ImageTypeNotRecognizedException(final ImageType imageType) {
        super(String.format("Couldn't find installation commands for given type, image type: %s", imageType));
    }
}
