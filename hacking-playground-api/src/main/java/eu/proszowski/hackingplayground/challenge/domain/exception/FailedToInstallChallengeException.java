package eu.proszowski.hackingplayground.challenge.domain.exception;

public class FailedToInstallChallengeException extends ChallengeDomainException {
    public FailedToInstallChallengeException(final String challengeName) {
        super(String.format("Failed to install challenge with image challenge name: %s", challengeName));
    }
}
