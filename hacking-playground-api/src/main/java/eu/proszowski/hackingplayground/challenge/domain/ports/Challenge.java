package eu.proszowski.hackingplayground.challenge.domain.ports;

import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import java.util.Collection;

public interface Challenge {
    ChallengeId getId();

    String getName();

    String getDescription();

    ChallengeDetails getDetails();

    Collection<Tag> getTags();

    double getDifficulty();
}
