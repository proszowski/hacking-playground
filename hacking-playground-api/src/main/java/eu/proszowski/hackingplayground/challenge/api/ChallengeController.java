package eu.proszowski.hackingplayground.challenge.api;

import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("challenge")
public interface ChallengeController {

    @PostMapping("new-docker-compose")
    ChallengeId createDockerComposeChallenge(@RequestBody final DockerComposeChallengeCreation challengeCreation);

    @PostMapping("new-docker")
    ChallengeId createDockerChallenge(@RequestBody final DockerChallengeCreation challengeCreation);

    @GetMapping("all")
    List<ChallengeDto> getAllChallenges();

    @GetMapping("{id}")
    ChallengeDto getChallenge(@PathVariable final UUID challengeId);

}
