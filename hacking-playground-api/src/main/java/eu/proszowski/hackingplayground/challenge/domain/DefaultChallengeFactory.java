package eu.proszowski.hackingplayground.challenge.domain;

import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.UniqueIdProvider;
import eu.proszowski.hackingplayground.challenge.ChallengeCreation;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeDetails;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeFactory;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository;
import eu.proszowski.hackingplayground.challenge.domain.ports.Image;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import lombok.Builder;
import lombok.Value;
import java.util.List;
import java.util.stream.Collectors;

@Value
@Builder
class DefaultChallengeFactory implements ChallengeFactory {

    private final ChallengeRepository challengeRepository;

    @Override
    public Challenge createNewChallenge(final HackerId hackerId,
                                        final ChallengeDetails challengeDetails,
                                        final ChallengeCreation challengeCreation) {

        final String name = challengeCreation.getChallengeName();
        final String description = challengeCreation.getDescription();
        final List<String> tags = challengeCreation.getSuggestedTags();

        final ChallengeId challengeId = ChallengeId.fromUUID(UniqueIdProvider.next());

        final Image image = challengeDetails.getImage();

        final Image installedImage = image.install();

        final DefaultChallengeDetails installedChallengeDetails = ((DefaultChallengeDetails) challengeDetails).toBuilder()
                .image(installedImage)
                .build();

        final DefaultChallenge challenge = DefaultChallenge.builder()
                .id(challengeId)
                .name(name)
                .description(description)
                .tags(tags.stream().map(DefaultTag::new).collect(Collectors.toList()))
                .details(installedChallengeDetails)
                .build();

        challengeRepository.save(challenge);

        return challenge;
    }

    @Override
    public Challenge withNewDifficulty(final Challenge challenge, final double difficulty){
        final DefaultChallenge challengeWithNewDifficulty = ((DefaultChallenge) challenge).toBuilder()
                .difficulty(difficulty)
                .build();
        challengeRepository.save(challengeWithNewDifficulty);

        return challenge;
    }
}
