package eu.proszowski.hackingplayground.challenge.domain.ports;

public interface Image {
    ImageType getType();

    Image install();

}
