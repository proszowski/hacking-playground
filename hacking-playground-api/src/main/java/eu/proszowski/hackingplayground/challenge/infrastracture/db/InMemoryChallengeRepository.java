package eu.proszowski.hackingplayground.challenge.infrastracture.db;

import com.google.common.collect.Maps;
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository;
import lombok.Builder;
import lombok.Value;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Builder
@Value
public class InMemoryChallengeRepository implements ChallengeRepository {

    @Builder.Default
    private final Map<ChallengeId, Challenge> challenges = Maps.newHashMap();

    @Override
    public List<Challenge> findAll() {
        return new ArrayList<>(challenges.values());
    }

    @Override
    public Optional<Challenge> findById(final ChallengeId challengeId) {
        return Optional.ofNullable(challenges.get(challengeId));
    }

    @Override
    public ChallengeId save(final Challenge challenge) {
        challenges.put(challenge.getId(), challenge);
        return challenge.getId();
    }
}
