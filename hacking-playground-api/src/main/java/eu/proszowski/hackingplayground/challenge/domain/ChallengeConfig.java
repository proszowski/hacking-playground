package eu.proszowski.hackingplayground.challenge.domain;

import com.spotify.docker.client.DockerClient;
import eu.proszowski.hackingplayground.common.RuntimeParameters;
import eu.proszowski.hackingplayground.challenge.ChallengeFacade;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeDetailsMapper;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeFactory;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository;
import eu.proszowski.hackingplayground.flags.domain.FlagsFacade;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChallengeConfig {

    @Bean
    ChallengeFacade challengeFacade(final ChallengeRepository challengeRepository,
                                    final ChallengeDetailsMapper challengeDetailsMapper,
                                    final FlagsFacade flagsFacade) {
        final ChallengeFactory challengeFactory = DefaultChallengeFactory.builder()
                .challengeRepository(challengeRepository)
                .build();
        return new ChallengeFacade(challengeFactory, challengeRepository, challengeDetailsMapper, flagsFacade);
    }

    @Bean
    ChallengeDetailsMapper challengeDetailsMapper(final DockerClient dockerClient, final RuntimeParameters runtimeParameters) {
        return new DockerChallengeDetailsMapper(dockerClient, runtimeParameters);
    }
}
