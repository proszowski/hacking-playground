package eu.proszowski.hackingplayground.challenge.domain.ports;

public enum ImageType {
    DOCKER, DOCKER_COMPOSE
}
