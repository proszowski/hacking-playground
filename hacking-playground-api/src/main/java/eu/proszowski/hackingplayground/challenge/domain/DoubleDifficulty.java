package eu.proszowski.hackingplayground.challenge.domain;

import eu.proszowski.hackingplayground.challenge.domain.ports.Difficulty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.Value;

@Value
@NoArgsConstructor(access = AccessLevel.NONE)
class DoubleDifficulty implements Difficulty {
    static Difficulty basedOnDouble(final Double proposedDifficulty) {
        return new DoubleDifficulty();
    }
}
