package eu.proszowski.hackingplayground.challenge.domain.ports;

import eu.proszowski.hackingplayground.challenge.ChallengeCreation;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;

public interface ChallengeFactory {
    Challenge createNewChallenge(final HackerId hackerId,
                                 final ChallengeDetails challengeDetails,
                                 final ChallengeCreation dockerChallengeCreation);

    Challenge withNewDifficulty(final Challenge challenge, final double difficulty);

}
