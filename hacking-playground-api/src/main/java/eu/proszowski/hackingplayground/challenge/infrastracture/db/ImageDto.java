package eu.proszowski.hackingplayground.challenge.infrastracture.db;

import com.spotify.docker.client.DockerClient;
import eu.proszowski.hackingplayground.challenge.domain.ports.Image;
import eu.proszowski.hackingplayground.common.RuntimeParameters;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import java.util.UUID;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ImageDto {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    Image toModel(final String name, final DockerClient dockerClient, final RuntimeParameters runtimeParameters) {
        throw new UnsupportedOperationException();
    }
}
