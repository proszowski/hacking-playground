package eu.proszowski.hackingplayground.challenge.domain.ports;

import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import java.util.List;
import java.util.Optional;

public interface ChallengeRepository {
    List<Challenge> findAll();

    Optional<Challenge> findById(ChallengeId challengeId);

    ChallengeId save(Challenge challenge);
}
