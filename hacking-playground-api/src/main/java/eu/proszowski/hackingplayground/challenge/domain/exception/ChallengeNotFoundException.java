package eu.proszowski.hackingplayground.challenge.domain.exception;

import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;

public class ChallengeNotFoundException extends ChallengeDomainException {
    public ChallengeNotFoundException(final ChallengeId challengeId) {
        super(String.format("Challenge with id %s not found", challengeId.getRaw()));
    }
}
