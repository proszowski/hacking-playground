package eu.proszowski.hackingplayground.challenge.domain;

import lombok.Value;
import java.util.UUID;

@Value(staticConstructor = "fromUUID")
public class ChallengeId {
    private UUID raw;
}
