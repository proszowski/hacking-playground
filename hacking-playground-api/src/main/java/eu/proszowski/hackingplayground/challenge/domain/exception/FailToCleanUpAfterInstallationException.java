package eu.proszowski.hackingplayground.challenge.domain.exception;

public class FailToCleanUpAfterInstallationException extends ChallengeDomainException {
    public FailToCleanUpAfterInstallationException(final String challengeName) {
        super(String.format("Failed to clean up after installation of image with challenge name: %s", challengeName));
    }
}
