package eu.proszowski.hackingplayground.challenge.domain.exception;

public class UnableToCreateContainerException extends ChallengeDomainException {
    public UnableToCreateContainerException(final String imageName) {
        super(String.format("Unable to create container for image with challengeName %s", imageName));
    }
}
