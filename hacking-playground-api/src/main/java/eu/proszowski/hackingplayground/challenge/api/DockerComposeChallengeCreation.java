package eu.proszowski.hackingplayground.challenge.api;

import eu.proszowski.hackingplayground.challenge.ChallengeCreation;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import org.hibernate.validator.constraints.Range;
import java.util.List;

@Builder
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor
public class DockerComposeChallengeCreation implements ChallengeCreation {

    @NonNull
    private String challengeName;

    @NonNull
    private String description;

    @NonNull
    private String sourceCodeUrl;

    @NonNull
    private String writeUpUrl;

    @NonNull
    private List<String> suggestedTags;

    @NonNull
    private List<String> flags;

    @Range(min = 0, max = 65535)
    private int exposedPort;

    @NonNull
    private String exposedServiceName;

    @NonNull
    private String dockerComposeYaml;

}
