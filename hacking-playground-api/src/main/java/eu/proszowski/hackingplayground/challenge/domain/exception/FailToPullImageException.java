package eu.proszowski.hackingplayground.challenge.domain.exception;

public class FailToPullImageException extends ChallengeDomainException {
    public FailToPullImageException(final String imageName) {
        super(String.format("Unable to pull image with name %s ", imageName));
    }
}
