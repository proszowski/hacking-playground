package eu.proszowski.hackingplayground.challenge.domain;

import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.challenge.domain.ports.Tag;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeDetails;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import java.util.Collections;
import java.util.List;

@Value
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class DefaultChallenge implements Challenge {

    @NonNull
    private ChallengeId id;

    @NonNull
    private String description;

    @NonNull
    private String name;

    @NonNull
    private ChallengeDetails details;

    @NonNull
    @Builder.Default
    private List<Tag> tags = Collections.emptyList();

    @Builder.Default
    private double difficulty = 1000;
}
