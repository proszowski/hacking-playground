package eu.proszowski.hackingplayground.challenge.domain.ports;

public interface Tag {
    String getName();
}
