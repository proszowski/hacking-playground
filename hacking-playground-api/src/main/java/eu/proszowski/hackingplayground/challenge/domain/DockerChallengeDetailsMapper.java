package eu.proszowski.hackingplayground.challenge.domain;

import com.spotify.docker.client.DockerClient;
import eu.proszowski.hackingplayground.challenge.InstallationCommandsFactory;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeDetailsMapper;
import eu.proszowski.hackingplayground.common.RuntimeParameters;
import eu.proszowski.hackingplayground.challenge.ChallengeCreation;
import eu.proszowski.hackingplayground.challenge.api.DockerChallengeCreation;
import eu.proszowski.hackingplayground.challenge.api.DockerComposeChallengeCreation;
import eu.proszowski.hackingplayground.challenge.domain.exception.InvalidChallengeCreationException;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeDetails;

class DockerChallengeDetailsMapper implements ChallengeDetailsMapper {

    private final DockerClient dockerClient;
    private final RuntimeParameters runtimeParameters;

    DockerChallengeDetailsMapper(final DockerClient dockerClient, final RuntimeParameters runtimeParameters) {
        this.dockerClient = dockerClient;
        this.runtimeParameters = runtimeParameters;
    }

    @Override
    public ChallengeDetails toModel(final ChallengeCreation challengeCreation) {
        if (challengeCreation instanceof DockerComposeChallengeCreation) {
            return toModel((DockerComposeChallengeCreation) challengeCreation);
        } else if (challengeCreation instanceof DockerChallengeCreation) {
            return toModel((DockerChallengeCreation) challengeCreation);
        } else {
            throw new InvalidChallengeCreationException();
        }
    }

    private ChallengeDetails toModel(final DockerComposeChallengeCreation dockerComposeChallengeCreation) {
        return DefaultChallengeDetails.builder()
                .sourceCode(UrlSourceCode.fromUrl(dockerComposeChallengeCreation.getSourceCodeUrl()))
                .writeUp(UrlWriteUp.fromUrl(dockerComposeChallengeCreation.getWriteUpUrl()))
                .image(DockerComposeImage.builder()
                        .exposedServiceName(dockerComposeChallengeCreation.getExposedServiceName())
                        .exposedPort(dockerComposeChallengeCreation.getExposedPort())
                        .dockerComposeConfig(dockerComposeChallengeCreation.getDockerComposeYaml())
                        .installationCommands(InstallationCommandsFactory.composeInstance(dockerComposeChallengeCreation.getDockerComposeYaml(),
                                dockerComposeChallengeCreation.getChallengeName(),
                                dockerClient,
                                runtimeParameters))
                        .build())
                .build();
    }

    private ChallengeDetails toModel(final DockerChallengeCreation dockerChallengeCreation) {
        return DefaultChallengeDetails.builder()
                .sourceCode(UrlSourceCode.fromUrl(dockerChallengeCreation.getSourceCodeUrl()))
                .writeUp(UrlWriteUp.fromUrl(dockerChallengeCreation.getWriteUpUrl()))
                .image(DockerImage.builder()
                        .name(dockerChallengeCreation.getImageName())
                        .port(dockerChallengeCreation.getImagePort())
                        .installationCommands(InstallationCommandsFactory.singleImageInstance(dockerChallengeCreation.getImageName(),
                                dockerChallengeCreation.getChallengeName(),
                                dockerClient,
                                runtimeParameters))
                        .build())
                .build();
    }

}
