package eu.proszowski.hackingplayground.challenge.api;

import eu.proszowski.hackingplayground.challenge.ChallengeFacade;
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.authentication.AuthenticationService;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.UUID;

@Component
public class DefaultChallengeController implements ChallengeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private ChallengeFacade challengeFacade;

    @Override
    public ChallengeId createDockerComposeChallenge(final DockerComposeChallengeCreation challengeCreation) {
        final HackerId hackerId = HackerId.fromUUID(authenticationService.getUserId());

        return challengeFacade.createNewChallenge(hackerId, challengeCreation);
    }

    @Override
    public ChallengeId createDockerChallenge(final DockerChallengeCreation challengeCreation) {
        final HackerId hackerId = HackerId.fromUUID(authenticationService.getUserId());

        return challengeFacade.createNewChallenge(hackerId, challengeCreation);
    }

    @Override
    public List<ChallengeDto> getAllChallenges() {
        return challengeFacade.getAllChallenges();
    }

    @Override
    public ChallengeDto getChallenge(final UUID challengeId) {
        return challengeFacade.getChallenge(ChallengeId.fromUUID(challengeId));
    }

}
