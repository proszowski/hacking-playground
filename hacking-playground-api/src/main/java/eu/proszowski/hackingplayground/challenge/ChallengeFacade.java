package eu.proszowski.hackingplayground.challenge;

import eu.proszowski.hackingplayground.challenge.api.ChallengeDto;
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.challenge.domain.exception.ChallengeNotFoundException;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeDetails;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeDetailsMapper;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeFactory;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository;
import eu.proszowski.hackingplayground.flags.domain.BaseId;
import eu.proszowski.hackingplayground.flags.domain.DefaultBaseId;
import eu.proszowski.hackingplayground.flags.domain.DefaultFlag;
import eu.proszowski.hackingplayground.flags.domain.Flag;
import eu.proszowski.hackingplayground.flags.domain.FlagsFacade;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public class ChallengeFacade {

    private static final int DIFFICULTY_DECREASE_CONSTANT = 5;
    private final ChallengeFactory challengeFactory;

    private final ChallengeRepository challengeRepository;

    private final ChallengeDetailsMapper challengeDetailsMapper;

    private final FlagsFacade flagsFacade;

    public ChallengeFacade(final ChallengeFactory challengeFactory,
                           final ChallengeRepository challengeRepository,
                           final ChallengeDetailsMapper challengeDetailsMapper,
                           final FlagsFacade flagsFacade) {
        this.challengeFactory = challengeFactory;
        this.challengeRepository = challengeRepository;
        this.challengeDetailsMapper = challengeDetailsMapper;
        this.flagsFacade = flagsFacade;
    }

    public ChallengeId createNewChallenge(final HackerId hackerId,
                                          final ChallengeCreation challengeCreation) {

        final ChallengeDetails challengeDetails = challengeDetailsMapper.toModel(challengeCreation);

        final Challenge challenge = challengeFactory.createNewChallenge(hackerId, challengeDetails, challengeCreation);
        final BaseId baseId = DefaultBaseId.fromUUID(challenge.getId().getRaw());
        final Set<Flag> flags = challengeCreation.getFlags().stream().map(DefaultFlag::fromString).collect(Collectors.toSet());
        flagsFacade.associateBaseIdWithFlags(baseId, flags);
        flagsFacade.subscribeForBaseCapture(baseId.getRaw(), this::decreaseDifficultyOfChallenge);
        return challenge.getId();
    }

    public List<ChallengeDto> getAllChallenges() {
        return challengeRepository.findAll()
                .stream()
                .map(ChallengeDto::fromModel)
                .collect(Collectors.toList());
    }

    private void decreaseDifficultyOfChallenge(final UUID id) {
        final ChallengeId challengeId = ChallengeId.fromUUID(id);
        final Challenge challenge = challengeRepository.findById(challengeId)
                .orElseThrow(() -> new ChallengeNotFoundException(challengeId));
        challengeFactory.withNewDifficulty(challenge, challenge.getDifficulty() - DIFFICULTY_DECREASE_CONSTANT);
    }

    public ChallengeDto getChallenge(final ChallengeId challengeId) {
        return challengeRepository.findById(challengeId)
                .map(ChallengeDto::fromModel)
                .orElseThrow(() -> new ChallengeNotFoundException(challengeId));
    }
}
