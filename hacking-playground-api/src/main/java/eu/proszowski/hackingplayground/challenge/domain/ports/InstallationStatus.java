package eu.proszowski.hackingplayground.challenge.domain.ports;

public enum InstallationStatus {
    INSTALLED, NOT_INSTALLED
}
