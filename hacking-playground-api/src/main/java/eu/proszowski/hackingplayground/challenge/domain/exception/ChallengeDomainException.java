package eu.proszowski.hackingplayground.challenge.domain.exception;

public class ChallengeDomainException extends RuntimeException{
    ChallengeDomainException(final String message) {
        super(message);
    }
}
