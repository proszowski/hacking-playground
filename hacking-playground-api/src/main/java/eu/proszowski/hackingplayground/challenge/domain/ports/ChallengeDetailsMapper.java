package eu.proszowski.hackingplayground.challenge.domain.ports;

import eu.proszowski.hackingplayground.challenge.ChallengeCreation;

public interface ChallengeDetailsMapper {
    ChallengeDetails toModel(final ChallengeCreation challengeCreation);
}
