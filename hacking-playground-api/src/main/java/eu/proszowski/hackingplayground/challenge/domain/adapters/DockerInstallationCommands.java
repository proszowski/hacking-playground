package eu.proszowski.hackingplayground.challenge.domain.adapters;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import com.spotify.docker.client.messages.ContainerCreation;
import eu.proszowski.hackingplayground.challenge.domain.exception.FailToCleanUpAfterInstallationException;
import eu.proszowski.hackingplayground.challenge.domain.exception.FailToPullImageException;
import eu.proszowski.hackingplayground.challenge.domain.exception.FailedToInstallChallengeException;
import eu.proszowski.hackingplayground.challenge.domain.exception.UnableToCreateContainerException;
import eu.proszowski.hackingplayground.challenge.domain.exception.UnableToStartContainerException;
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationCommands;
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationStatus;
import lombok.Builder;
import lombok.NonNull;
import java.util.Optional;

@Builder
public class DockerInstallationCommands implements InstallationCommands {

    public static final String EXPECTED_STATUS = "running";
    private static final int MAX_RETRIES = 10;

    @NonNull
    private final DockerClient dockerClient;

    @NonNull
    private final String imageName;

    @NonNull
    private final String challengeName;

    private boolean shouldPullImage;

    @Override
    public InstallationStatus run() {

        //TODO: check if image exists in local registry
        if(shouldPullImage){
            pullImage(imageName);
        }
        final ContainerCreation creation = createContainer(imageName);
        final String containerId = creation.id();
        startContainer(containerId);
        checkStatusOfContainer(containerId, imageName);
        cleanUpAfterInstallation(containerId, imageName);
        return InstallationStatus.INSTALLED;
        //TODO: Push image to private repository created with special purpose for THP
    }

    private void startContainer(final String containerId) {
        try {
            dockerClient.startContainer(containerId);
        } catch (InterruptedException | DockerException e) {
            throw new UnableToStartContainerException(containerId);
        }
    }

    private void pullImage(final String imageName) {
        try {
            dockerClient.pull(imageName);
        } catch (final InterruptedException | DockerException e) {
            throw new FailToPullImageException(imageName);
        }
    }

    private ContainerCreation createContainer(final String imageName) {
        final ContainerConfig config = ContainerConfig.builder()
                .addVolume("/tmp")
                .image(imageName)
                .build();
        try {
            return dockerClient.createContainer(config);
        } catch (final DockerException | InterruptedException e) {
            throw new UnableToCreateContainerException(imageName);
        }
    }

    private void cleanUpAfterInstallation(final String containerId, final String imageName) {
        try {
            dockerClient.killContainer(containerId);
            dockerClient.removeContainer(containerId);
        } catch (final DockerException | InterruptedException e) {
            throw new FailToCleanUpAfterInstallationException(imageName);
        }
    }

    private void checkStatusOfContainer(final String containerId, final String imageName) {
        try {
            final String status = Optional.ofNullable(dockerClient.inspectContainer(containerId).state().status())
                    .orElse("");
            if (!EXPECTED_STATUS.equals(status)) {
                throw new ContainerHasWrongStatusException();
            }
        } catch (final DockerException | InterruptedException e) {
            throw new FailedToInstallChallengeException(challengeName);
        }
    }

    private static class ContainerHasWrongStatusException extends RuntimeException {
    }
}
