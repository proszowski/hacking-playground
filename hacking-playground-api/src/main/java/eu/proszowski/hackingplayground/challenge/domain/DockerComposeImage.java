package eu.proszowski.hackingplayground.challenge.domain;

import eu.proszowski.hackingplayground.challenge.domain.exception.ImageAlreadyInstalledException;
import eu.proszowski.hackingplayground.challenge.domain.ports.Image;
import eu.proszowski.hackingplayground.challenge.domain.ports.ImageType;
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationCommands;
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationStatus;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class DockerComposeImage implements Image {

    private ImageType type = ImageType.DOCKER_COMPOSE;

    @NonNull
    private InstallationCommands installationCommands;

    @NonNull
    private String dockerComposeConfig;

    @NonNull
    private String exposedServiceName;

    private int exposedPort;

    @Builder.Default
    private InstallationStatus installationStatus = InstallationStatus.NOT_INSTALLED;

    @Override
    public Image install() {
        if(installationStatus == InstallationStatus.INSTALLED){
            throw new ImageAlreadyInstalledException();
        }
        installationCommands.run();

        return toBuilder()
                .installationStatus(InstallationStatus.INSTALLED)
                .build();
    }
}
