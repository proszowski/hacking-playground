package eu.proszowski.hackingplayground.challenge.domain;

import com.github.dockerjava.api.model.Container;
import com.spotify.docker.client.DockerClient;
import eu.proszowski.hackingplayground.challenge.domain.exception.FailedToInstallChallengeException;
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationCommands;
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationStatus;
import eu.proszowski.hackingplayground.game.domain.DockerComposeContainerChildrenRetriever;
import lombok.Builder;
import lombok.NonNull;
import org.testcontainers.containers.DockerComposeContainer;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Builder
public class DockerComposeInstallationCommands implements InstallationCommands {
    @NonNull
    private final DockerClient dockerClient;
    @NonNull
    private final String dockerComposeConfiguration;
    @NonNull
    private final String challengeName;

    private final boolean shouldPullImage;

    @Override
    public InstallationStatus run() {
        try {
            final File file = File.createTempFile(UUID.randomUUID().toString(), UUID.randomUUID().toString());
            Files.write(Paths.get(file.getPath()), dockerComposeConfiguration.getBytes());
            final DockerComposeContainer dockerComposeContainer = new DockerComposeContainer(file).withPull(shouldPullImage);
            dockerComposeContainer.start();
            final List<Container> childContainers = DockerComposeContainerChildrenRetriever.getChildContainers(dockerComposeContainer);
            if (childContainers.isEmpty()) {
                throw new FailedToInstallChallengeException(challengeName);
            }
            dockerComposeContainer.stop();
        } catch (final Exception e) {
            throw new FailedToInstallChallengeException(challengeName);
        }
        return InstallationStatus.INSTALLED;
    }
}
