package eu.proszowski.hackingplayground.challenge;

import com.spotify.docker.client.DockerClient;
import eu.proszowski.hackingplayground.challenge.domain.DockerComposeInstallationCommands;
import eu.proszowski.hackingplayground.challenge.domain.adapters.DockerInstallationCommands;
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationCommands;
import eu.proszowski.hackingplayground.common.RuntimeParameters;

public class InstallationCommandsFactory {

    public static InstallationCommands composeInstance(final String dockerComposeConfig,
                                                       final String challengeName,
                                                       final DockerClient dockerClient,
                                                       final RuntimeParameters runtimeParameters) {
        return DockerComposeInstallationCommands.builder()
                .dockerClient(dockerClient)
                .dockerComposeConfiguration(dockerComposeConfig)
                .challengeName(challengeName)
                .shouldPullImage(runtimeParameters.shouldPullImages())
                .build();
    }

    public static InstallationCommands singleImageInstance(final String imageName,
                                                           final String challengeName,
                                                           final DockerClient dockerClient,
                                                           final RuntimeParameters runtimeParameters
    ) {
        return DockerInstallationCommands.builder()
                .imageName(imageName)
                .dockerClient(dockerClient)
                .challengeName(challengeName)
                .shouldPullImage(runtimeParameters.shouldPullImages())
                .build();
    }
}
