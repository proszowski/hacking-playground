package eu.proszowski.hackingplayground.challenge.domain;

import eu.proszowski.hackingplayground.challenge.domain.ports.Tag;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class DefaultTag implements Tag {
    private String name;
}
