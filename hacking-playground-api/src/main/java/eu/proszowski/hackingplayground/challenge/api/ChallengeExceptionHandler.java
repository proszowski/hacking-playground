package eu.proszowski.hackingplayground.challenge.api;

import eu.proszowski.hackingplayground.challenge.domain.exception.ChallengeDomainException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.lang.invoke.MethodHandles;

@ControllerAdvice
public class ChallengeExceptionHandler extends ResponseEntityExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());


    @ExceptionHandler(value = {ChallengeDomainException.class} )
    protected ResponseEntity<Object> handleConflict(final RuntimeException ex,
                                                    final WebRequest request) {

        final String bodyOfResponse = ex.getMessage();
        logger.info("{}", bodyOfResponse);
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}
