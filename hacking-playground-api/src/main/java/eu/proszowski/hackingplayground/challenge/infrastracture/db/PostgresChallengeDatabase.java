package eu.proszowski.hackingplayground.challenge.infrastracture.db;

import com.spotify.docker.client.DockerClient;
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository;
import eu.proszowski.hackingplayground.common.RuntimeParameters;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public class PostgresChallengeDatabase implements ChallengeRepository {

    private final JpaChallengeRepository jpaChallengeRepository;
    private final DockerClient dockerClient;
    private final RuntimeParameters runtimeParameters;

    public PostgresChallengeDatabase(final JpaChallengeRepository jpaChallengeRepository,
                                     final DockerClient dockerClient,
                                     final RuntimeParameters runtimeParameters) {
        this.jpaChallengeRepository = jpaChallengeRepository;
        this.dockerClient = dockerClient;
        this.runtimeParameters = runtimeParameters;
    }

    @Override
    public List<Challenge> findAll() {
        return jpaChallengeRepository.findAll()
                .stream()
                .map(a -> ChallengeDbDto.mapToModel(a, dockerClient, runtimeParameters))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Challenge> findById(final ChallengeId challengeId) {
        return jpaChallengeRepository.findById(challengeId.getRaw())
                .map(a -> ChallengeDbDto.mapToModel(a, dockerClient, runtimeParameters));
    }

    @Override
    public ChallengeId save(final Challenge challenge) {
        final ChallengeDbDto saved = jpaChallengeRepository.save(ChallengeDbDto.fromChallenge(challenge));
        return ChallengeId.fromUUID(saved.getId());
    }
}
