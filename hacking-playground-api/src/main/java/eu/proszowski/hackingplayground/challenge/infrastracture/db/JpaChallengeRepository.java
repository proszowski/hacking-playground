package eu.proszowski.hackingplayground.challenge.infrastracture.db;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface JpaChallengeRepository extends JpaRepository<ChallengeDbDto, UUID> {
}
