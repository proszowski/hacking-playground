package eu.proszowski.hackingplayground.challenge.api;

import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.challenge.domain.ports.Tag;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.hibernate.validator.constraints.Range;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Value
@Builder
public class ChallengeDto {
    @NonNull
    private UUID id;
    @NonNull
    private String name;
    @NonNull
    private String description;
    @NonNull
    private List<String> tags;

    @Range(min = 1, max = 1000)
    private double difficulty;

    public static ChallengeDto fromModel(final Challenge challenge) {
        return ChallengeDto.builder()
                .id(challenge.getId().getRaw())
                .name(challenge.getName())
                .description(challenge.getDescription())
                .tags(challenge.getTags().stream().map(Tag::getName).collect(Collectors.toList()))
                .difficulty(challenge.getDifficulty())
                .build();
    }
}
