package eu.proszowski.hackingplayground.challenge.domain;

import eu.proszowski.hackingplayground.challenge.domain.exception.ImageAlreadyInstalledException;
import eu.proszowski.hackingplayground.challenge.domain.ports.Image;
import eu.proszowski.hackingplayground.challenge.domain.ports.ImageType;
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationCommands;
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationStatus;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(toBuilder = true)
public class DockerImage implements Image {

    @NonNull
    private String name;

    @NonNull
    private Integer port;

    private final ImageType type = ImageType.DOCKER;

    @NonNull
    private InstallationCommands installationCommands;

    @Builder.Default
    private InstallationStatus installationStatus = InstallationStatus.NOT_INSTALLED;

    @Override
    public Image install() {
        if(installationStatus == InstallationStatus.INSTALLED){
            throw new ImageAlreadyInstalledException(name);
        }

        final InstallationStatus installationStatus = installationCommands.run();
        return toBuilder()
                .installationStatus(InstallationStatus.INSTALLED)
                .build();
    }
}
