package eu.proszowski.hackingplayground.challenge.domain.ports;

public interface InstallationCommands {
    InstallationStatus run();
}
