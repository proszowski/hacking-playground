package eu.proszowski.hackingplayground.challenge.domain;

import eu.proszowski.hackingplayground.challenge.domain.ports.WriteUp;
import lombok.Value;

@Value
public class UrlWriteUp implements WriteUp {

    private String url;

    public static WriteUp fromUrl(final String writeUpUrl) {
        return new UrlWriteUp(writeUpUrl);
    }
}
