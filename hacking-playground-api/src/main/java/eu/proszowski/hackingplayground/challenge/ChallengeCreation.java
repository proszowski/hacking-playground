package eu.proszowski.hackingplayground.challenge;

import java.util.List;

public interface ChallengeCreation {
    List<String> getFlags();

    String getChallengeName();

    String getDescription();

    List<String> getSuggestedTags();
}
