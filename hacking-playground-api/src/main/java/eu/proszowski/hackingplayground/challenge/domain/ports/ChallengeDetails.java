package eu.proszowski.hackingplayground.challenge.domain.ports;

import java.time.Instant;

public interface ChallengeDetails {

    Image getImage();

    SourceCode getSourceCode();

    WriteUp getWriteUp();

    Instant getCreationDate();
}
