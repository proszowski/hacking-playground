package eu.proszowski.hackingplayground.challenge.domain;

import eu.proszowski.hackingplayground.challenge.domain.ports.SourceCode;
import lombok.Value;

@Value
public class UrlSourceCode implements SourceCode {
    private String url;

    public static SourceCode fromUrl(final String sourceCodeUrl) {
        return new UrlSourceCode(sourceCodeUrl);
    }
}
