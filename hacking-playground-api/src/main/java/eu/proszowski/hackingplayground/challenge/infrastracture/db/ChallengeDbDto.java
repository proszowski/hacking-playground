package eu.proszowski.hackingplayground.challenge.infrastracture.db;

import com.spotify.docker.client.DockerClient;
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.challenge.domain.DefaultChallenge;
import eu.proszowski.hackingplayground.challenge.domain.DefaultChallengeDetails;
import eu.proszowski.hackingplayground.challenge.domain.DefaultTag;
import eu.proszowski.hackingplayground.challenge.domain.DockerComposeImage;
import eu.proszowski.hackingplayground.challenge.domain.DockerComposeInstallationCommands;
import eu.proszowski.hackingplayground.challenge.domain.DockerImage;
import eu.proszowski.hackingplayground.challenge.domain.UrlSourceCode;
import eu.proszowski.hackingplayground.challenge.domain.UrlWriteUp;
import eu.proszowski.hackingplayground.challenge.domain.adapters.DockerInstallationCommands;
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeDetails;
import eu.proszowski.hackingplayground.challenge.domain.ports.Image;
import eu.proszowski.hackingplayground.challenge.domain.ports.ImageType;
import eu.proszowski.hackingplayground.challenge.domain.ports.InstallationStatus;
import eu.proszowski.hackingplayground.challenge.domain.ports.Tag;
import eu.proszowski.hackingplayground.common.RuntimeParameters;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "challenges")
@Builder
public class ChallengeDbDto {

    @Id
    private UUID id;

    private String challengeName;

    @Column(length = 1000)
    private String description;

    @ElementCollection
    private Set<String> tags;

    private double difficulty;

    private String sourceCodeUrl;

    private String writeUpUrl;

    private Instant creationDate;

    private ImageType type;

    @Column(length = 1000)
    private String dockerComposeConfig;

    private String exposedServiceName;

    private int exposedPort;

    private InstallationStatus installationStatus;

    private String imageName;

    private Integer port;

    public static ChallengeDbDto fromChallenge(final Challenge challenge) {
        final ChallengeDbDto.ChallengeDbDtoBuilder challengeDtoBuilder = ChallengeDbDto.builder()
                .id(challenge.getId().getRaw())
                .challengeName(challenge.getName())
                .description(challenge.getDescription())
                .difficulty(challenge.getDifficulty())
                .tags(challenge.getTags().stream().map(Tag::getName).collect(Collectors.toSet()))
                .creationDate(challenge.getDetails().getCreationDate())
                .sourceCodeUrl(((UrlSourceCode) challenge.getDetails().getSourceCode()).getUrl())
                .writeUpUrl(((UrlWriteUp) challenge.getDetails().getWriteUp()).getUrl());

        addImageInfo(challenge, challengeDtoBuilder);

        return challengeDtoBuilder.build();
    }

    private static void addImageInfo(final Challenge challenge,
                                     final ChallengeDbDto.ChallengeDbDtoBuilder challengeDtoBuilder) {
        final Image image = challenge.getDetails().getImage();
        final ImageType imageType = image.getType();

        challengeDtoBuilder.type(image.getType());

        switch (imageType){
            case DOCKER:
                final DockerImage dockerImage = (DockerImage) image;
                challengeDtoBuilder
                        .imageName(dockerImage.getName())
                        .port(dockerImage.getPort())
                        .installationStatus(dockerImage.getInstallationStatus());
                break;
            case DOCKER_COMPOSE:
                final DockerComposeImage dockerComposeImage = (DockerComposeImage) image;
                challengeDtoBuilder
                        .dockerComposeConfig(dockerComposeImage.getDockerComposeConfig())
                        .exposedPort(dockerComposeImage.getExposedPort())
                        .installationStatus(dockerComposeImage.getInstallationStatus())
                        .exposedServiceName(dockerComposeImage.getExposedServiceName());
                break;
            default:
                throw new UnsupportedOperationException();
        }
    }



    public static Challenge mapToModel(final ChallengeDbDto challenge,
                                       final DockerClient dockerClient,
                                       final RuntimeParameters runtimeParameters) {
        return DefaultChallenge.builder()
                .id(ChallengeId.fromUUID(challenge.getId()))
                .name(challenge.getChallengeName())
                .tags(challenge.getTags().stream().map(a -> DefaultTag.builder().name(a).build()).collect(Collectors.toList()))
                .difficulty(challenge.getDifficulty())
                .description(challenge.getDescription())
                .details(getDetails(challenge, dockerClient, runtimeParameters))
                .build();
    }

    private static ChallengeDetails getDetails(final ChallengeDbDto challenge,
                                               final DockerClient dockerClient,
                                               final RuntimeParameters runtimeParameters) {
        return DefaultChallengeDetails.builder()
                .sourceCode(UrlSourceCode.fromUrl(challenge.getSourceCodeUrl()))
                .writeUp(UrlWriteUp.fromUrl(challenge.getWriteUpUrl()))
                .creationDate(challenge.getCreationDate())
                .image(getImage(challenge, dockerClient, runtimeParameters))
                .build();
    }

    private static Image getImage(final ChallengeDbDto challenge,
                                  final DockerClient dockerClient,
                                  final RuntimeParameters runtimeParameters) {

        if(challenge.getType() == ImageType.DOCKER){
            return DockerImage.builder()
                    .port(challenge.getPort())
                    .name(challenge.getImageName())
                    .installationStatus(challenge.getInstallationStatus())
                    .installationCommands(DockerInstallationCommands.builder()
                            .challengeName(challenge.getChallengeName())
                            .imageName(challenge.getImageName())
                            .dockerClient(dockerClient)
                            .shouldPullImage(runtimeParameters.shouldPullImages())
                            .build())
                    .build();
        }else if(challenge.getType() == ImageType.DOCKER_COMPOSE){
            return DockerComposeImage.builder()
                    .installationStatus(challenge.getInstallationStatus())
                    .exposedServiceName(challenge.getExposedServiceName())
                    .exposedPort(challenge.getExposedPort())
                    .dockerComposeConfig(challenge.getDockerComposeConfig())
                    .installationCommands(DockerComposeInstallationCommands.builder()
                            .challengeName(challenge.getChallengeName())
                            .dockerClient(dockerClient)
                            .shouldPullImage(runtimeParameters.shouldPullImages())
                            .dockerComposeConfiguration(challenge.getDockerComposeConfig())
                            .build()).build();
        }

        throw new UnsupportedOperationException();
    }

}
