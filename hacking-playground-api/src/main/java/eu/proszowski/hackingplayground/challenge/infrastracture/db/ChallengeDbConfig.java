package eu.proszowski.hackingplayground.challenge.infrastracture.db;

import com.spotify.docker.client.DockerClient;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository;
import eu.proszowski.hackingplayground.common.RuntimeParameters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChallengeDbConfig {

    @Bean
    ChallengeRepository challengeRepository(final JpaChallengeRepository jpaChallengeRepository,
                                            final DockerClient dockerClient,
                                            final RuntimeParameters runtimeParameters) {
        return new PostgresChallengeDatabase(jpaChallengeRepository, dockerClient, runtimeParameters);
    }
}
