package eu.proszowski.hackingplayground.game.domain.exception;

public class GameStartException extends RuntimeException {

    public GameStartException(final String message) {
        super(message);
    }

    public GameStartException(final Exception e) {
        super(String.format("Exception occured when starting game, cause: %s", e.getCause()));
    }
}
