package eu.proszowski.hackingplayground.game.domain.ports;

public interface UrlGenerator {
    String generate();
}
