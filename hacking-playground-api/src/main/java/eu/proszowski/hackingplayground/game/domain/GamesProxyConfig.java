package eu.proszowski.hackingplayground.game.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Iterables;
import com.google.common.io.Files;
import eu.proszowski.hackingplayground.common.RuntimeParameters;
import eu.proszowski.hackingplayground.game.domain.ports.Game;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GamesProxyConfig {

    private static final String REMOVE_COMMIT_MESSAGE_TEMPLATE = "Remove routing for game with id %s";
    private static final String ADD_COMMIT_MESSAGE_TEMPLATE = "Add routing for game with id %s";
    private static final String ROUTE_PATH_TEMPLATE = "zuul.routes.%s.path: /game/%s/**";
    private static final String ROUTE_URL_TEMPLATE = "zuul.routes.%s.url: %s/";
    private static final Semaphore semaphore = new Semaphore(1);

    private final RuntimeParameters runtimeParameters;
    private final GameUrlResolver gameUrlResolver;
    private final RestTemplate restTemplate;

    GamesProxyConfig(final GameUrlResolver gameUrlResolver,
                     final RestTemplate restTemplate,
                     final RuntimeParameters runtimeParameters) {
        this.gameUrlResolver = gameUrlResolver;
        this.restTemplate = restTemplate;
        this.runtimeParameters = runtimeParameters;
    }

    void addGameToConfig(final Game game) {

        try {
            semaphore.acquire();
            final File dir = Files.createTempDir();
            final Git git = fetchConfigRepository(dir);
            addGameToConfig(game, git);
        } catch (final GitAPIException | InterruptedException e) {
            e.printStackTrace();
        }finally {
            semaphore.release();
        }
    }

    private void addGameToConfig(final Game game, final Git git) throws GitAPIException {
        final File workTree = git.getRepository().getWorkTree();
        try (final Stream<Path> walk = java.nio.file.Files.walk(Paths.get(workTree.getPath()))) {
            final File configuration = findConfigurationFile(walk);
            appendNewRoutesToConfiguration(game, configuration);
            commitChanges(game, git, ADD_COMMIT_MESSAGE_TEMPLATE);
            fetchNewConfiguration();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    void removeGameFromConfig(final Game game) {

        try {
            semaphore.acquire();
            final File dir = Files.createTempDir();
            final Git git = fetchConfigRepository(dir);
            removeGameFromConfig(game, git);
        } catch (final GitAPIException | InterruptedException e) {
            e.printStackTrace();
        }finally {
            semaphore.release();
        }
    }

    private void removeGameFromConfig(final Game game, final Git git) throws GitAPIException {
        final File workTree = git.getRepository().getWorkTree();
        try (final Stream<Path> walk = java.nio.file.Files.walk(Paths.get(workTree.getPath()))) {
            final File configuration = findConfigurationFile(walk);
            removeRoutesFromConfiguration(game, configuration);
            commitChanges(game, git, REMOVE_COMMIT_MESSAGE_TEMPLATE);
            fetchNewConfiguration();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    private void removeRoutesFromConfiguration(final Game game, final File configuration) throws IOException {
        final String content = java.nio.file.Files.readString(Paths.get(configuration.getPath()));
        final List<String> properties = new ArrayList<>(Arrays.asList(content.split("\n")))
                .stream()
                .filter(a -> !a.contains(game.getUrl()))
                .collect(Collectors.toList());

        final String contentToWrite = String.join("\n", properties);
        Files.write(contentToWrite.getBytes(), configuration);
    }

    private Git fetchConfigRepository(final File dir) throws GitAPIException {
        return Git.cloneRepository()
                        .setCredentialsProvider(new UsernamePasswordCredentialsProvider(runtimeParameters.getGitEmail(), runtimeParameters.getGitPassword()))
                        .setURI(runtimeParameters.getGitRepository())
                        .setDirectory(dir)
                        .call();
    }

    private File findConfigurationFile(final Stream<Path> walk) {
        final List<File> result = walk.filter(a -> a.getFileName().toString().contains(runtimeParameters.getConfigurationFileName()))
                .map(path -> new File(path.toUri())).collect(Collectors.toList());

        final File configuration = Iterables.getOnlyElement(result);
        configuration.setWritable(true);
        return configuration;
    }

    private void appendNewRoutesToConfiguration(final Game game, final File configuration) throws IOException {
        final String content = java.nio.file.Files.readString(Paths.get(configuration.getPath()));
        final List<String> properties = new ArrayList<>(Arrays.asList(content.split("\n")));
        properties.add(String.format(ROUTE_PATH_TEMPLATE, game.getUrl(), game.getUrl()));
        properties.add(String.format(ROUTE_URL_TEMPLATE, game.getUrl(), gameUrlResolver.resolve(game)));

        final String contentToWrite = String.join("\n", properties);
        Files.write(contentToWrite.getBytes(), configuration);
    }

    private void commitChanges(final Game game, final Git git, final String messageTemplate) throws GitAPIException {

        git.add()
           .addFilepattern(runtimeParameters.getConfigurationFileName())
           .call();

        git.commit()
           .setMessage(String.format(messageTemplate, game.getId().getRaw()))
           .call();

        git.push()
           .setCredentialsProvider(new UsernamePasswordCredentialsProvider(runtimeParameters.getGitEmail(), runtimeParameters.getGitPassword()))
           .call();

    }

    private void fetchNewConfiguration() throws JsonProcessingException {
        final ObjectMapper mapper = new ObjectMapper();
        final ObjectNode emptyObject = mapper.createObjectNode();
        final String emptyJson = mapper.writeValueAsString(emptyObject);

        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json");
        final HttpEntity<String> entityWithEmptyBody = new HttpEntity<>(emptyJson, httpHeaders);

        restTemplate.exchange(runtimeParameters.getRefreshEndpoint(), HttpMethod.POST, entityWithEmptyBody, Object.class);
    }
}
