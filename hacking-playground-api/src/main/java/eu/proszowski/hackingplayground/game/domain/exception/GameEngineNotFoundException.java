package eu.proszowski.hackingplayground.game.domain.exception;

import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.challenge.domain.ports.ImageType;
import eu.proszowski.hackingplayground.game.domain.GameId;

public class GameEngineNotFoundException extends RuntimeException {
    public GameEngineNotFoundException(final ChallengeId challengeId, final ImageType type) {
        super(String.format("Any game engine which supports image type: %s for challenge with id: %s couldn't be find", type, challengeId.getRaw()));
    }

    public GameEngineNotFoundException(final GameId id) {
        super(String.format("Any game engine which supports game with id %s couldn't be found", id.getRaw()));
    }
}
