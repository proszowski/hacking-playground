package eu.proszowski.hackingplayground.game.domain.exception;

public class GameRemovalException extends RuntimeException {
    public GameRemovalException(final Exception e) {
        super(String.format("Exception occured during game removal with cause: %s", e.getCause()));
    }
}
