package eu.proszowski.hackingplayground.game.domain.ports;

import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;

public interface GameRequest {
    String getPath();

    MultiValueMap<String, String> getHeaders();

    MultiValueMap<String, String> getParams();

    HttpMethod getHttpMethod();

    String getBody();
}
