package eu.proszowski.hackingplayground.game.infrastracture.db;

import eu.proszowski.hackingplayground.game.domain.ports.GameAssociationsRepository;
import eu.proszowski.hackingplayground.game.domain.ports.GameRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class GameDbConfig {

    @Bean
    GameAssociationsRepository gameAssociationsRepository() {
        return new InMemoryGameAssociationsRepository();
    }

    @Bean
    GameRepository gameRepository() {
        return new InMemoryGameRepository();
    }
}
