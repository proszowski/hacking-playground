package eu.proszowski.hackingplayground.game.domain.exception;

public class GameRestartException extends RuntimeException {
    public GameRestartException(final Exception e) {
        super(String.format("Exception occured during game restart with cause: %s", e.getCause()));
    }

    public GameRestartException() {
        super("Exception occured during game restart");
    }
}
