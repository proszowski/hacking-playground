package eu.proszowski.hackingplayground.game.domain;

import com.spotify.docker.client.DockerClient;
import eu.proszowski.hackingplayground.UniqueIdProvider;
import eu.proszowski.hackingplayground.challenge.domain.DockerComposeImage;
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository;
import eu.proszowski.hackingplayground.challenge.domain.ports.ImageType;
import eu.proszowski.hackingplayground.game.domain.ports.Game;
import eu.proszowski.hackingplayground.game.domain.ports.GameAssociationsRepository;
import eu.proszowski.hackingplayground.game.domain.ports.GameRepository;
import eu.proszowski.hackingplayground.game.domain.ports.UrlGenerator;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import java.time.Instant;
import java.util.Collections;
import java.util.List;

public class DockerComposeGameEngine extends AbstractGameEngine {

    private static final List<ImageType> SUPPORTED_TYPES = Collections.singletonList(ImageType.DOCKER_COMPOSE);
    private final DockerClient dockerClient;

    DockerComposeGameEngine(final ChallengeRepository challengeRepository,
                            final GameRepository gameRepository,
                            final UrlGenerator urlGenerator,
                            final DockerClient dockerClient,
                            final GameAssociationsRepository gameAssociationsRepository,
                            final GamesProxyConfig gamesProxyConfig) {
        super(challengeRepository, gameRepository, urlGenerator, gameAssociationsRepository, gamesProxyConfig);
        this.dockerClient = dockerClient;
    }

    @Override
    protected Game createNewGame(final Challenge challenge, final HackerId hackerId) {
        final GameId gameId = GameId.fromUUID(UniqueIdProvider.next());
        final DockerComposeImage dockerComposeImage = (DockerComposeImage) challenge.getDetails().getImage();

        final DockerComposeGame game = DockerComposeGame.builder()
                .id(gameId)
                .url(urlGenerator.generate())
                .creatorId(hackerId)
                .challenge(challenge)
                .dockerClient(dockerClient)
                .dockerComposeConfig(dockerComposeImage.getDockerComposeConfig())
                .build();

        final DockerComposeGame initializedGame = (DockerComposeGame) game.initialize();
        final DockerComposeGame startedGame = (DockerComposeGame) initializedGame.start();

        gameRepository.save(startedGame);

        return startedGame;
    }

    @Override
    public boolean isSupporting(final ImageType imageType) {
        return SUPPORTED_TYPES.contains(imageType);
    }

    @Override
    public boolean isSupporting(final Game game) {
        return game instanceof DockerComposeGame;
    }
}
