package eu.proszowski.hackingplayground.game.domain;

import eu.proszowski.hackingplayground.game.domain.ports.GameRequest;
import eu.proszowski.hackingplayground.game.domain.ports.GameRequestFactory;
import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;

class DefaultGameRequestFactory implements GameRequestFactory {

    @Override
    public GameRequest create(final MultiValueMap<String, String> params, final MultiValueMap<String, String> headers, final HttpMethod httpMethod, final String body) {
        return DefaultGameRequest.builder()
                .headers(headers)
                .params(params)
                .httpMethod(httpMethod)
                .body(body)
                .build();
    }

    @Override
    public GameRequest create(final String path, final MultiValueMap<String, String> params, final MultiValueMap<String, String> headers, final HttpMethod httpMethod, final String body) {
        return DefaultGameRequest.builder()
                .headers(headers)
                .params(params)
                .httpMethod(httpMethod)
                .path("/" + path)
                .body(body)
                .build();
    }
}
