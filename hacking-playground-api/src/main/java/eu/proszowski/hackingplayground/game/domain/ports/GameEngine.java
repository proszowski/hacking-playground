package eu.proszowski.hackingplayground.game.domain.ports;

import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.challenge.domain.ports.ImageType;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import java.lang.invoke.MethodHandles;

public interface GameEngine {

    Game getInstanceOf(Challenge challenge, HackerId hackerId);

    boolean isSupporting(ImageType imageType);

    boolean isSupporting(Game game);

    default String getName() {
        return MethodHandles.lookup().lookupClass().getSimpleName();
    }

    Game restartGame(HackerId hackerId, Game game);
}
