package eu.proszowski.hackingplayground.game.domain;

import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository;
import eu.proszowski.hackingplayground.game.domain.exception.UserNotAuthorizedToRestartGameException;
import eu.proszowski.hackingplayground.game.domain.ports.Game;
import eu.proszowski.hackingplayground.game.domain.ports.GameRepository;
import eu.proszowski.hackingplayground.challenge.domain.ports.Image;
import eu.proszowski.hackingplayground.game.domain.exception.BrokenAssociationException;
import eu.proszowski.hackingplayground.game.domain.exception.ImageNotSupportedByGameEngine;
import eu.proszowski.hackingplayground.game.domain.ports.GameAssociationsRepository;
import eu.proszowski.hackingplayground.game.domain.ports.GameEngine;
import eu.proszowski.hackingplayground.game.domain.ports.UrlGenerator;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import lombok.experimental.SuperBuilder;
import java.util.Optional;

@SuperBuilder
abstract class AbstractGameEngine implements GameEngine {

    protected ChallengeRepository challengeRepository;
    protected GameRepository gameRepository;
    protected UrlGenerator urlGenerator;
    protected GameAssociationsRepository gameAssociationsRepository;
    private GamesProxyConfig gamesProxyConfig;

    AbstractGameEngine(final ChallengeRepository challengeRepository,
                       final GameRepository gameRepository,
                       final UrlGenerator urlGenerator,
                       final GameAssociationsRepository gameAssociationsRepository,
                       final GamesProxyConfig gamesProxyConfig) {
        this.challengeRepository = challengeRepository;
        this.gameRepository = gameRepository;
        this.urlGenerator = urlGenerator;
        this.gameAssociationsRepository = gameAssociationsRepository;
        this.gamesProxyConfig = gamesProxyConfig;
    }

    @Override
    public Game getInstanceOf(final Challenge challenge, final HackerId hackerId) {
        final Image image = challenge.getDetails().getImage();

        if (isSupporting(image.getType())) {
            final Optional<GameId> gameId = gameAssociationsRepository.findGameIdByChallengeIdAndHackerId(challenge.getId(), hackerId);
            return gameId
                    .map(id -> gameRepository.findById(id).orElseThrow(() -> new BrokenAssociationException(id)))
                    .orElseGet(() -> {
                        final Game game = createNewGame(challenge, hackerId);
                        gamesProxyConfig.addGameToConfig(game);
                        gameAssociationsRepository.createAssociation(challenge.getId(), hackerId, game.getId());
                        return game;
                    });
        }

        throw new ImageNotSupportedByGameEngine(image, this);
    }

    protected abstract Game createNewGame(final Challenge challenge, final HackerId hackerId);

    @Override
    public Game restartGame(final HackerId hackerId, final Game game) {
        validateIfUserIsAuthorizedForGivenAction(game.getUrl(), hackerId, game);

        gamesProxyConfig.removeGameFromConfig(game);
        final Game restartedGame = game.restart();
        gameRepository.save(restartedGame);
        gamesProxyConfig.addGameToConfig(restartedGame);
        return restartedGame;
    }

    private void validateIfUserIsAuthorizedForGivenAction(final String gameUrl, final HackerId hackerId, final Game game) {
        if(!hackerId.equals(game.getCreatorId())){
            throw new UserNotAuthorizedToRestartGameException(gameUrl, hackerId);
        }
    }

}
