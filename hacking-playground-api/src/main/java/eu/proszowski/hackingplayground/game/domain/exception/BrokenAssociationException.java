package eu.proszowski.hackingplayground.game.domain.exception;

import eu.proszowski.hackingplayground.game.domain.GameId;

public class BrokenAssociationException extends RuntimeException {
    public BrokenAssociationException(final GameId id) {
        super(String.format("Association claimed that there is a game with id %s but there was not", id));
    }
}
