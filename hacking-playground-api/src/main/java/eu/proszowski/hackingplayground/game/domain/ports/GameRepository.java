package eu.proszowski.hackingplayground.game.domain.ports;

import eu.proszowski.hackingplayground.game.domain.GameId;
import java.util.Optional;

public interface GameRepository {
    Optional<Game> findById(GameId gameId);

    GameId save(Game game);

    Optional<Game> findByGameUrl(String gameUrl);
}
