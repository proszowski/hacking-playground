package eu.proszowski.hackingplayground.game.domain;

import com.github.dockerjava.api.model.Container;
import eu.proszowski.hackingplayground.game.domain.exception.ServiceNameNotSpecifiedException;
import java.util.Optional;

public class ContainerLabelsRetriever {

    static String retrieveServiceName(final Container container) {
        return Optional.ofNullable(container.getLabels().get("com.docker.compose.service"))
                .orElseThrow(() -> new ServiceNameNotSpecifiedException(container.getId()));
    }
}
