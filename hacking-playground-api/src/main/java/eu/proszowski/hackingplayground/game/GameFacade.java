package eu.proszowski.hackingplayground.game;

import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository;
import eu.proszowski.hackingplayground.game.domain.exception.GameNotFoundException;
import eu.proszowski.hackingplayground.game.domain.ports.Game;
import eu.proszowski.hackingplayground.game.domain.ports.GameRepository;
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.challenge.domain.exception.ChallengeNotFoundException;
import eu.proszowski.hackingplayground.challenge.domain.ports.Image;
import eu.proszowski.hackingplayground.game.domain.exception.GameEngineNotFoundException;
import eu.proszowski.hackingplayground.game.domain.exception.UserNotAuthorizedToRestartGameException;
import eu.proszowski.hackingplayground.game.domain.ports.GameEngine;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import java.util.List;

public class GameFacade {

    private final List<GameEngine> gameEngines;
    private final ChallengeRepository challengeRepository;
    private final GameRepository gameRepository;

    public GameFacade(final List<GameEngine> gameEngines,
                      final ChallengeRepository challengeRepository,
                      final GameRepository gameRepository) {
        this.gameEngines = gameEngines;
        this.challengeRepository = challengeRepository;
        this.gameRepository = gameRepository;
    }

    public Game getInstanceOf(final ChallengeId challengeId, final HackerId hackerId) {
        final Challenge challenge = challengeRepository.findById(challengeId)
                .orElseThrow(() -> new ChallengeNotFoundException(challengeId));
        final Image image = challenge.getDetails().getImage();

        return gameEngines.stream()
                .filter(gameEngine -> gameEngine.isSupporting(image.getType()))
                .findAny()
                .map(gameEngine -> gameEngine.getInstanceOf(challenge, hackerId))
                .orElseThrow(() -> new GameEngineNotFoundException(challengeId, image.getType()));
    }

    public Game restartMachine(final String gameUrl, final HackerId hackerId) {
        final Game game = gameRepository.findByGameUrl(gameUrl)
                .orElseThrow(() -> new GameNotFoundException(gameUrl));

        return gameEngines.stream()
                .filter(gameEngine -> gameEngine.isSupporting(game))
                .findAny()
                .map(gameEngine -> gameEngine.restartGame(hackerId, game))
                .orElseThrow(() -> new GameEngineNotFoundException(game.getId()));

    }

}
