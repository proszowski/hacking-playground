package eu.proszowski.hackingplayground.game.infrastracture.db;

import com.google.common.collect.Maps;
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.game.domain.GameId;
import eu.proszowski.hackingplayground.game.domain.ports.GameAssociationsRepository;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import lombok.Value;
import java.util.Map;
import java.util.Optional;

public class InMemoryGameAssociationsRepository implements GameAssociationsRepository {

    private final Map<HackerIdAndChallengeId, GameId> associations = Maps.newHashMap();

    @Override
    public Optional<GameId> findGameIdByChallengeIdAndHackerId(final ChallengeId challengeId, final HackerId hackerId) {
        final HackerIdAndChallengeId hackerIdAndChallengeId = new HackerIdAndChallengeId(hackerId, challengeId);
        return Optional.ofNullable(associations.get(hackerIdAndChallengeId));
    }

    @Override
    public void createAssociation(final ChallengeId challengeId, final HackerId hackerId, final GameId gameId) {
        final HackerIdAndChallengeId hackerIdAndChallengeId = new HackerIdAndChallengeId(hackerId, challengeId);
        associations.put(hackerIdAndChallengeId, gameId);
    }

    @Value
    private static class HackerIdAndChallengeId {
        private HackerId hackerId;
        private ChallengeId challengeId;
    }
}
