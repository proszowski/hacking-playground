package eu.proszowski.hackingplayground.game.domain.ports;

import eu.proszowski.hackingplayground.game.domain.GameId;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;

public interface Game {

    GameId getId();

    String getUrl();

    HackerId getCreatorId();

    Game initialize();

    Game start();

    Game stop();

    Game remove();

    Game restart();

    int getRestartsCount();

    GameStatus getStatus();
}
