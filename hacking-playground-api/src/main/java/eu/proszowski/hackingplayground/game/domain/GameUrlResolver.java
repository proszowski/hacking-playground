package eu.proszowski.hackingplayground.game.domain;

import com.github.dockerjava.api.exception.DockerException;
import com.github.dockerjava.api.model.Container;
import eu.proszowski.hackingplayground.challenge.domain.DockerComposeImage;
import eu.proszowski.hackingplayground.challenge.domain.DockerImage;
import eu.proszowski.hackingplayground.game.domain.exception.GettingDockerContainerIpException;
import eu.proszowski.hackingplayground.game.domain.ports.Game;
import java.util.Optional;

class GameUrlResolver {

    String resolve(final Game game) {
        if(game instanceof  DockerComposeGame){
            return fetchUrlForDockerComposeGame((DockerComposeGame) game);
        }else if (game instanceof DockerGame){
            return fetchUrlForDockerGame((DockerGame) game);
        }

        throw new IllegalStateException("Unsupported game type");
    }

    private String fetchUrlForDockerComposeGame(final DockerComposeGame game) {
        final Container containerOfExposedService = game.retrieveContainerOfExposedService();
        final String containerIp = game.retrieveContainerIp(containerOfExposedService);
        final Integer port = ((DockerComposeImage) game.getChallenge().getDetails().getImage()).getExposedPort();
        return String.format("http://%s:%s", containerIp, port);
    }

    private static String fetchUrlForDockerGame(final DockerGame game) {
        final DockerImage image = (DockerImage) game.getChallenge().getDetails().getImage();
        final Integer port = image.getPort();

        String containerIp = null;
        final GameId id = game.getId();
        try {
            containerIp = Optional.ofNullable(game.getDockerClient().inspectContainer(game.getContainerId().get()).networkSettings().networks())
                    .map(networks -> networks.get("bridge").ipAddress()).orElseThrow(() -> new GettingDockerContainerIpException(id));
            if (containerIp == null || containerIp.isEmpty()) {
                throw new GettingDockerContainerIpException(id);
            }
        } catch (final DockerException | InterruptedException | com.spotify.docker.client.exceptions.DockerException e) {
            throw new GettingDockerContainerIpException(id);
        }

        return String.format("http://%s:%s", containerIp, port);
    }
}
