package eu.proszowski.hackingplayground.game.domain.exception;

public class GameCreationException extends RuntimeException {

    public GameCreationException(final String message) {
        super(message);
    }

    public GameCreationException(final Exception e) {
        super(String.format("During game creation exception occured, cause: %s", e.getCause()));
    }
}
