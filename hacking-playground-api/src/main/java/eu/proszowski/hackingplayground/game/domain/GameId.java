package eu.proszowski.hackingplayground.game.domain;

import lombok.Value;
import java.util.UUID;

@Value
public class GameId {
    private UUID raw;

    public static GameId fromUUID(final UUID id) {
        return new GameId(id);
    }
}
