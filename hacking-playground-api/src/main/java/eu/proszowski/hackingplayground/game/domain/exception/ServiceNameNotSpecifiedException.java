package eu.proszowski.hackingplayground.game.domain.exception;

public class ServiceNameNotSpecifiedException extends RuntimeException {
    public ServiceNameNotSpecifiedException(final String id) {
        super(String.format("Service name not specified for container with id %s", id));
    }
}
