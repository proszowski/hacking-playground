package eu.proszowski.hackingplayground.game.domain.ports;

import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.game.domain.GameId;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import java.util.Optional;

public interface GameAssociationsRepository {
    Optional<GameId> findGameIdByChallengeIdAndHackerId(ChallengeId challengeId, HackerId hackerId);

    void createAssociation(ChallengeId challengeId, HackerId hackerId, GameId gameId);
}
