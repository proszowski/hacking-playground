package eu.proszowski.hackingplayground.game.domain;

import com.github.dockerjava.api.model.Container;
import com.google.common.collect.Iterables;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.testcontainers.containers.DockerComposeContainer;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.NONE)
public class DockerComposeContainerChildrenRetriever {

    @SneakyThrows
    static public List<Container> getChildContainers(final DockerComposeContainer dockerComposeContainer) {
        final Method[] methods = DockerComposeContainer.class.getDeclaredMethods();
        final Method listChildContainersMethod = Iterables.getOnlyElement(Arrays.stream(methods)
                .filter(method -> method.getName().equals("listChildContainers"))
                .collect(Collectors.toList()));

        listChildContainersMethod.setAccessible(true);
        return (List<Container>) listChildContainersMethod.invoke(dockerComposeContainer);
    }

}
