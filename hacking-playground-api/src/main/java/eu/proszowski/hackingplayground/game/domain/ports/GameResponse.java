package eu.proszowski.hackingplayground.game.domain.ports;

import org.springframework.http.ResponseEntity;

public interface GameResponse {
    ResponseEntity<Object> getRaw();
}
