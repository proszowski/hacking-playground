package eu.proszowski.hackingplayground.game.api;

import eu.proszowski.hackingplayground.authentication.AuthenticationService;
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.game.GameFacade;
import eu.proszowski.hackingplayground.game.domain.ports.GameRequest;
import eu.proszowski.hackingplayground.game.domain.ports.GameRequestFactory;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class DefaultGameController implements GameController {

    @Autowired
    private GameFacade gameFacade;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private GameRequestFactory gameRequestFactory;

    @Override
    public String getInstanceOfChallenge(final UUID challengeId) {
        final HackerId hackerId = HackerId.fromUUID(authenticationService.getUserId());
        return gameFacade.getInstanceOf(ChallengeId.fromUUID(challengeId), hackerId)
                .getUrl();
    }

    @Override
    public void restartGame(final String gameUrl) {
        final HackerId hackerId = HackerId.fromUUID(authenticationService.getUserId());
        gameFacade.restartMachine(gameUrl, hackerId);
    }

}
