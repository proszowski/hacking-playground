package eu.proszowski.hackingplayground.game.domain.exception;

import eu.proszowski.hackingplayground.game.domain.GameId;

public class GettingDockerContainerIpException extends RuntimeException {
    public GettingDockerContainerIpException(final GameId id) {
        super(String.format("Exception occured when getting docker container ip, gameId: %s", id.getRaw()));
    }
}
