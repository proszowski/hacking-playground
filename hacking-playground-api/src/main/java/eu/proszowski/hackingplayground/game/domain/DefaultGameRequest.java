package eu.proszowski.hackingplayground.game.domain;

import eu.proszowski.hackingplayground.game.domain.ports.GameRequest;
import lombok.Builder;
import lombok.Value;
import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;

@Builder
@Value
class DefaultGameRequest implements GameRequest {
    @Builder.Default
    private String path = "";
    private MultiValueMap<String, String> params;
    private MultiValueMap<String, String> headers;
    private HttpMethod httpMethod;
    private String body;
}
