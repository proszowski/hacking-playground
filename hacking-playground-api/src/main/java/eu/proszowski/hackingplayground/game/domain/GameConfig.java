package eu.proszowski.hackingplayground.game.domain;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import eu.proszowski.hackingplayground.challenge.domain.ports.ChallengeRepository;
import eu.proszowski.hackingplayground.common.RuntimeParameters;
import eu.proszowski.hackingplayground.game.GameFacade;
import eu.proszowski.hackingplayground.game.domain.ports.GameAssociationsRepository;
import eu.proszowski.hackingplayground.game.domain.ports.GameEngine;
import eu.proszowski.hackingplayground.game.domain.ports.GameRepository;
import eu.proszowski.hackingplayground.game.domain.ports.GameRequestFactory;
import eu.proszowski.hackingplayground.game.domain.ports.UrlGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.UUID;

@Configuration
public class GameConfig {

    public static final String LOCALHOST_DOCKER = "unix:///var/run/docker.sock";

    @Bean
    public DockerClient dockerClient() {
        return new DefaultDockerClient(LOCALHOST_DOCKER);
    }

    @Bean
    public UrlGenerator urlGenerator() {
        return () -> UUID.randomUUID().toString();
    }

    @Bean
    public GameFacade gameEnginesFacade(final DockerClient dockerClient,
                                        final RestTemplate restTemplate,
                                        final ChallengeRepository challengeRepository,
                                        final GameRepository gameRepository,
                                        final UrlGenerator urlGenerator,
                                        final GameAssociationsRepository gameAssociationsRepository,
                                        final RuntimeParameters runtimeParameters) {

        final GamesProxyConfig gamesProxyConfig = new GamesProxyConfig(new GameUrlResolver(), restTemplate, runtimeParameters);

        final GameEngine dockerGameEngine = new DockerGameEngine(challengeRepository,
                gameRepository,
                urlGenerator,
                gameAssociationsRepository,
                dockerClient,
                gamesProxyConfig);

        final GameEngine dockerComposeGameEngine = new DockerComposeGameEngine(challengeRepository,
                gameRepository,
                urlGenerator,
                dockerClient,
                gameAssociationsRepository,
                gamesProxyConfig);

        return new GameFacade(Arrays.asList(dockerGameEngine, dockerComposeGameEngine), challengeRepository, gameRepository);
    }

    @Bean
    public GameRequestFactory gameRequestFactory() {
        return new DefaultGameRequestFactory();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
