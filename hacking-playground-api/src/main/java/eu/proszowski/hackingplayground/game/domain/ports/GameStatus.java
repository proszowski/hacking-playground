package eu.proszowski.hackingplayground.game.domain.ports;

public enum GameStatus {
    STARTED, STOPPED, INITIALIZED, REMOVED, RESTARTED, UNSPECIFIED
}
