package eu.proszowski.hackingplayground.game.domain.exception;

import eu.proszowski.hackingplayground.challenge.domain.ports.Image;
import eu.proszowski.hackingplayground.game.domain.ports.GameEngine;

public class ImageNotSupportedByGameEngine extends RuntimeException {

    public ImageNotSupportedByGameEngine(final Image image, final GameEngine gameEngine) {
        super(String.format("Image of type %s is not supported by game engine with challengeName %s", image.getType(), gameEngine.getName()));
    }
}
