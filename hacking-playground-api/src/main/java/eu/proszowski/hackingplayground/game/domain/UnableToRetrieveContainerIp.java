package eu.proszowski.hackingplayground.game.domain;

public class UnableToRetrieveContainerIp extends RuntimeException {
    public UnableToRetrieveContainerIp(final String id) {
        super(String.format("Unable to retrieve container ip, id of container: %s", id));
    }
}
