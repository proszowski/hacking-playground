package eu.proszowski.hackingplayground.game.domain.exception;

import eu.proszowski.hackingplayground.hacker.domain.HackerId;

public class UserNotAuthorizedToRestartGameException extends RuntimeException {
    public UserNotAuthorizedToRestartGameException(final String gameId, final HackerId hackerId) {
        super(String.format("User with id %s tried to restart game with id %s", hackerId, gameId));
    }
}
