package eu.proszowski.hackingplayground.game.infrastracture.db;

import com.google.common.collect.Maps;
import eu.proszowski.hackingplayground.game.domain.ports.GameRepository;
import eu.proszowski.hackingplayground.game.domain.GameId;
import eu.proszowski.hackingplayground.game.domain.ports.Game;
import java.util.Map;
import java.util.Optional;

public class InMemoryGameRepository implements GameRepository {

    private final Map<GameId, Game> games = Maps.newHashMap();

    @Override
    public Optional<Game> findById(final GameId gameId) {
        return Optional.ofNullable(games.get(gameId));
    }

    @Override
    public GameId save(final Game game) {
        games.put(game.getId(), game);
        return game.getId();
    }

    @Override
    public Optional<Game> findByGameUrl(final String gameUrl) {
        return games.values().stream()
                .filter(g -> g.getUrl().equals(gameUrl))
                .findFirst();
    }

}
