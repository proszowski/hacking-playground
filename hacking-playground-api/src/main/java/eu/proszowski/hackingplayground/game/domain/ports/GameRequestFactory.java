package eu.proszowski.hackingplayground.game.domain.ports;

import org.springframework.http.HttpMethod;
import org.springframework.util.MultiValueMap;

public interface GameRequestFactory {

    GameRequest create(final MultiValueMap<String, String> params,
                       final MultiValueMap<String, String> headers,
                       final HttpMethod httpMethod,
                       final String body);

    GameRequest create(final String path,
                       final MultiValueMap<String, String> params,
                       final MultiValueMap<String, String> headers,
                       final HttpMethod httpMethod,
                       final String body);

}
