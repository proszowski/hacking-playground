package eu.proszowski.hackingplayground.game.domain;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.ContainerConfig;
import eu.proszowski.hackingplayground.challenge.domain.DockerImage;
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.game.domain.exception.GameCreationException;
import eu.proszowski.hackingplayground.game.domain.exception.GameRemovalException;
import eu.proszowski.hackingplayground.game.domain.exception.GameRestartException;
import eu.proszowski.hackingplayground.game.domain.exception.GameStartException;
import eu.proszowski.hackingplayground.game.domain.exception.GameStopException;
import eu.proszowski.hackingplayground.game.domain.ports.Game;
import eu.proszowski.hackingplayground.game.domain.ports.GameStatus;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import java.time.Instant;
import java.util.Optional;

@Value
@Builder(toBuilder = true)
public class DockerGame implements Game {

    @NonNull
    private GameId id;

    @NonNull
    private DockerClient dockerClient;

    @NonNull
    private String url;

    @NonNull
    private Challenge challenge;

    @NonNull
    private HackerId creatorId;

    @NonNull
    private final Instant creationDate = Instant.now();

    @NonNull
    @Builder.Default
    private GameStatus status = GameStatus.UNSPECIFIED;

    private String containerId;

    private int restartsCount;

    public Optional<String> getContainerId() {
        return Optional.ofNullable(containerId);
    }

    @Override
    public Game initialize() {
        final DockerImage image = (DockerImage) challenge.getDetails().getImage();

        final ContainerConfig container = ContainerConfig.builder()
                .image(image.getName())
                .exposedPorts(image.getPort().toString())
                .addVolume("/tmp")
                .build();
        try {
            final String id = dockerClient.createContainer(container).id();
            return this.toBuilder()
                    .containerId(id)
                    .status(GameStatus.INITIALIZED)
                    .build();
        } catch (final DockerException | InterruptedException e) {
            throw new GameCreationException(e);
        }
    }

    @Override
    public Game start() {
        if (status == GameStatus.STARTED) {
            throw new GameStartException("Game is already started");
        }

        try {
            dockerClient.startContainer(containerId);
            return toBuilder()
                    .status(GameStatus.STARTED)
                    .build();
        } catch (final DockerException | InterruptedException e) {
            throw new GameStartException(e);
        }
    }

    @Override
    public Game stop() {
        try {
            dockerClient.stopContainer(containerId, 0);
            return toBuilder()
                    .status(GameStatus.STOPPED)
                    .build();
        } catch (final DockerException | InterruptedException e) {
            throw new GameStopException(e);
        }
    }

    @Override
    public Game remove() {
        try {
            dockerClient.removeContainer(containerId);
            return toBuilder()
                    .status(GameStatus.REMOVED)
                    .build();
        } catch (final DockerException | InterruptedException e) {
            throw new GameRemovalException(e);
        }
    }

    @Override
    public Game restart() {
        try {
            dockerClient.killContainer(containerId);
            dockerClient.removeContainer(containerId);
            final Game initializedGame = initialize();
            final DockerGame startedGame = (DockerGame) initializedGame.start();
            return toBuilder()
                    .status(GameStatus.RESTARTED)
                    .restartsCount(this.restartsCount + 1)
                    .containerId(startedGame.getContainerId().orElseThrow(GameRestartException::new))
                    .build();
        } catch (final DockerException | InterruptedException e) {
            throw new GameRestartException(e);
        }
    }
}
