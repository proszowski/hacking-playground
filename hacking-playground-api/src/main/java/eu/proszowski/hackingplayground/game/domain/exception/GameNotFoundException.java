package eu.proszowski.hackingplayground.game.domain.exception;

import eu.proszowski.hackingplayground.game.domain.GameId;

public class GameNotFoundException extends RuntimeException {

    public GameNotFoundException(final GameId gameId) {
        super(String.format("Game with id %s not found", gameId.getRaw()));
    }

    public GameNotFoundException(final String gameUrl) {
        super(String.format("Game with url %s not found", gameUrl));
    }
}
