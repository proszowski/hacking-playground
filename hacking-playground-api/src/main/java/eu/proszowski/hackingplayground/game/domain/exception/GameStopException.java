package eu.proszowski.hackingplayground.game.domain.exception;

public class GameStopException extends RuntimeException {

    public GameStopException(final String message) {
        super(message);
    }

    public GameStopException(final Exception e) {
        super(String.format("Exception occured when trying to stop game with cause: %s", e.getCause()));
    }
}
