package eu.proszowski.hackingplayground.game.domain;

import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.ContainerNetwork;
import com.spotify.docker.client.DockerClient;
import eu.proszowski.hackingplayground.challenge.domain.DockerComposeImage;
import eu.proszowski.hackingplayground.challenge.domain.ports.Challenge;
import eu.proszowski.hackingplayground.game.domain.exception.GameCreationException;
import eu.proszowski.hackingplayground.game.domain.exception.GameStartException;
import eu.proszowski.hackingplayground.game.domain.exception.GameStopException;
import eu.proszowski.hackingplayground.game.domain.ports.Game;
import eu.proszowski.hackingplayground.game.domain.ports.GameStatus;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.testcontainers.containers.DockerComposeContainer;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Value
@Builder(toBuilder = true)
public class DockerComposeGame implements Game {

    @NonNull
    private GameId id;

    @NonNull
    private DockerClient dockerClient;

    @NonNull
    private String url;

    @NonNull
    private Challenge challenge;

    @NonNull
    private HackerId creatorId;

    @NonNull
    private Instant creationDate = Instant.now();

    @NonNull
    private String dockerComposeConfig;

    @NonNull
    @Builder.Default
    private GameStatus status = GameStatus.UNSPECIFIED;

    private DockerComposeContainer composeContainer;

    private int restartsCount;

    @Override
    public Game initialize() {
        final File dockerComposeFile = getDockerComposeFile();

        final DockerComposeContainer dockerComposeContainer = new DockerComposeContainer(dockerComposeFile).withPull(true);

        return this.toBuilder()
                .composeContainer(dockerComposeContainer)
                .status(GameStatus.INITIALIZED)
                .build();

    }

    @Override
    public Game start() {

        if (status == GameStatus.STARTED) {
            throw new GameStartException("Game is already started");
        }

        final DockerComposeContainer container = Optional.ofNullable(composeContainer)
                .orElseThrow(() -> new GameStartException("Couldn't start a game"));

        container.start();


        return toBuilder()
                .status(GameStatus.STARTED)
                .build();
    }

    @Override
    public Game stop() {
        if (status != GameStatus.STARTED && status != GameStatus.RESTARTED) {
            throw new GameStopException("Game is not started, so can't be stopped");
        }

        final DockerComposeContainer container = Optional.ofNullable(composeContainer)
                .orElseThrow(() -> new GameStopException("Couldn't stop container"));

        container.stop();

        return toBuilder()
                .status(GameStatus.STOPPED)
                .build();
    }

    @Override
    public Game remove() {
        if (status != GameStatus.STOPPED && status != GameStatus.REMOVED) {
            stop();
        }

        return toBuilder()
                .status(GameStatus.REMOVED)
                .build();
    }

    @Override
    public Game restart() {
        final Game stoppedGame = stop();
        stoppedGame.start();

        return toBuilder()
                .restartsCount(this.restartsCount + 1)
                .status(GameStatus.RESTARTED)
                .build();
    }

    public Container retrieveContainerOfExposedService() {
        final DockerComposeImage image = (DockerComposeImage) challenge.getDetails().getImage();
        return DockerComposeContainerChildrenRetriever.getChildContainers(composeContainer).stream()
                .filter(container -> ContainerLabelsRetriever.retrieveServiceName(container).contains((image.getExposedServiceName())))
                .findFirst()
                .orElseThrow(() -> {
                    composeContainer.stop();
                    throw new GameCreationException("Exception occured during game creation. Exposed service with name %s couldn't be found");
                });
    }

    public String retrieveContainerIp(final Container container) {
        return container.getNetworkSettings()
                .getNetworks()
                .values()
                .stream()
                .findFirst()
                .map(ContainerNetwork::getIpAddress)
                .orElseThrow(() -> new UnableToRetrieveContainerIp(container.getId()));
    }

    private File getDockerComposeFile() {
        try {
            final File dockerComposeFile = File.createTempFile(UUID.randomUUID().toString(), UUID.randomUUID().toString());
            Files.write(Paths.get(dockerComposeFile.getPath()), dockerComposeConfig.getBytes());
            return dockerComposeFile;
        } catch (final IOException e) {
            throw new GameCreationException(e);
        }
    }
}
