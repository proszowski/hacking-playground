package eu.proszowski.hackingplayground.statistics.db;

import eu.proszowski.hackingplayground.statistics.Statistics;
import eu.proszowski.hackingplayground.statistics.StatisticsRepository;
import lombok.AllArgsConstructor;

import java.util.Optional;
import java.util.stream.Collectors;

public class PostgresStatisticsDatabase implements StatisticsRepository {

    private final JpaStatisticsRepository jpaStatisticsRepository;

    PostgresStatisticsDatabase(final JpaStatisticsRepository jpaStatisticsRepository) {
        this.jpaStatisticsRepository = jpaStatisticsRepository;
    }

    @Override
    public void save(final String gameUrl, final Statistics statistics) {
        new StatisticsEntity(gameUrl,
                statistics.getStatisticInfos().stream().map(StatisticInfoEntity::fromModel).collect(Collectors.toList()),
                statistics.getFlagSubmissionInfos().stream().map(FlagSubmissionInfoEntity::fromModel).collect(Collectors.toList()));
    }

    @Override
    public Optional<Statistics> getStatisticsFor(final String gameUrl) {
        return jpaStatisticsRepository
                .findById(gameUrl)
                .map(StatisticsEntity::fromModel);
    }
}
