package eu.proszowski.hackingplayground.statistics;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.springframework.http.HttpMethod;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

@Builder
@Value
public class StatisticInfo {

    private final Instant executionDate = Instant.now();

    private Map<String, String> headers;

    @NonNull
    private String uri;

    @NonNull
    private HttpMethod method;

    private String queryString;

    private Map<String, String[]> parameters;

    private String body;
}
