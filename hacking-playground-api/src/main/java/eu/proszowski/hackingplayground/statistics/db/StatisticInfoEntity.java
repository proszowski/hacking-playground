package eu.proszowski.hackingplayground.statistics.db;

import eu.proszowski.hackingplayground.statistics.StatisticInfo;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.http.HttpMethod;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MapKeyColumn;
import javax.persistence.Table;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;

@Entity
@Table(name = "statistic_infos")
@Data
@Builder
public class StatisticInfoEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    private Instant executionDate;

    @ElementCollection
    @MapKeyColumn(name="headerName")
    @Column(name="headerValue")
    private Map<String, String> headers;

    private String uri;

    private HttpMethod method;

    private String queryString;

    @ElementCollection
    @MapKeyColumn(name="parameterName")
    @Column(name="parameterValue")
    private Map<String, String[]> parameters;

    private String body;

    static StatisticInfoEntity fromModel(final StatisticInfo statisticInfo) {
        return StatisticInfoEntity.builder()
                .executionDate(statisticInfo.getExecutionDate())
                .headers(statisticInfo.getHeaders())
                .uri(statisticInfo.getUri())
                .method(statisticInfo.getMethod())
                .queryString(statisticInfo.getQueryString())
                .parameters(statisticInfo.getParameters())
                .body(statisticInfo.getBody())
                .build();
    }

    StatisticInfo toModel() {
        return StatisticInfo.builder()
                .body(this.body)
                .headers(this.headers)
                .method(this.method)
                .parameters(this.parameters)
                .queryString(this.queryString)
                .uri(this.uri)
                .build();
    }
}
