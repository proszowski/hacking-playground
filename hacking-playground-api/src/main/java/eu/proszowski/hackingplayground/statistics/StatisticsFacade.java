package eu.proszowski.hackingplayground.statistics;

import com.google.common.collect.Iterables;
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.game.domain.GameId;
import eu.proszowski.hackingplayground.game.domain.ports.Game;
import eu.proszowski.hackingplayground.game.domain.ports.GameAssociationsRepository;
import eu.proszowski.hackingplayground.game.domain.ports.GameRepository;

import java.util.Arrays;
import java.util.stream.Collectors;

public class StatisticsFacade {

    private final StatisticsRepository statisticsRepository;
    private final GameRepository gameRepository;
    private final GameAssociationsRepository gameAssociationsRepository;

    public StatisticsFacade(final GameRepository gameRepository,
                            final StatisticsRepository statisticsRepository,
                            final GameAssociationsRepository gameAssociationsRepository) {
        this.gameRepository = gameRepository;
        this.statisticsRepository = statisticsRepository;
        this.gameAssociationsRepository = gameAssociationsRepository;
    }

    public void notifyAboutFlagSubmission(final FlagSubmissionInfo flagSubmissionInfo) {
        final ChallengeId challengeId = ChallengeId.fromUUID(flagSubmissionInfo.getBaseId().getRaw());

        final String url = gameAssociationsRepository.findGameIdByChallengeIdAndHackerId(challengeId, flagSubmissionInfo.getHackerId())
                .map(GameId::getRaw)
                .map(gameId -> gameRepository.findById(GameId.fromUUID(gameId))
                        .orElseThrow(() -> new IllegalStateException(String.format("Cannot find game with id %s", gameId))))
                .map(Game::getUrl)
                .orElseThrow(() -> new IllegalStateException("Cannot apply statistics related to flag submission"));

        final Statistics statistics = statisticsRepository.getStatisticsFor(url)
                .orElse(Statistics.builder().build())
                .withFlagSubmitted(flagSubmissionInfo);

        statisticsRepository.save(url, statistics);
    }

    public void notifyAboutRequest(final StatisticInfo statisticInfo) {
        final String gameUrl = Iterables.get(Arrays.stream(statisticInfo.getUri().split("/")).collect(Collectors.toList()), 2);

        gameRepository.findByGameUrl(gameUrl)
                .orElseThrow(() -> new IllegalStateException(String.format("Cannot apply statistics for game with URL %s", gameUrl)));

        final Statistics statistics = statisticsRepository.getStatisticsFor(gameUrl)
                .orElse(Statistics.builder().build())
                .withStatisticInfoAdded(statisticInfo);

        statisticsRepository.save(gameUrl, statistics);
    }

    public Statistics getStatisticsForGame(final String gameUrl) {
        gameRepository.findByGameUrl(gameUrl)
                .orElseThrow(() -> new IllegalStateException(String.format("Cannot get statistics for game with URL %s", gameUrl)));

        return statisticsRepository.getStatisticsFor(gameUrl)
                .orElse(Statistics.EMPTY);
    }
}
