package eu.proszowski.hackingplayground.statistics;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class StatisticInfoDto {

    private Instant executionDate;

    private Map<String, String> headers;

    @NonNull
    private String uri;

    @NonNull
    private String method;

    private String queryString;

    private Map<String, String[]> parameters;

    private String body;

    public static StatisticInfoDto fromStatisticInfo(final StatisticInfo statisticInfo) {
        return StatisticInfoDto.builder()
                .executionDate(statisticInfo.getExecutionDate())
                .headers(statisticInfo.getHeaders())
                .uri(statisticInfo.getUri())
                .method(statisticInfo.getMethod().name())
                .queryString(statisticInfo.getQueryString())
                .parameters(statisticInfo.getParameters())
                .body(statisticInfo.getBody())
                .build();
    }
}
