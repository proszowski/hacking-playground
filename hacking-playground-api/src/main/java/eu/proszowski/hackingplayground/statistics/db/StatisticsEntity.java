package eu.proszowski.hackingplayground.statistics.db;

import eu.proszowski.hackingplayground.statistics.Statistics;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;
import java.util.stream.Collectors;

@Entity
@Table(name = "statistics")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class StatisticsEntity {

    @Id
    private String gameUrl;

    @OneToMany
    @Cascade(CascadeType.ALL)
    private Collection<StatisticInfoEntity> statisticInfos;

    @OneToMany
    @Cascade(CascadeType.ALL)
    private Collection<FlagSubmissionInfoEntity> flagSubmissionInfos;

    static Statistics fromModel(final StatisticsEntity statisticsEntity) {
        return Statistics.builder()
                .statisticInfos(statisticsEntity.getStatisticInfos().stream().map(StatisticInfoEntity::toModel).collect(Collectors.toList()))
                .flagSubmissionInfos(statisticsEntity.getFlagSubmissionInfos().stream().map(FlagSubmissionInfoEntity::toModel).collect(Collectors.toList()))
                .build();
    }
}
