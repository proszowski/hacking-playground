package eu.proszowski.hackingplayground.statistics;

import eu.proszowski.hackingplayground.game.domain.ports.GameAssociationsRepository;
import eu.proszowski.hackingplayground.game.domain.ports.GameRepository;
import eu.proszowski.hackingplayground.statistics.db.InMemoryStatisticsDatabase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class StatisticsConfig {

    @Bean
    StatisticsFacade statisticsFacade(final GameRepository gameRepository,
                                      final GameAssociationsRepository gameAssociationsRepository,
                                      final StatisticsRepository statisticsRepository){
        return new StatisticsFacade(gameRepository, statisticsRepository, gameAssociationsRepository);
    }
}
