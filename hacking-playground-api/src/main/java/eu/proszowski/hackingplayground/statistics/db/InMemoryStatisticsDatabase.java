package eu.proszowski.hackingplayground.statistics.db;

import eu.proszowski.hackingplayground.statistics.Statistics;
import eu.proszowski.hackingplayground.statistics.StatisticsRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class InMemoryStatisticsDatabase implements StatisticsRepository {

    final Map<String, Statistics> statistics = new HashMap<>();

    @Override
    public void save(final String gameUrl, final Statistics statistics) {
        this.statistics.put(gameUrl, statistics);
    }

    @Override
    public Optional<Statistics> getStatisticsFor(final String gameUrl) {
        return Optional.ofNullable(statistics.get(gameUrl));
    }
}
