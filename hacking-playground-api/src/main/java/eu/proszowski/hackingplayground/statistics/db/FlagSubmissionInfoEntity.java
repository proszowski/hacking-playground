package eu.proszowski.hackingplayground.statistics.db;

import eu.proszowski.hackingplayground.flags.domain.DefaultBaseId;
import eu.proszowski.hackingplayground.flags.domain.FlagSubmissionResult;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import eu.proszowski.hackingplayground.statistics.FlagSubmissionInfo;
import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "flag_submissions_info")
@Data
@Builder
public class FlagSubmissionInfoEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    private Instant date;

    private String flag;

    private FlagSubmissionResult flagSubmissionResult;

    private UUID hackerId;

    private UUID baseId;

    static FlagSubmissionInfoEntity fromModel(final FlagSubmissionInfo flagSubmissionInfo) {
        return FlagSubmissionInfoEntity.builder()
                .date(flagSubmissionInfo.getDate())
                .flag(flagSubmissionInfo.getFlag())
                .flagSubmissionResult(flagSubmissionInfo.getFlagSubmissionResult())
                .hackerId(flagSubmissionInfo.getHackerId().getRaw())
                .baseId(flagSubmissionInfo.getBaseId().getRaw())
                .build();
    }

    FlagSubmissionInfo toModel() {
        return FlagSubmissionInfo.builder()
                .baseId(DefaultBaseId.fromUUID(baseId))
                .hackerId(HackerId.fromUUID(hackerId))
                .flagSubmissionResult(flagSubmissionResult)
                .flag(flag)
                .build();
    }
}
