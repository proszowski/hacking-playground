package eu.proszowski.hackingplayground.statistics;

import com.google.common.collect.Lists;
import lombok.Builder;
import lombok.Value;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Builder(toBuilder = true)
@Value
public class Statistics {
    public static final Statistics EMPTY = Statistics.builder().build();
    private final UUID id = UUID.randomUUID();

    @Builder.Default
    private List<StatisticInfo> statisticInfos = Collections.emptyList();

    @Builder.Default
    private List<FlagSubmissionInfo> flagSubmissionInfos = Collections.emptyList();

    public Statistics withStatisticInfoAdded(final StatisticInfo statisticInfo) {
        return toBuilder()
                .statisticInfos(Lists.asList(statisticInfo, statisticInfos.toArray(new StatisticInfo[0])))
                .build();
    }

    public Statistics withFlagSubmitted(final FlagSubmissionInfo flagSubmissionInfo) {
        return toBuilder()
                .flagSubmissionInfos(Lists.asList(flagSubmissionInfo, flagSubmissionInfos.toArray(new FlagSubmissionInfo[0])))
                .build();
    }
}
