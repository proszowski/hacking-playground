package eu.proszowski.hackingplayground.statistics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticsController {

    @Autowired
    private StatisticsFacade statisticsFacade;

    @GetMapping("statistics/{gameUrl}")
    public StatisticsDto getStatistics(@PathVariable final String gameUrl){
        return StatisticsDto.fromStatistics(statisticsFacade.getStatisticsForGame(gameUrl));
    }
}
