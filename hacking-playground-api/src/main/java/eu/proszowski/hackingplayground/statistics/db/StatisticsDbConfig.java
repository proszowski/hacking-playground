package eu.proszowski.hackingplayground.statistics.db;

import eu.proszowski.hackingplayground.statistics.StatisticsRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StatisticsDbConfig {

    @Bean
    static StatisticsRepository statisticsRepository(final JpaStatisticsRepository jpaStatisticsRepository){
        return new InMemoryStatisticsDatabase();
    }
}
