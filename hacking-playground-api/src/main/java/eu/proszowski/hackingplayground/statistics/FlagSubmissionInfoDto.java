package eu.proszowski.hackingplayground.statistics;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Builder;
import lombok.Value;

import java.time.Instant;
import java.util.UUID;

@Builder
@Value
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class FlagSubmissionInfoDto {
    private Instant date;
    private String flag;
    private String flagSubmissionResult;
    private String hackerId;
    private String baseId;

    public static FlagSubmissionInfoDto fromFlagSubmissionInfo(final FlagSubmissionInfo flagSubmissionInfo) {
        return FlagSubmissionInfoDto.builder()
                .date(flagSubmissionInfo.getDate())
                .flag(flagSubmissionInfo.getFlag())
                .flagSubmissionResult(flagSubmissionInfo.getFlagSubmissionResult().name())
                .hackerId(flagSubmissionInfo.getHackerId().getRaw().toString())
                .baseId(flagSubmissionInfo.getBaseId().getRaw().toString())
                .build();
    }
}
