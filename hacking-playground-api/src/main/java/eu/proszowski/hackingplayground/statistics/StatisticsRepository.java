package eu.proszowski.hackingplayground.statistics;

import java.util.Optional;

public interface StatisticsRepository {
    void save(String gameUrl, Statistics statistics);

    Optional<Statistics> getStatisticsFor(String gameUrl);
}
