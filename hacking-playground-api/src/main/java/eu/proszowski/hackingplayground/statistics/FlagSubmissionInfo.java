package eu.proszowski.hackingplayground.statistics;

import eu.proszowski.hackingplayground.flags.domain.BaseId;
import eu.proszowski.hackingplayground.flags.domain.FlagSubmissionResult;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import lombok.*;

import java.time.Instant;

@Builder
@Value
public class FlagSubmissionInfo {

    private final Instant date = Instant.now();
    @NonNull
    private String flag;
    @NonNull
    private FlagSubmissionResult flagSubmissionResult;
    @NonNull
    private HackerId hackerId;
    @NonNull
    private BaseId baseId;
}
