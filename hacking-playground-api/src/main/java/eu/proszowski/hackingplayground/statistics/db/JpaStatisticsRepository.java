package eu.proszowski.hackingplayground.statistics.db;

import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaStatisticsRepository extends JpaRepository<StatisticsEntity, String> {
}
