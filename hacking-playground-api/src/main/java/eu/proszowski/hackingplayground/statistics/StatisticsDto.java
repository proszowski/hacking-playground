package eu.proszowski.hackingplayground.statistics;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Builder;
import lombok.Value;

import java.util.Collection;
import java.util.stream.Collectors;

@Value
@Builder
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class StatisticsDto {
    private Collection<StatisticInfoDto> statisticInfos;
    private Collection<FlagSubmissionInfoDto> flagSubmissionInfos;

    static StatisticsDto fromStatistics(final Statistics statistics){
        return StatisticsDto.builder()
                .flagSubmissionInfos(statistics.getFlagSubmissionInfos().stream()
                        .map(FlagSubmissionInfoDto::fromFlagSubmissionInfo)
                        .collect(Collectors.toList()))
                .statisticInfos(statistics.getStatisticInfos().stream()
                        .map(StatisticInfoDto::fromStatisticInfo)
                        .collect(Collectors.toList()))
                .build();
    }
}
