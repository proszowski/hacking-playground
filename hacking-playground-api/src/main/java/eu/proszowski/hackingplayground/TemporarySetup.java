package eu.proszowski.hackingplayground;

import static eu.proszowski.hackingplayground.TestImageNames.PHP_EXAMPLE;
import static eu.proszowski.hackingplayground.TestImageNames.PHP_HELLO_WORLD;
import static eu.proszowski.hackingplayground.game.domain.GameConfig.LOCALHOST_DOCKER;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerException;
import eu.proszowski.hackingplayground.challenge.ChallengeFacade;
import eu.proszowski.hackingplayground.common.RuntimeParameters;
import eu.proszowski.hackingplayground.authentication.AuthenticationFacade;
import eu.proszowski.hackingplayground.authentication.AuthenticationService;
import eu.proszowski.hackingplayground.authentication.UserRegister;
import eu.proszowski.hackingplayground.challenge.api.DockerChallengeCreation;
import eu.proszowski.hackingplayground.challenge.api.DockerComposeChallengeCreation;
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.flags.domain.FlagsFacade;
import eu.proszowski.hackingplayground.hacker.HackerCreation;
import eu.proszowski.hackingplayground.hacker.domain.HackerFacade;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Component
public class TemporarySetup {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    private ChallengeFacade challengeFacade;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private HackerFacade hackerFacade;

    @Autowired
    private FlagsFacade flagsFacade;

    @Autowired
    private AuthenticationFacade authenticationFacade;

    @Autowired
    RuntimeParameters runtimeParameters;

    private static final List<String> FLAGS = Arrays.asList("Flag{fsdfdsfsd}", "Flag{fsfsdfdssdfsd}");

    @PostConstruct
    void simple() throws ExecutionException, InterruptedException {
        if (!runtimeParameters.shouldInitializeMockupScenario()) {
            return;
        }
        if(hackerFacade.getAllHackers().stream().noneMatch(a -> a.getName().equals("test"))){
            authenticationFacade.registerUser(new UserRegister("test", "test@test.com", "12345678"));
        }else {
            return;
        }

        final HackerCreation admin = HackerCreation.builder()
                .id(HackerId.fromUUID(UUID.randomUUID()))
                .name("admin")
                .build();

        final HackerId adminId = hackerFacade.save(admin);

        final List<CompletableFuture<Void>> all = Arrays.asList(
                CompletableFuture.runAsync(() -> addTommysFirstBlogChallenge(adminId)),
                CompletableFuture.runAsync(() -> addHillarysGlassesChallenge(adminId)),
                CompletableFuture.runAsync(() -> addMemesChallenge(adminId)),
                CompletableFuture.runAsync(() -> addMvnToGradleChallenge(adminId)),
                CompletableFuture.runAsync(() -> addSongsScrapperChallenge(adminId)),
                CompletableFuture.runAsync(() -> addLittleCattiesChallenge(adminId))
        );

        CompletableFuture.allOf(all.toArray(new CompletableFuture[0])).get();
    }

    private void addSongsScrapperChallenge(final HackerId id) {
        DockerChallengeCreation dockerChallengeCreation = DockerChallengeCreation.builder()
                .challengeName("Simple Songs Scrapper")
                .description("Get text and chords of songs in the unique view!")
                .flags(Collections.singletonList("Flag{ItWasSoSimpleSSRF...Wasn'tIt?}"))
                .imageName("powder13/the-hacking-playground:ssrf-example")
                .imagePort(3000)
                .suggestedTags(Collections.singletonList("SSRF"))
                .sourceCodeUrl("random")
                .writeUpUrl("random")
                .build();

        challengeFacade.createNewChallenge(id, dockerChallengeCreation);
    }

    private void addMvnToGradleChallenge(final HackerId id) {
        DockerChallengeCreation dockerChallengeCreation = DockerChallengeCreation.builder()
                .challengeName("Maven to gradle converter")
                .description("Convert maven dependencies to gradle in one click!")
                .flags(Collections.singletonList("Flag{AwesomeGradleDependencies}"))
                .imageName("powder13/the-hacking-playground:mvn-to-gradle")
                .imagePort(5000)
                .suggestedTags(Collections.singletonList("XXE"))
                .sourceCodeUrl("random")
                .writeUpUrl("random")
                .build();

        challengeFacade.createNewChallenge(id, dockerChallengeCreation);
    }

    @SneakyThrows
    private void addMemesChallenge(final HackerId hackerId) {
        final File dockerComposeFile = runtimeParameters.runFromJar() ?
                new File("memes-docker-compose.yml") : new ClassPathResource("docker-compose-vulnerable-apps/memes-docker-compose.yml").getFile();
        final String dockerComposeYml = Files.readString(Paths.get(dockerComposeFile.getPath()));

        final DockerComposeChallengeCreation dockerChallengeCreation = DockerComposeChallengeCreation.builder()
                .description("Are you able to force all users to vote for your meme?")
                .dockerComposeYaml(dockerComposeYml)
                .exposedPort(8080)
                .exposedServiceName("memes")
                .challengeName("Just next site with memes")
                .flags(Collections.singletonList("Flag{YouKnowAwesomeMemesKeepGoing!}"))
                .suggestedTags(Collections.singletonList("CSRF"))
                .writeUpUrl("wrong image url")
                .sourceCodeUrl("wrong source code url")
                .build();
        challengeFacade.createNewChallenge(hackerId, dockerChallengeCreation);
    }

    private void addLittleCattiesChallenge(final HackerId firstId) {
        DockerChallengeCreation dockerChallengeCreation = DockerChallengeCreation.builder()
                .challengeName("Little Catties")
                .description("Easy challenge based on docker technology.")
                .flags(Collections.singletonList("Flag{FkIrW7JwvFJNCrC688aw}"))
                .imageName("powder13/the-hacking-playground:little-catties")
                .imagePort(5000)
                .sourceCodeUrl("random")
                .suggestedTags(Collections.singletonList("Path Traversal"))
                .writeUpUrl("random")
                .build();

        challengeFacade.createNewChallenge(firstId, dockerChallengeCreation);
    }

    @SneakyThrows
    private void addTommysFirstBlogChallenge(final HackerId hackerId) {
        final File dockerComposeFile = runtimeParameters.runFromJar() ?
                new File("tommys-first-blog-docker-compose.yml") : new ClassPathResource("docker-compose-vulnerable-apps/tommys-first-blog-docker-compose.yml").getFile();

        final String dockerComposeYml = Files.readString(Paths.get(dockerComposeFile.getPath()));

        final DockerComposeChallengeCreation dockerChallengeCreation = DockerComposeChallengeCreation.builder()
                .description("Can you find Hillary's glasses?")
                .dockerComposeYaml(dockerComposeYml)
                .exposedPort(8080)
                .exposedServiceName("blog")
                .challengeName("Tommy's first blog")
                .writeUpUrl("wrong image url")
                .flags(Collections.singletonList("Flag{49965074YouFoundMyCookies49965074}"))
                .suggestedTags(Collections.singletonList("XSS"))
                .sourceCodeUrl("wrong source code url")
                .build();
        challengeFacade.createNewChallenge(hackerId, dockerChallengeCreation);
    }

    private ChallengeId addChallenge(final HackerId hackerId) {
        final DockerChallengeCreation dockerChallengeCreation = DockerChallengeCreation.builder()
                .description("dummy description")
                .imageName(PHP_EXAMPLE)
                .challengeName("Awesome challenge")
                .imagePort(80)
                .writeUpUrl("wrong image url")
                .flags(FLAGS)
                .suggestedTags(Arrays.asList("Sql Injection", "XSS", "Hack the world"))
                .sourceCodeUrl("wrong source code url")
                .build();
        LOGGER.info("Mock challenge initialized!");
        return challengeFacade.createNewChallenge(hackerId, dockerChallengeCreation);
    }

    private ChallengeId addAnotherChallenge(final HackerId hackerId) {
        final DockerChallengeCreation dockerChallengeCreation = DockerChallengeCreation.builder()
                .description("another dummy description, this time a little bit longer to get a view how it looks.another dummy description, this time a little bit longer to get a view how it looks.another dummy description, this time a little bit longer to get a view how it looks.another dummy description, this time a little bit longer to get a view how it looks.another dummy description, this time a little bit longer to get a view how it looks.another dummy description, this time a little bit longer to get a view how it looks.")
                .imageName(PHP_HELLO_WORLD)
                .challengeName("Very easy CTF challenge for a warm up")
                .imagePort(80)
                .writeUpUrl("wrong image url")
                .flags(Collections.singletonList("Flag{hello-world}"))
                .suggestedTags(Arrays.asList("Sql Injection", "XSS", "Hack the world"))
                .sourceCodeUrl("wrong source code url")
                .build();
        LOGGER.info("Another mock challenge initialized!");
        return challengeFacade.createNewChallenge(hackerId, dockerChallengeCreation);
    }

    @SneakyThrows
    private ChallengeId addHillarysGlassesChallenge(final HackerId hackerId) {
        final File dockerComposeFile = runtimeParameters.runFromJar() ?
                new File("where-are-my-glasses-docker-compose.yml") : new ClassPathResource("docker-compose-vulnerable-apps/where-are-my-glasses-docker-compose.yml").getFile();
        final String dockerComposeYml = Files.readString(Paths.get(dockerComposeFile.getPath()));

        final DockerComposeChallengeCreation dockerChallengeCreation = DockerComposeChallengeCreation.builder()
                .description("Can you find Hillary's glasses?")
                .dockerComposeYaml(dockerComposeYml)
                .exposedPort(5000)
                .exposedServiceName("server")
                .challengeName("Where are my glasses?")
                .writeUpUrl("wrong image url")
                .flags(Collections.singletonList("Flag{YouHaveFoundMyGlasses.Congratulations!}"))
                .suggestedTags(Collections.singletonList("Sql Injection"))
                .sourceCodeUrl("wrong source code url")
                .build();
        return challengeFacade.createNewChallenge(hackerId, dockerChallengeCreation);
    }

    @PreDestroy
    public void onDestroy() throws DockerException, InterruptedException {
        final DockerClient dockerClient = new DefaultDockerClient(LOCALHOST_DOCKER);
        dockerClient.listContainers().forEach(
                container -> {
                    try {
                        dockerClient.killContainer(container.id());
                    } catch (DockerException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
        );
    }
}
