package eu.proszowski.hackingplayground.config;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.netflix.util.Pair;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import eu.proszowski.hackingplayground.common.LinksConverter;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class PostFilter extends ZuulFilter {

    private static final String X_FORWARDED_PREFIX = "x-forwarded-prefix";
    private static final String FILTER_TYPE = "post";
    private static final int FILTER_ORDER = 999;
    private static final String CONTENT_TYPE = "content-type";
    private static final String HTML_INDICATOR = "html";


    @Override
    public String filterType() {
        return FILTER_TYPE;
    }

    @Override
    public int filterOrder() {
        return FILTER_ORDER;
    }

    @Override
    public boolean shouldFilter() {
        final RequestContext ctx = RequestContext.getCurrentContext();
        return responseHeadersHaveContentTypeHtml(ctx) || responseHeadersHaveLocation(ctx);
    }

    private boolean responseHeadersHaveContentTypeHtml(final RequestContext ctx) {
        return ctx.getZuulResponseHeaders().stream()
                .filter(a -> a.first().equalsIgnoreCase(CONTENT_TYPE))
                .map(Pair::second)
                .anyMatch(a -> a.contains(HTML_INDICATOR));
    }

    private boolean responseHeadersHaveLocation(final RequestContext ctx) {
        return ctx.getZuulResponseHeaders().stream()
                .map(Pair::first)
                .anyMatch(a -> a.equals("Location"));
    }

    @Override
    public Object run() {
        final RequestContext ctx = RequestContext.getCurrentContext();

        if(responseHeadersHaveLocation(ctx)){
            final Map<String, String> zuulRequestHeaders = ctx.getZuulRequestHeaders();
            final String host = zuulRequestHeaders.get("x-forwarded-host");
            final String path = zuulRequestHeaders.get("x-forwarded-prefix");

            final String location = "http://" + host + path + "/";

            final List<Pair<String,String>> zuulResponseHeaders = ctx.getZuulResponseHeaders();
            final Multimap<String, String> responseHeaders = MultimapBuilder.hashKeys().hashSetValues().build();
            zuulResponseHeaders.forEach(a -> responseHeaders.put(a.first(), a.second()));

            final String zuulService = ctx.getOriginResponseHeaders().stream()
                    .filter(a -> a.first().equals("X-Zuul-Service"))
                    .findFirst()
                    .map(Pair::second).get();

            final String zuulLocation = zuulResponseHeaders.stream()
                    .filter(a -> a.first().equals("Location"))
                    .findFirst()
                    .map(Pair::second).get();

            final List<Pair<String, String>> modifiedZuulHeaders = zuulResponseHeaders.stream().filter(a -> !a.first().equals("Location")).collect(Collectors.toList());
            modifiedZuulHeaders.add(new Pair<>("Location", zuulLocation.replace(zuulService, location)));

            ctx.set("zuulResponseHeaders", modifiedZuulHeaders);
            ctx.getZuulResponseHeaders();
        }


        if(responseHeadersHaveContentTypeHtml(ctx)){
            final InputStream inputStream = ctx.getResponseDataStream();
            try {
                ctx.setResponseDataStream(getResponse(inputStream, ctx));
            } catch (final IOException e) {
                e.printStackTrace();
            }

        }
        //Returned value isn't used in Zuul implementation
        return null;
    }

    private InputStream getResponse(final InputStream inputStream, final RequestContext ctx) throws IOException {
        if(ctx.getResponseGZipped()){
            return inputStream;
        }else {
            final String response = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
            final String fixedResponse = getFixedResponse(ctx, response);
            return IOUtils.toInputStream(fixedResponse, StandardCharsets.UTF_8);
        }
    }

    private String getFixedResponse(final RequestContext ctx, final String response) {
        return LinksConverter.getHtmlWithFixedUrls(response, ctx.getZuulRequestHeaders().get(X_FORWARDED_PREFIX));
    }
}

