package eu.proszowski.hackingplayground.config;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import com.netflix.zuul.http.HttpServletRequestWrapper;
import eu.proszowski.hackingplayground.statistics.StatisticInfo;
import eu.proszowski.hackingplayground.statistics.StatisticsFacade;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Component
public class PreFilter extends ZuulFilter {

    @Autowired
    private StatisticsFacade statisticsFacade;

    public static final String FILTER_TYPE = "pre";

    @Override
    public String filterType() {
        return FILTER_TYPE;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
        final RequestContext ctx = RequestContext.getCurrentContext();
        final HttpServletRequest request = new HttpServletRequestWrapper(ctx.getRequest());

        final Map<String, String> headers = getHeaders(request);
        final String uri = request.getRequestURI();
        final String method = request.getMethod();
        final String queryString = request.getQueryString();
        final Map<String, String[]> parameters = request.getParameterMap();
        final String body = getBody(ctx);

        final StatisticInfo statisticInfo = StatisticInfo.builder()
                .headers(headers)
                .uri(uri)
                .method(HttpMethod.resolve(method))
                .queryString(queryString)
                .parameters(parameters)
                .body(body)
                .build();

        statisticsFacade.notifyAboutRequest(statisticInfo);

        return null;
    }

    @SneakyThrows(IOException.class)
    private String getBody(final RequestContext context) {
        InputStream in = (InputStream) context.get("requestEntity");
        if (in == null) {
            in = context.getRequest().getInputStream();
        }
        return StreamUtils.copyToString(in, StandardCharsets.UTF_8);
    }

    private Map<String, String> getHeaders(final HttpServletRequest request) {
        final Map<String, String> headers = new HashMap<>();
        request.getHeaderNames()
                .asIterator()
                .forEachRemaining(headerName -> headers.put(headerName, request.getHeader(headerName)));
        return headers;
    }
}
