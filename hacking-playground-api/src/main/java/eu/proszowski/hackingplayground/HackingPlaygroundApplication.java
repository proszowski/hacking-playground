package eu.proszowski.hackingplayground;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
@EnableZuulProxy
public class HackingPlaygroundApplication {

    public static void main(final String[] args) {
        SpringApplication.run(HackingPlaygroundApplication.class, args);
    }

}
