package eu.proszowski.hackingplayground.flags.domain;

public class FlagAlreadyHasBeenSubmitted extends RuntimeException{
    public FlagAlreadyHasBeenSubmitted() {
        super("Flag already has been submitted");
    }
}
