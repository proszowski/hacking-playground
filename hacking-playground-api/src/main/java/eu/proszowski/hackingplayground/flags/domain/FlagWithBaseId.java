package eu.proszowski.hackingplayground.flags.domain;

public interface FlagWithBaseId {
    BaseId getBaseId();
    Flag getFlag();
}
