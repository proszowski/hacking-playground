package eu.proszowski.hackingplayground.flags.domain;

import java.util.UUID;

public interface BaseId {
    UUID getRaw();
}
