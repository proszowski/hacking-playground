package eu.proszowski.hackingplayground.flags.infrastracture.db;

import eu.proszowski.hackingplayground.flags.domain.BaseId;
import eu.proszowski.hackingplayground.flags.domain.DefaultBaseId;
import eu.proszowski.hackingplayground.flags.domain.DefaultFlag;
import eu.proszowski.hackingplayground.flags.domain.DefaultFlagWithBaseId;
import eu.proszowski.hackingplayground.flags.domain.Flag;
import eu.proszowski.hackingplayground.flags.domain.FlagWithBaseId;
import eu.proszowski.hackingplayground.flags.domain.FlagsRepository;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class PostgresFlagsDatabase implements FlagsRepository {

    private final JpaBaseToFlagsRepository baseToFlagsRepository;
    private final JpaHackerToFlagRepository hackerToFlagRepository;

    PostgresFlagsDatabase(final JpaBaseToFlagsRepository baseToFlagsRepository,
                          final JpaHackerToFlagRepository hackerToFlagRepository) {

        this.baseToFlagsRepository = baseToFlagsRepository;
        this.hackerToFlagRepository = hackerToFlagRepository;
    }

    @Override
    public void associateBaseWithFlags(final BaseId baseId,
                                       final Set<Flag> flags) {
        final BaseToFlagsDbDto baseToFlags = new BaseToFlagsDbDto(baseId.getRaw(), flags.stream().map(Flag::getRaw).collect(Collectors.toSet()));
        this.baseToFlagsRepository.save(baseToFlags);
    }

    @Override
    public int howManyFlags(final BaseId baseId) {
        return baseToFlagsRepository.findById(baseId.getRaw())
                .map(a -> a.getFlags().size())
                .orElse(0);
    }

    @Override
    public Set<Flag> findFlags(final BaseId baseId) {
        return baseToFlagsRepository.findById(baseId.getRaw())
                .map(a -> a.getFlags().stream().map(DefaultFlag::fromString).map(x -> (Flag)x).collect(Collectors.toSet()))
                .orElse(Collections.emptySet());
    }

    @Override
    public boolean associateHackerWithFlag(final HackerId hackerId,
                                           final FlagWithBaseId flagWithBaseId) {
        log.info("associating hacker with id: {}", hackerId);
        final Optional<HackerToFlags> hackerToFlags = hackerToFlagRepository.findById(hackerId.getRaw());

        final Collection<DefaultFlagWithBaseId> flags = hackerToFlags
                .map(HackerToFlags::getFlags)
                .map(Collection::stream)
                .map(a -> a.map(x -> new DefaultFlagWithBaseId(DefaultBaseId.fromUUID(x.getBaseId()), DefaultFlag.fromString(x.getFlag()))))
                .map(a -> a.collect(Collectors.toSet()))
                .orElse(new HashSet<>());

        log.info("flags before: {}", flags);
        log.info("flagWithBaseId : {}", flagWithBaseId);
        if (flags.contains(flagWithBaseId)) {
            return false;
        }else{
            final Set<FlagWitBaseIdDbDto> newFlags = Stream.concat(flags.stream(), Stream.of(flagWithBaseId))
                    .map(a -> new FlagWitBaseIdDbDto(a.getFlag().getRaw(), a.getBaseId().getRaw()))
                    .collect(Collectors.toSet());

            hackerToFlagRepository.save(new HackerToFlags(hackerId.getRaw(), newFlags));
            log.info("flags after: {}", hackerToFlagRepository.findById(hackerId.getRaw()));

            return true;
        }
    }

    @Override
    public int howManyFlagsHaveBeenFoundByGivenHackerInGivenBase(final HackerId hackerId,
                                                                 final BaseId baseId) {
        return hackerToFlagRepository.findById(hackerId.getRaw())
                .map(HackerToFlags::getFlags)
                .map(a -> a.stream().filter(x -> x.getBaseId().equals(baseId.getRaw())).collect(Collectors.toSet()))
                .map(Collection::size)
                .orElse(0);
    }

    @Override
    public Set<FlagWithBaseId> getCapturedFlagsByHacker(final HackerId hackerId) {
        log.info("flags: {} for hacker with id: {}", hackerToFlagRepository.findById(hackerId.getRaw())
                .map(HackerToFlags::getFlags), hackerId);

        log.info("all flags: {}", hackerToFlagRepository.findAll());

        return hackerToFlagRepository.findById(hackerId.getRaw())
                .map(HackerToFlags::getFlags)
                .map(Collection::stream)
                .map(a -> a.map( x -> new DefaultFlagWithBaseId(DefaultBaseId.fromUUID(x.getBaseId()), DefaultFlag.fromString(x.getFlag()))).map(x -> (FlagWithBaseId)x))
                .map(a -> a.collect(Collectors.toSet()))
                .orElse(Collections.emptySet());
    }
}
