package eu.proszowski.hackingplayground.flags.infrastracture.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;
import java.util.UUID;

@Entity
@Table(name = "hacker_to_flags")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HackerToFlags {

    @Id
    private UUID hackerId;

    @OneToMany
    @Cascade(CascadeType.ALL)
    private Collection<FlagWitBaseIdDbDto> flags;
}
