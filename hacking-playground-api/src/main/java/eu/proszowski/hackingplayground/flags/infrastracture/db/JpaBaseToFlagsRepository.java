package eu.proszowski.hackingplayground.flags.infrastracture.db;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface JpaBaseToFlagsRepository extends JpaRepository<BaseToFlagsDbDto, UUID> {
}
