package eu.proszowski.hackingplayground.flags.domain;

import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import java.util.Set;

public interface FlagsRepository {
    void associateBaseWithFlags(BaseId baseId, Set<Flag> flags);

    int howManyFlags(BaseId baseId);

    Set<Flag> findFlags(BaseId baseId);

    boolean associateHackerWithFlag(HackerId hackerId, FlagWithBaseId flagWithBaseId);

    int howManyFlagsHaveBeenFoundByGivenHackerInGivenBase(HackerId hackerId, BaseId baseId);

    Set<FlagWithBaseId> getCapturedFlagsByHacker(HackerId hackerId);
}
