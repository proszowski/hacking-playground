package eu.proszowski.hackingplayground.flags.api;

import lombok.NonNull;
import lombok.Value;
import java.util.UUID;

@Value
public class FlagWithBaseIdDto {
    @NonNull
    private String flag;
    @NonNull
    private UUID baseId;
}
