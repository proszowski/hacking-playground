package eu.proszowski.hackingplayground.flags.domain;

import lombok.Value;
import java.util.UUID;

@Value(staticConstructor = "fromUUID")
public class DefaultBaseId implements BaseId {
    private UUID raw;
}
