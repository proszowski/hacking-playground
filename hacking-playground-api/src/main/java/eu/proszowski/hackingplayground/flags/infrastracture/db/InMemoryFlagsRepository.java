package eu.proszowski.hackingplayground.flags.infrastracture.db;

import eu.proszowski.hackingplayground.flags.domain.BaseDoesNotExistException;
import eu.proszowski.hackingplayground.flags.domain.BaseId;
import eu.proszowski.hackingplayground.flags.domain.Flag;
import eu.proszowski.hackingplayground.flags.domain.FlagWithBaseId;
import eu.proszowski.hackingplayground.flags.domain.FlagsRepository;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import org.glassfish.jersey.internal.guava.Sets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class InMemoryFlagsRepository implements FlagsRepository {

    private final Map<BaseId, Set<Flag>> baseIdAssociatedWithFlags = new HashMap<>();
    private final Map<HackerId, Set<FlagWithBaseId>> hackerIdAssociatedWithFlagWithBaseId = new HashMap<>();

    @Override
    public void associateBaseWithFlags(final BaseId baseId, final Set<Flag> flags) {
        baseIdAssociatedWithFlags.put(baseId, flags);
    }

    @Override
    public int howManyFlags(final BaseId baseId) {
        return Optional.ofNullable(baseIdAssociatedWithFlags.get(baseId))
                .map(Set::size)
                .orElseThrow(() -> new BaseDoesNotExistException(baseId));
    }

    @Override
    public Set<Flag> findFlags(final BaseId baseId) {
        final Set<Flag> flags = baseIdAssociatedWithFlags.get(baseId);
        if (flags == null) {
            return Collections.emptySet();
        }

        return flags;
    }

    @Override
    public boolean associateHackerWithFlag(final HackerId hackerId, final FlagWithBaseId flagWithBaseId) {
        final Set<FlagWithBaseId> flags = hackerIdAssociatedWithFlagWithBaseId.getOrDefault(hackerId, Sets.newHashSet());
        if (flags.contains(flagWithBaseId)) {
            return false;
        } else {
            flags.add(flagWithBaseId);
            hackerIdAssociatedWithFlagWithBaseId.put(hackerId, flags);
            return true;
        }
    }

    @Override
    public int howManyFlagsHaveBeenFoundByGivenHackerInGivenBase(final HackerId hackerId, final BaseId baseId) {
        final Set<FlagWithBaseId> flags = Optional.ofNullable(hackerIdAssociatedWithFlagWithBaseId.get(hackerId))
                .orElse(Collections.emptySet());

        return (int) flags.stream()
                .filter(flagWithBaseId -> flagWithBaseId.getBaseId().equals(baseId))
                .count();
    }

    @Override
    public Set<FlagWithBaseId> getCapturedFlagsByHacker(final HackerId hackerId) {
        return hackerIdAssociatedWithFlagWithBaseId.getOrDefault(hackerId, Collections.emptySet());
    }
}
