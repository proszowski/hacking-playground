package eu.proszowski.hackingplayground.flags.domain;

import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import eu.proszowski.hackingplayground.statistics.FlagSubmissionInfo;
import eu.proszowski.hackingplayground.statistics.StatisticsFacade;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FlagsFacade {

    private final FlagsRepository flagsRepository;

    private final Map<UUID, Consumer<UUID>> onBaseCapturedConsumers = new HashMap<>();
    private final StatisticsFacade statisticsFacade;

    public FlagsFacade(final FlagsRepository flagsRepository, final StatisticsFacade statisticsFacade) {
        this.flagsRepository = flagsRepository;
        this.statisticsFacade = statisticsFacade;
    }

    public void associateBaseIdWithFlags(final BaseId baseId, final Set<Flag> flags) {
        flagsRepository.associateBaseWithFlags(baseId, flags);
    }

    public int howManyFlags(final BaseId baseId) {
        return flagsRepository.howManyFlags(baseId);
    }

    public FlagSubmissionResult captureTheFlag(final HackerId hackerId, final BaseId baseId, final Flag flag) {
        final Set<Flag> flags = flagsRepository.findFlags(baseId);
        if (flags.contains(flag)) {
            return associateHackerWithFlag(hackerId, baseId, flag);
        }
        notifyStatisticsFacade(hackerId, baseId, flag, FlagSubmissionResult.FAILURE);
        return FlagSubmissionResult.FAILURE;
    }

    private FlagSubmissionResult associateHackerWithFlag(final HackerId hackerId, final BaseId baseId, final Flag flag) {
        final FlagWithBaseId flagWithBaseId = new DefaultFlagWithBaseId(baseId, flag);
        final boolean associationResult = flagsRepository.associateHackerWithFlag(hackerId, flagWithBaseId);
        if (associationResult) {
            reactOnBaseCapture(hackerId, baseId);
            notifyStatisticsFacade(hackerId, baseId, flag, FlagSubmissionResult.SUCCESS);
            return FlagSubmissionResult.SUCCESS;
        } else {
            notifyStatisticsFacade(hackerId, baseId, flag, FlagSubmissionResult.ALREADY_SUBMITTED);
            return FlagSubmissionResult.ALREADY_SUBMITTED;
        }
    }

    private void notifyStatisticsFacade(final HackerId hackerId,
                                        final BaseId baseId,
                                        final Flag flag,
                                        final FlagSubmissionResult failure) {
        statisticsFacade.notifyAboutFlagSubmission(FlagSubmissionInfo.builder()
                .baseId(baseId)
                .flag(flag.getRaw())
                .flagSubmissionResult(failure)
                .hackerId(hackerId)
                .build());
    }

    private void reactOnBaseCapture(final HackerId hackerId, final BaseId baseId) {
        final int totalNumberOfFlagsForGivenBaseId = flagsRepository.howManyFlags(baseId);
        final int flagsFoundByUserId = flagsRepository.howManyFlagsHaveBeenFoundByGivenHackerInGivenBase(hackerId, baseId);
        if (totalNumberOfFlagsForGivenBaseId == flagsFoundByUserId) {
            onBaseCapturedConsumers.get(baseId.getRaw()).accept(baseId.getRaw());
        }
    }

    public int howManyFlagsHaveBeenFoundByGivenHackerInGivenBase(final HackerId hackerId, final BaseId baseId) {
        return flagsRepository.howManyFlagsHaveBeenFoundByGivenHackerInGivenBase(hackerId, baseId);
    }

    public List<BaseId> getBaseIdsCapturedByGivenHacker(final HackerId hackerId){
        return flagsRepository.getCapturedFlagsByHacker(hackerId).stream()
                .map(FlagWithBaseId::getBaseId)
                .collect(Collectors.toList());
    }

    public void subscribeForBaseCapture(final UUID id, final Consumer<UUID> consumer) {
        onBaseCapturedConsumers.put(id, consumer);
    }
}
