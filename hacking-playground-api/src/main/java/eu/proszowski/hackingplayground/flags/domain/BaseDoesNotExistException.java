package eu.proszowski.hackingplayground.flags.domain;

public class BaseDoesNotExistException extends RuntimeException {
    public BaseDoesNotExistException(final BaseId baseId) {
        super(String.format("Base with id %s does not exist", baseId));
    }
}
