package eu.proszowski.hackingplayground.flags.api;

import eu.proszowski.hackingplayground.flags.domain.FlagSubmissionResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

@RestController
@RequestMapping("flags")
public interface FlagsController {

    @GetMapping("howMuch/{baseId}")
    int getNumberOfFlagsForGivenBaseId(@PathVariable UUID baseId);

    @GetMapping("howMuchIHave/{baseId}")
    int getNumberOfFlagsForGivenBaseIdAndGivenHacker(@PathVariable UUID baseId);

    @PostMapping("captureTheFlag")
    FlagSubmissionResult captureTheFlag(@RequestBody FlagWithBaseIdDto flagWithBaseIdDto);

}
