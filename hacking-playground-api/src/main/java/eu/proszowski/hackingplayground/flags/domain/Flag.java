package eu.proszowski.hackingplayground.flags.domain;

public interface Flag {
    String getRaw();
}
