package eu.proszowski.hackingplayground.flags.domain;

import lombok.Value;

@Value(staticConstructor = "fromString")
public class DefaultFlag implements Flag {
    private String raw;
}
