package eu.proszowski.hackingplayground.flags.infrastracture.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "base_to_flags")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseToFlagsDbDto {

    @Id
    private UUID baseId;

    @ElementCollection
    private Set<String> flags;
}
