package eu.proszowski.hackingplayground.flags.infrastracture.db;

import eu.proszowski.hackingplayground.flags.domain.FlagsRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DbConfig {

    @Bean
    FlagsRepository flagsRepository(final JpaBaseToFlagsRepository jpaBaseToFlagsRepository,
                                    final JpaHackerToFlagRepository jpaHackerToFlagRepository){

        return new PostgresFlagsDatabase(jpaBaseToFlagsRepository, jpaHackerToFlagRepository);
    }
}
