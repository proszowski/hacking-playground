package eu.proszowski.hackingplayground.flags.domain;

import lombok.Value;

@Value
public class DefaultFlagWithBaseId implements FlagWithBaseId {
    BaseId baseId;
    Flag flag;
}
