package eu.proszowski.hackingplayground.flags.domain;

import eu.proszowski.hackingplayground.statistics.StatisticsFacade;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FlagsConfig {

    @Bean
    FlagsFacade flagsFacade(final StatisticsFacade statisticsFacade, final FlagsRepository flagsRepository){
        return new FlagsFacade(flagsRepository, statisticsFacade);
    }
}
