package eu.proszowski.hackingplayground.flags.infrastracture.db;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "flags_with_base_ids")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlagWitBaseIdDbDto {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;

    private String flag;

    private UUID baseId;

    FlagWitBaseIdDbDto(final String flag,
                       final UUID baseId) {
        this.flag = flag;
        this.baseId = baseId;
    }
}
