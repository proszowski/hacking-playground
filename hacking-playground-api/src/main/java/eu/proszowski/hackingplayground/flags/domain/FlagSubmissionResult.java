package eu.proszowski.hackingplayground.flags.domain;

public enum FlagSubmissionResult {
    SUCCESS, FAILURE, ALREADY_SUBMITTED
}
