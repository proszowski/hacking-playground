package eu.proszowski.hackingplayground.flags.api;

import eu.proszowski.hackingplayground.flags.domain.DefaultFlag;
import eu.proszowski.hackingplayground.flags.domain.FlagSubmissionResult;
import eu.proszowski.hackingplayground.authentication.AuthenticationService;
import eu.proszowski.hackingplayground.flags.domain.DefaultBaseId;
import eu.proszowski.hackingplayground.flags.domain.FlagsFacade;
import eu.proszowski.hackingplayground.hacker.domain.HackerFacade;
import eu.proszowski.hackingplayground.hacker.domain.HackerId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.UUID;

@Component
public class DefaultFlagsController implements FlagsController {

    @Autowired
    private FlagsFacade flagsFacade;

    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public int getNumberOfFlagsForGivenBaseId(final UUID baseId) {
        return flagsFacade.howManyFlags(DefaultBaseId.fromUUID(baseId));
    }

    @Override
    public int getNumberOfFlagsForGivenBaseIdAndGivenHacker(final UUID baseId) {
        final HackerId hackerId = HackerId.fromUUID(authenticationService.getUserId());
        return flagsFacade.howManyFlagsHaveBeenFoundByGivenHackerInGivenBase(hackerId, DefaultBaseId.fromUUID(baseId));
    }

    @Override
    public FlagSubmissionResult captureTheFlag(final FlagWithBaseIdDto flagWithBaseIdDto) {
        final HackerId hackerId = HackerId.fromUUID(authenticationService.getUserId());
        final UUID baseId = flagWithBaseIdDto.getBaseId();
        final String flag = flagWithBaseIdDto.getFlag();
        return flagsFacade.captureTheFlag(hackerId, DefaultBaseId.fromUUID(baseId), DefaultFlag.fromString(flag));
    }
}
