package eu.proszowski.hackingplayground.ranking;

import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class DefaultUsersRankingController implements UsersRankingController {

    private final RankingFacade rankingFacade;

    public DefaultUsersRankingController(final RankingFacade rankingFacade) {
        this.rankingFacade = rankingFacade;
    }

    @Override
    public List<HackerDto> getHackersByScoreDescending() {
        return rankingFacade.getHackersByScoreDescending();
    }
}
