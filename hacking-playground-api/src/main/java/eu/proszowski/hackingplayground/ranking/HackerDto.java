package eu.proszowski.hackingplayground.ranking;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import java.util.UUID;

@Value
@Builder
public class HackerDto {
    @NonNull
    private UUID id;
    @NonNull
    private String name;

    private double score;
}
