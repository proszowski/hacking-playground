package eu.proszowski.hackingplayground.ranking;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("users-ranking")
public interface UsersRankingController {

    @GetMapping("by-score-descending")
    List<HackerDto> getHackersByScoreDescending();

}
