package eu.proszowski.hackingplayground.ranking;

import eu.proszowski.hackingplayground.challenge.ChallengeFacade;
import eu.proszowski.hackingplayground.challenge.api.ChallengeDto;
import eu.proszowski.hackingplayground.challenge.domain.ChallengeId;
import eu.proszowski.hackingplayground.flags.domain.FlagsFacade;
import eu.proszowski.hackingplayground.hacker.domain.Hacker;
import eu.proszowski.hackingplayground.flags.domain.BaseId;
import eu.proszowski.hackingplayground.hacker.domain.HackerFacade;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RankingFacade {

    private final HackerFacade hackerFacade;

    private final FlagsFacade flagsFacade;

    private final ChallengeFacade challengeFacade;

    public RankingFacade(final HackerFacade hackerFacade, final FlagsFacade flagsFacade, final ChallengeFacade challengeFacade) {
        this.hackerFacade = hackerFacade;
        this.flagsFacade = flagsFacade;
        this.challengeFacade = challengeFacade;
    }

    public List<HackerDto> getHackersByScoreDescending() {
        final List<Hacker> hackers = hackerFacade.getAllHackers();
        return hackers.stream()
                .collect(Collectors.toMap(
                        hacker -> hacker,
                        hacker -> flagsFacade.getBaseIdsCapturedByGivenHacker(hacker.getId())))
                .entrySet()
                .stream()
                .map(this::mapToHackerWithPosition)
                .sorted(Comparator.comparingDouble(HackerDto::getScore).reversed())
                .collect(Collectors.toList());
    }

    private HackerDto mapToHackerWithPosition(final Map.Entry<Hacker, List<BaseId>> entry) {
        final Hacker hacker = entry.getKey();
        final double score = countScore(entry.getValue());
        return HackerDto.builder()
                .id(hacker.getId().getRaw())
                .name(hacker.getName())
                .score(score)
                .build();
    }

    private double countScore(final List<BaseId> value) {
        return value.stream()
                .map(id -> challengeFacade.getChallenge(ChallengeId.fromUUID(id.getRaw())))
                .map(ChallengeDto::getDifficulty)
                .mapToDouble(Double::doubleValue)
                .sum();
    }
}
