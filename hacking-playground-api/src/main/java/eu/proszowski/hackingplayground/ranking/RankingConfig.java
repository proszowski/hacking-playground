package eu.proszowski.hackingplayground.ranking;

import eu.proszowski.hackingplayground.challenge.ChallengeFacade;
import eu.proszowski.hackingplayground.flags.domain.FlagsFacade;
import eu.proszowski.hackingplayground.hacker.domain.HackerFacade;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class RankingConfig {

    @Bean
    RankingFacade rankingFacade(final HackerFacade hackerFacade,
                                final FlagsFacade flagsFacade,
                                final ChallengeFacade challengeFacade) {
        return new RankingFacade(hackerFacade, flagsFacade, challengeFacade);
    }
}
