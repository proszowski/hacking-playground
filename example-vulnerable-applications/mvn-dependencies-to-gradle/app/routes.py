import os
from app import app
from flask import render_template, request, send_file
#import xml.etree.ElementTree as ET
from lxml import etree

@app.route('/')
def index():
    return render_template('index.html', title='Convert Maven Dependencies to Gradle Dependencies')

@app.route('/', methods=['POST'])
def convert():
    gradleDependencies = 'dependencies {\n'
    mvnToConvert = request.form.get('mvnToConvert')
    try:
        root = etree.fromstring(mvnToConvert)

        for child in root:
            groupId = child.find('groupId').text
            artifactId = child.find('artifactId').text
            versionTag = child.find('version')
            version = ":" + versionTag.text if versionTag != None else ''
            scopeTag = child.find('scope')
            scope = scopeTag.text if scopeTag != None else ''
            if scope == '':
                scope = 'implementation'
            elif scope == 'compile':
                scope = 'implementation'
            elif scope == 'provided':
                scope = 'compileOnly'
            elif scope == 'runtime':
                scope = 'runtime'
            elif scope == 'test':
                scope = 'testImplementation'

            gradleDependencies += '\t' + scope + ' \'' + groupId + ':' + artifactId + version + '\'' + '\n'

        gradleDependencies += '}'
    except etree.ParseError:
        return errorPage(mvnToConvert)

    return render_template('index.html', title='Convert Maven Dependencies to Gradle Dependencies', mvnToConvert=mvnToConvert, gradleDependencies=gradleDependencies)

def errorPage(mvnToConvert):
    return render_template('index.html', title='Convert Maven Dependencies to Gradle Dependencies', errorMessage="Invalid xml!", mvnToConvert=mvnToConvert)
