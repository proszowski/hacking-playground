package eu.proszowski.vulnerable.xssexample;

import java.util.Collection;
import java.util.UUID;

public interface CommentsRepository {
    Collection<Comment> findByPostId(UUID id);
    void add(Comment comment);
}
