package eu.proszowski.vulnerable.xssexample;

import java.util.List;
import java.util.UUID;

interface BlogPostsRepository {

    List<BlogPost> findAll();

    BlogPost findById(UUID id);
}
