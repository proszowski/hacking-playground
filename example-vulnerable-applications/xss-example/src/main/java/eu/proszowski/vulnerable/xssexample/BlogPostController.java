package eu.proszowski.vulnerable.xssexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;
import java.util.UUID;

@Controller
@RequestMapping("/blog-post")
public class BlogPostController {

    @Autowired
    private BlogPostsRepository blogPostsRepository;

    @Autowired
    private CommentsRepository commentsRepository;

    @GetMapping("{id}")
    String blogPost(Model model, @PathVariable UUID id){
        model.addAttribute("commentDto", new CommentDto());
        model.addAttribute("blogPost", blogPostsRepository.findById(id));
        model.addAttribute("comments", commentsRepository.findByPostId(id));
        return "blogPost.html";
    }

    @PostMapping("/{id}/add-comment")
    RedirectView addComment(Model model, @PathVariable UUID id, CommentDto commentDto){
        System.out.println(commentDto);
        final Comment comment = Comment.builder()
                .postId(id)
                .author(commentDto.getAuthor())
                .content(commentDto.getContent())
                .build();
        commentsRepository.add(comment);
        return new RedirectView("/blog-post/" + id);
    }
}
