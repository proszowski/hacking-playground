package eu.proszowski.vulnerable.xssexample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class Scenario {

    @Autowired
    private BlogPostsRepository blogPostsRepository;

    @Autowired
    private CommentsRepository commentsRepository;

    @PostConstruct
    void addCommentsToPosts() {

        final List<UUID> ids = blogPostsRepository.findAll()
                .stream()
                .map(BlogPost::getId)
                .collect(Collectors.toList());

        final Comment comment = Comment.builder().id(UUID.randomUUID()).postId(ids.get(0)).author("Piotr Proszowski").content("Very good content, keep it up!").build();
        final Comment secondComment = Comment.builder().id(UUID.randomUUID()).postId(ids.get(1)).author("Bartek Nowicki").content("Did you think about writing a book 'bout that?").build();
        final Comment thirdComment = Comment.builder().id(UUID.randomUUID()).postId(ids.get(2)).author("Andrzej Stachura").content("I like what you are doing, that's great!").build();

        commentsRepository.add(comment);
        commentsRepository.add(secondComment);
        commentsRepository.add(thirdComment);

    }
}
