package eu.proszowski.vulnerable.xssexample;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import java.time.Instant;
import java.util.UUID;

@Value
@Builder
class Comment {
    @Builder.Default
    private UUID id = UUID.randomUUID();
    @NonNull
    private UUID postId;
    @NonNull
    private String author;
    private final Instant creationDate = Instant.now();
    @NonNull
    private String content;
}
