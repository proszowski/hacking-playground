package eu.proszowski.vulnerable.xssexample;

import static org.springframework.http.HttpHeaders.SET_COOKIE;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthController {
    private static final String UNGUESSABLE_STRING = "49965074-fd8f-45ef-8073-fb85cb613a52";
    private static final String UNGUESSABLE_FLAG = "Flag{49965074YouFoundMyCookies49965074}";

    @GetMapping(UNGUESSABLE_STRING)
    ResponseEntity<Void> getSecretCookie(){
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(SET_COOKIE, "login=admin");
        httpHeaders.add(SET_COOKIE, "password=" + UNGUESSABLE_STRING);
        httpHeaders.add(SET_COOKIE, "flag=" + UNGUESSABLE_FLAG);
        return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    }
}
