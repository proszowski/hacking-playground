package eu.proszowski.vulnerable.xssexample;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import org.springframework.stereotype.Component;
import java.util.Collection;
import java.util.UUID;

@Component
public class CommentsDummyRepository implements CommentsRepository {
    private Multimap<UUID, Comment> commentsGroupedByPostId = MultimapBuilder.hashKeys().hashSetValues().build();

    @Override
    public Collection<Comment> findByPostId(final UUID id) {
        return commentsGroupedByPostId.get(id);
    }

    @Override
    public void add(final Comment comment) {
        commentsGroupedByPostId.put(comment.getPostId(), comment);
    }
}
