package eu.proszowski.vulnerable.xssexample;

import lombok.Data;

@Data
class CommentDto {
    private String author;
    private String content;
}
