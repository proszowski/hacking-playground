package eu.proszowski.vulnerable.xssexample;

import org.springframework.stereotype.Component;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Component
public class BlogPostsDummyRepository implements BlogPostsRepository {

    private static final String LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam a tortor consectetur, convallis sem id, ultricies ipsum. Ut suscipit et massa eu semper. Aliquam interdum egestas mauris, eu tincidunt eros dapibus vel. Suspendisse ullamcorper lectus vitae placerat eleifend. Donec nulla ante, faucibus at ligula lacinia, pretium ullamcorper neque. Proin congue mi at sapien cursus, vitae pulvinar ex tristique. Maecenas pretium, tellus dictum feugiat ornare, sapien nibh maximus eros, quis tempus nunc mauris quis ex.";

    private List<BlogPost> BLOG_POSTS = Arrays.asList(
            new BlogPost(UUID.randomUUID(), "How did I celebrate my 23rd birthday?", LOREM_IPSUM),
            new BlogPost(UUID.randomUUID(), "10 tricks which every developer should know", LOREM_IPSUM),
            new BlogPost(UUID.randomUUID(), "My first blog", LOREM_IPSUM)
    );

    @Override
    public List<BlogPost> findAll() {
        return BLOG_POSTS;
    }

    @Override
    public BlogPost findById(final UUID id) {
        return BLOG_POSTS.stream()
                .filter(a -> a.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("There is no blog post with given id"));
    }
}
