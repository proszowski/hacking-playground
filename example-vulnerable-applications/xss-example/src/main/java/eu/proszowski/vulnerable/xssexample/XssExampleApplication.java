package eu.proszowski.vulnerable.xssexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XssExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(XssExampleApplication.class, args);
	}

}
