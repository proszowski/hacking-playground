package eu.proszowski.vulnerable.xssexample;

import lombok.Value;
import java.util.UUID;

@Value
class BlogPost {
    private UUID id;
    private String title;
    private String content;
}
