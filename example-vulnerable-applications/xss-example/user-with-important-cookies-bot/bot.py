import requests
import time
from pyquery import PyQuery as pq
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

options = Options()
options.headless = True
web = webdriver.Firefox(options=options, executable_path="/geckodriver")

time.sleep(5)
d = pq(url="http://blog:8080")

buttons = d("a.btn")
urls = []
for button in buttons:
    urls.append(button.get('href'))

web.get("http://blog:8080/49965074-fd8f-45ef-8073-fb85cb613a52")

while True:
    for url in urls:
        web.get("http://blog:8080" + url)
        print(url)

        try:
            WebDriverWait(web, 1).until(EC.alert_is_present(),
                                           'Timed out waiting for PA creation ' +
                                           'confirmation popup to appear.')

            alert = web.switch_to.alert
            alert.accept()
            print("alert accepted")
        except TimeoutException:
            print("no alert")
    time.sleep(15)
