from app import db

class Image(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pathToImage = db.Column(db.String(64), index=True)
    imageName = db.Column(db.String(64), index=True, unique=True)

    def __repr__(self):
        return '<Image {}>'.format(self.imageName)    
