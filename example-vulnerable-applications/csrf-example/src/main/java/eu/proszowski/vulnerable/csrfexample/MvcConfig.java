package eu.proszowski.vulnerable.csrfexample;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/", "/home");
        registry.addViewController("/error").setViewName("error");
        registry.addStatusController("/error", HttpStatus.BAD_REQUEST);
        registry.addStatusController("/error", HttpStatus.NOT_FOUND);
        registry.addStatusController("/error", HttpStatus.INTERNAL_SERVER_ERROR);
        registry.addStatusController("/error", HttpStatus.BAD_GATEWAY);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/images/**")
                .addResourceLocations("classpath:/static/images/");
    }
}
