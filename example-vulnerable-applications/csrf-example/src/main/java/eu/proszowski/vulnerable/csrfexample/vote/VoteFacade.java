package eu.proszowski.vulnerable.csrfexample.vote;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import eu.proszowski.vulnerable.csrfexample.user.CurrentUserHolder;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static com.google.common.collect.Multimaps.synchronizedListMultimap;
import static eu.proszowski.vulnerable.csrfexample.meme.MemesRepository.SPECIAL_USERS;

@Component
public class VoteFacade {
    private final Multimap<String, UUID> votesForMemeGroupedByUserNames = synchronizedListMultimap(MultimapBuilder.linkedHashKeys().linkedListValues().build());
    private final Map<UUID, Integer> votesCountForEachMeme = new ConcurrentHashMap<>();

    public void vote(final String currentPrincipalName, final UUID id) {
        if(didUserVotedFor(id.toString())){
            return;
        }

        votesForMemeGroupedByUserNames.put(currentPrincipalName, id);
        votesCountForEachMeme.merge(id, 1, (prev, next) -> prev + next);
    }

    public boolean didUserVotedFor(String id){
        return votesForMemeGroupedByUserNames.get(CurrentUserHolder.getCurrentUser()).contains(UUID.fromString(id));
    }

    public boolean didAllSpecialUsersVotedForIt(String id){
        return SPECIAL_USERS.stream().allMatch(a -> votesForMemeGroupedByUserNames.get(a).contains(UUID.fromString(id)));
    }

    public int getPointsForId(final UUID id){
        return getPointsForId(id.toString());
    }

    public int getPointsForId(final String id){
        if(votesCountForEachMeme.containsKey(UUID.fromString(id))){
            return votesCountForEachMeme.get(UUID.fromString(id));
        }

        return 0;
    }

}
