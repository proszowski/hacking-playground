package eu.proszowski.vulnerable.csrfexample.user;

public class UserValidationException extends Exception {
    public UserValidationException(String message) {
        super(message);
    }
}
