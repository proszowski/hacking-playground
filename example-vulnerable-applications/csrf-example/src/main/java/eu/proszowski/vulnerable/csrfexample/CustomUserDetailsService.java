package eu.proszowski.vulnerable.csrfexample;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.ArrayList;
import java.util.Set;

public class CustomUserDetailsService implements UserDetailsService {

    private final UserDetailsManager userDetailsManager = new InMemoryUserDetailsManager(new ArrayList<>());
    private final Set<String> usernames = Sets.newHashSet();

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userDetailsManager.loadUserByUsername(username);
    }

    public void addUser(UserDetails userDetails){
        userDetailsManager.createUser(userDetails);
        usernames.add(userDetails.getUsername());
    }

    public boolean exists(String username) {
        return userDetailsManager.userExists(username);
    }

    public Set<String> getUsers() {
        return ImmutableSet.copyOf(usernames);
    }
}
