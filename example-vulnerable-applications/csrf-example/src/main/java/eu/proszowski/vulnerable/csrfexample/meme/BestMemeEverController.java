package eu.proszowski.vulnerable.csrfexample.meme;

import com.google.common.collect.Iterables;
import eu.proszowski.vulnerable.csrfexample.vote.VoteFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;
import java.util.stream.Collectors;

@Controller
public class BestMemeEverController {

    public static final String HIDDEN_FLAG = "Flag{YouKnowAwesomeMemesKeepGoing!}";

    @Autowired
    private MemesRepository memesRepository;

    @Autowired
    private VoteFacade voteFacade;

    @GetMapping("/best-meme-ever")
    ModelAndView bestMemeEver(Model model){
        final Map<UUID, Integer> memesGroupedByAmountOfPoints = memesRepository.getMemes()
                .stream()
                .collect(Collectors.toMap(Meme::getId, meme -> voteFacade.getPointsForId(meme.getId())));

        final List<UUID> bestMemeIds = memesGroupedByAmountOfPoints
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Comparator.comparingInt(Map.Entry::getValue)))
                .limit(1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        final UUID bestMemeId = Iterables.getOnlyElement(bestMemeIds, null);

        final List<Meme> bestMemes = memesRepository.getMemes()
                .stream()
                .filter(meme -> meme.getId().equals(bestMemeId))
                .collect(Collectors.toList());

        final Meme bestMeme = Iterables.getOnlyElement(bestMemes, Meme.DUMMY);
        model.addAttribute("meme", bestMeme);

        if(voteFacade.didAllSpecialUsersVotedForIt(bestMemeId.toString())){
            model.addAttribute("flag", HIDDEN_FLAG);
        }

        return new ModelAndView("best-meme-ever", model.asMap());
    }
}
