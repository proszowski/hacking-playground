package eu.proszowski.vulnerable.csrfexample.meme;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.*;

@Component
public class MemesRepository {
    private final Map<String, Meme> memes = new HashMap<>();

    public static final List<String> SPECIAL_USERS = Arrays.asList("XstaszekX", "wiola9", "andrzej112", "zbyszek16");

    @PostConstruct
    void initializeScenario(){
        Arrays.asList(
                Meme.builder()
                        .withName("Very funny meme")
                        .withPathToMeme("c16342ba-889a-4bd7-9f7b-1d826b776609")
                        .withAuthor("XstaszekX")
                        .build(),
                Meme.builder()
                        .withName("little spider...")
                        .withPathToMeme("d4d38581-4075-4d07-a1f6-755f8b0928cb")
                        .withAuthor("wiola9")
                        .build(),
                Meme.builder()
                        .withName("Thanksgiving day :)")
                        .withPathToMeme("5c850d35-9596-4377-8609-26431aad451e")
                        .withAuthor("andrzej112")
                        .build(),
                Meme.builder()
                        .withName("nice note!")
                        .withPathToMeme("63d52bc0-6f7e-4077-8632-c903c915f387")
                        .withAuthor("zbyszek16")
                        .build()
        ).forEach(this::addMeme);
    }

    public void addMeme(Meme meme) {
        if (memes.containsKey(meme.getName())) {
            throw new IllegalStateException("Meme with given name already exists.");
        }

        memes.put(meme.getName(), meme);
    }

    public Set<Meme> getMemes() {
        return new HashSet<>(memes.values());
    }
}
