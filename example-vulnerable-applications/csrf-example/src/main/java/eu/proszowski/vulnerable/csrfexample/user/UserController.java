package eu.proszowski.vulnerable.csrfexample.user;

import com.google.common.collect.*;
import eu.proszowski.vulnerable.csrfexample.CustomUserDetailsService;
import eu.proszowski.vulnerable.csrfexample.vote.VoteFacade;
import eu.proszowski.vulnerable.csrfexample.meme.Meme;
import eu.proszowski.vulnerable.csrfexample.meme.MemesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;
import java.util.stream.Collectors;

@Controller
@Slf4j
public class UserController {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private VoteFacade voteFacade;

    @Autowired
    private MemesRepository memesRepository;

    @GetMapping("login")
    String login(){
        if(CurrentUserHolder.isAnonymous()){
            return "login";
        }else {
            return "home";
        }
    }

    @GetMapping("users")
    ModelAndView users(Model model) {
        CustomUserDetailsService customUserDetailsService = (CustomUserDetailsService) userDetailsService;

        final Multimap<String, Meme> memesGroupedByAuthor = memesRepository.getMemes().stream()
                .collect(Multimaps.toMultimap(
                        meme -> meme.getAuthor(),
                        meme -> meme,
                        ArrayListMultimap::create
                ));

        List<Map.Entry<String, Integer>> usersWithPoints = customUserDetailsService.getUsers()
                .stream()
                .collect(Collectors.toMap(
                        u -> u,
                        u -> getPointsForUserName(u, memesGroupedByAuthor.get(u))
                )).entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Comparator.comparingInt(Map.Entry::getValue)))
                .collect(Collectors.toList());

        model.addAttribute("users", usersWithPoints);

        return new ModelAndView("users", model.asMap());
    }

    private Integer getPointsForUserName(String u, Collection<Meme> memes) {
        return memes.stream()
                .map(meme -> voteFacade.getPointsForId(meme.getId()))
                .reduce(0, (subtotal, element) -> subtotal + element);
    }

    @GetMapping("register")
    String register() {
        if(CurrentUserHolder.isAnonymous()){
            return "register";
        }else {
            return "home";
        }
    }

    @PostMapping("register")
    ModelAndView register(UserDto userDto, Model model) {

        try {
            validate(userDto);
        } catch (UserPasswordCannotBeEmptyOrBlank e) {
            model.addAttribute("error_message", e.getMessage());
            return new ModelAndView("register", model.asMap());
        } catch (UserNameCannotBeEmptyOrBlank e) {
            model.addAttribute("error_message", e.getMessage());
            return new ModelAndView("register", model.asMap());
        }

        UserDetails userDetails = User.withDefaultPasswordEncoder()
                .username(userDto.getUsername())
                .password(userDto.getPassword())
                .roles("USER")
                .build();

        CustomUserDetailsService userDetailsService = (CustomUserDetailsService) this.userDetailsService;

        if(userDetailsService.exists(userDto.getUsername())){
            model.addAttribute("user_exists", true);
            return new ModelAndView("register", model.asMap());
        }else {
            userDetailsService.addUser(userDetails);
            model.addAttribute("register_success", true);
            return new ModelAndView("login", model.asMap());
        }
    }

    private void validate(UserDto userDto) throws UserPasswordCannotBeEmptyOrBlank, UserNameCannotBeEmptyOrBlank {
        if(userDto.getUsername() != null && (userDto.getUsername().isEmpty() || userDto.getUsername().isBlank())){
            throw new UserNameCannotBeEmptyOrBlank();
        }

        if (userDto.getPassword() != null && (userDto.getPassword().isBlank() || userDto.getPassword().isEmpty())){
            throw new UserPasswordCannotBeEmptyOrBlank();
        }
    }

    private class UserNameCannotBeEmptyOrBlank extends UserValidationException{
        public UserNameCannotBeEmptyOrBlank() {
            super("User name cannot be empty or blank.");
        }
    }

    private class UserPasswordCannotBeEmptyOrBlank extends UserValidationException{
        public UserPasswordCannotBeEmptyOrBlank() {
            super("User password cannot be empty or blank.");
        }
    }

}
