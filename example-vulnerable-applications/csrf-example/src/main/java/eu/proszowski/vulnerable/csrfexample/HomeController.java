package eu.proszowski.vulnerable.csrfexample;

import eu.proszowski.vulnerable.csrfexample.meme.Meme;
import eu.proszowski.vulnerable.csrfexample.meme.MemesRepository;
import eu.proszowski.vulnerable.csrfexample.user.CurrentUserHolder;
import eu.proszowski.vulnerable.csrfexample.vote.VoteFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;

@Controller
public class HomeController {

    @Autowired
    private MemesRepository memesRepository;

    @Autowired
    private VoteFacade voteFacade;


    @GetMapping("home")
    public ModelAndView home(final Model model) {
        model.addAttribute("memes", memesRepository.getMemes());

        final boolean isUserLoggedIn = !CurrentUserHolder.isAnonymous();
        model.addAttribute("user_logged_in", isUserLoggedIn);
        return new ModelAndView("home", model.asMap());
    }
}
