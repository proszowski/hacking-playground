package eu.proszowski.vulnerable.csrfexample.meme;

import eu.proszowski.vulnerable.csrfexample.user.CurrentUserHolder;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Controller
public class MemeController {

    @Autowired
    private MemesRepository memesRepository;

    @GetMapping("/add-meme")
    private ModelAndView addMeme() {
        return new ModelAndView("add-meme");
    }

    @GetMapping("/image/{pathToImage}")
    private @ResponseBody  byte[] getImage(@PathVariable final UUID pathToImage) throws IOException {
        File file = new File(pathToImage.toString());
        return IOUtils.toByteArray(new FileInputStream(file));
    }

    @PostMapping("/add-meme")
    private ModelAndView addMeme(final String memeName,
                                 final MultipartFile memeFile,
                                 final Model model) throws IOException {

        final Path source = Paths.get("/");
        final UUID randomUUID = UUID.randomUUID();
        final Path desiredPath = Paths.get(source.toAbsolutePath().toString() + "/" + randomUUID.toString());

        memeFile.transferTo(desiredPath);

        final Meme meme = Meme.builder()
                .withName(memeName)
                .withPathToMeme(randomUUID.toString())
                .withAuthor(CurrentUserHolder.getCurrentUser())
                .build();

        try {
            memesRepository.addMeme(meme);
        } catch (Exception e) {
            model.addAttribute("errorMessage", e.getMessage());
            return new ModelAndView("add-meme", model.asMap());
        }

        model.addAttribute("success", true);

        return new ModelAndView("add-meme", model.asMap());
    }
}
