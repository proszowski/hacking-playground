package eu.proszowski.vulnerable.csrfexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CsrfExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(CsrfExampleApplication.class, args);
	}

}
