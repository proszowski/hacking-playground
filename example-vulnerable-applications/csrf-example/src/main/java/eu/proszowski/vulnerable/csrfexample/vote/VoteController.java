package eu.proszowski.vulnerable.csrfexample.vote;

import eu.proszowski.vulnerable.csrfexample.meme.Meme;
import eu.proszowski.vulnerable.csrfexample.user.CurrentUserHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.annotation.PostConstruct;
import java.util.UUID;

@RestController
@Slf4j
public class VoteController {

    @Autowired
    private VoteFacade voteFacade;

    @GetMapping("/vote/{id}")
    Object vote(@PathVariable final UUID id, final Model model) {

        if(CurrentUserHolder.isAnonymous()){
            model.addAttribute("vote_message", "You need to login in order to vote.");
            return new ModelAndView("login", model.asMap());
        }

        voteFacade.vote(CurrentUserHolder.getCurrentUser(), id);

        return new RedirectView("/home#" + id);
    }
}
