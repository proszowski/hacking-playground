package eu.proszowski.vulnerable.csrfexample.user;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class CurrentUserHolder {

    public static String getCurrentUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        return currentPrincipalName;
    }

    public static boolean isAnonymous() {
        return getCurrentUser().equals("anonymousUser");
    }
}
