package eu.proszowski.vulnerable.csrfexample.meme;

import lombok.NonNull;
import lombok.Value;

import java.time.Instant;
import java.util.UUID;

@Value
public class Meme {
    public static final Meme DUMMY = Meme.builder()
            .withName("DUMMY")
            .withPathToMeme("img_1.jpg")
            .withAuthor("DUMMY")
            .build();

    @NonNull
    private final UUID id = UUID.randomUUID();
    @NonNull
    private String name;
    @NonNull
    private String pathToMeme;
    @NonNull
    private String author;
    @NonNull
    private final Instant creationDate = Instant.now();

    private Meme(Builder builder) {
        name = builder.name;
        pathToMeme = builder.pathToMeme;
        author = builder.author;
    }

    public static IName builder() {
        return new Builder();
    }

    public interface IBuild {
        Meme build();
    }

    public interface IAuthor {
        IBuild withAuthor(String val);
    }

    public interface IPathToMeme {
        IAuthor withPathToMeme(String val);
    }

    public interface IName {
        IPathToMeme withName(String val);
    }

    public static final class Builder implements IAuthor, IPathToMeme, IName, IBuild {
        private @NonNull String author;
        private @NonNull String pathToMeme;
        private @NonNull String name;

        private Builder() {
        }

        @Override
        public IBuild withAuthor(@NonNull String val) {
            author = val;
            return this;
        }

        @Override
        public IAuthor withPathToMeme(@NonNull String val) {
            pathToMeme = val;
            return this;
        }

        @Override
        public IPathToMeme withName(@NonNull String val) {
            name = val;
            return this;
        }

        public Meme build() {
            return new Meme(this);
        }
    }
}
