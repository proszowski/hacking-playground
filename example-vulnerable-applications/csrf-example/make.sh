#!/bin/bash

./gradlew bootJar
docker build -t powder13/the-hacking-playground:csrf-example .
docker build -t powder13/the-hacking-playground:voting-users ./voting-users
docker-compose up
