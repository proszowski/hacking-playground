import os
from app import app
from flask import render_template, request, send_file

@app.route('/')
def index():
    return render_template('index.html', title='Home')

@app.route('/cat-image')
def cat_image():
    image_name = request.args.get('image_name')
    full_image_name = "app/static/images/" + image_name;
    return send_file(os.path.join(os.getcwd(), full_image_name))
