const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser');
const httpClient = require('request');
const cheerio = require('cheerio');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');

app.get('/', function(request, response)
{
	response.render('success-page');
});

app.post('/login', function(request, response)
{
	const login = request.body['login'];
	const password = request.body['password'];

	if(login === "admin" && password === "admin"){
		response.render('success-page');
	}else{
		response.render('index', {errorMessage:"Incorrect credentials"});
	}

});

var server = app.listen(3001, function() {});
