const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser');
const httpClient = require('request');
const cheerio = require('cheerio');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');

app.get('/', function(request, response)
{
	response.render('index', {errorMessage:undefined});
});

app.post('/scrap', function(request, response)
{
	const options = {};
	const link = request.body['pageLink'];
	if(link.includes('localhost') || link.includes('127.0.0.1')){
		response.render('index', {errorMessage:'Given page isn\'t supported'});
		return;
	}
	httpClient.get(link, options, function(err, res, body){

	  if(err){
			response.render('index', {errorMessage:'Couldn\'t process request'});
			return;
		}

	  if(res.statusCode === 200 ){
			const $ = cheerio.load(body, { decodeEntities: false });
			if(!$('.interpretation-content').html()){
				response.render('original', {errorMessage:'Given page isn\'t supported', original:body});
				return;
			}

			const title = $('h1 strong').text()

			const countOfChordsPerLine = $('.interpretation-content').html().split(/\n/).map((item, i) => {
				count = (item.match(/data-chord/g) || []).length
				return count
			});

			const chords = $('code')
				.remove()
				.map((i, item) => {
					return item.attribs['data-chord'] + item.attribs['data-suffix']
				});

			const scrappedText = $('.interpretation-content').html().split(/\n\n/);
			const scrappedTextWithBreaks = scrappedText.map(function(i){
				return i.split(/\n/);
			});

			var last = 0;
			var lastWasZero = false;
			var linesAfterWhichIsLongerPause = [];
			var lastIndexOfTakenChordLine;

			const chordsPerLine = countOfChordsPerLine
			.filter((item, index) => {
				if(lastWasZero && item == 0){
					linesAfterWhichIsLongerPause.push(lastIndexOfTakenChordLine);
				}
				if(item != 0){
					lastWasZero = false;
					lastIndexOfTakenChordLine = index;
					return true;
				}else{
					lastWasZero = true;
					return false;
				}
			})
			.map((howManyChords, index) => {
				 return chords.splice(last, howManyChords).join(' ')
			});

			response.render('scrapped', {scrappedText:scrappedTextWithBreaks, title:title, chordsPerLine:chordsPerLine, breaks:linesAfterWhichIsLongerPause});
		}
	});
});

var server = app.listen(3000, function() {});
