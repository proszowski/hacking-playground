const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser');
const httpClient = require('request');
const cheerio = require('cheerio');

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');

app.get('/', function(request, response)
{
	response.render('index');
});

var server = app.listen(3002, function() {});
