const express = require('express');
const ejs = require('ejs');
const bodyParser = require('body-parser');
const httpClient = require('request');
const cheerio = require('cheerio');

var app = express();

app.get('/', function(request, response)
{
	response.send(`
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8">
        <meta name="robots" content="index,follow">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="aEnhFH883ITE5Dw8UgcYSmeiWrBfjeIay3RJ3vrX">
    <title>Nie mam nic, co bym mógł Tobie dać - Piosenki Religijne, Tekst piosenki i chwyty na gitarę</title>
<meta name="description" content=" D h e A 1. Nie mam nic co bym mógł Tobie dać, e A7 D nie mam sił, by przed Tobą Panie stać. D D7 G g Ref. / Puste ręce przynoszę przed Twój w niebie tron, D e A D manną z nieba nakarm duszę mą. /x2 2...">
<meta name="keywords" content="Piosenki Religijne, chwyty, chwyty na gitarę, ukulele, akordy, akordy gitarowe, tabulatura, gitara, nuty, tekst">
<link rel="canonical" href="https://spiewnik.wywrota.pl/piosenki-religijne/nie-mam-nic-co-bym-mogl-tobie-dac/chwyty-tekst"/>
<meta property="og:title" content="Nie mam nic, co bym mógł Tobie dać - Piosenki Religijne, Tekst piosenki i chwyty na gitarę" />
<meta property="og:description" content=" D h e A 1. Nie mam nic co bym mógł Tobie dać, e A7 D nie mam sił, by przed Tobą Panie stać. D D7 G g Ref. / Puste ręce przynoszę przed Twój w niebie tron, D e A D manną z nieba nakarm duszę mą. /x2 2..." />
<meta property="og:type" content="article" />
<meta property="og:image" content="https://moja.wywrota.pl/storage/media/14556/eeb12f63_jpg-lg" />

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@WywrotaPL" />
<meta name="twitter:title" content="Nie mam nic, co bym mógł Tobie dać - Piosenki Religijne, Tekst piosenki i chwyty na gitarę" />
<meta name="twitter:description" content=" D h e A 1. Nie mam nic co bym mógł Tobie dać, e A7 D nie mam sił, by przed Tobą Panie stać. D D7 G g Ref. / Puste ręce przynoszę przed Twój w niebie tron, D e A D manną z nieba nakarm duszę mą. /x2 2..." />
<script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"WebPage","name":"Nie mam nic, co bym m\u00f3g\u0142 Tobie da\u0107 - Piosenki Religijne, Tekst piosenki i chwyty na gitar\u0119","description":" D h e A 1. Nie mam nic co bym m\u00f3g\u0142 Tobie da\u0107, e A7 D nie mam si\u0142, by przed Tob\u0105 Panie sta\u0107. D D7 G g Ref. \/ Puste r\u0119ce przynosz\u0119 przed Tw\u00f3j w niebie tron, D e A D mann\u0105 z nieba nakarm dusz\u0119 m\u0105. \/x2 2..."}</script>
            <script type="application/ld+json">{"@context":"https:\/\/schema.org","@type":"MusicRecording","name":"Nie mam nic, co bym mógł Tobie dać","url":"https:\/\/spiewnik.wywrota.pl\/piosenki-religijne\/nie-mam-nic-co-bym-mogl-tobie-dac\/chwyty-tekst","datePublished":"2019-06-24T23:35:05+02:00","aggregateRating":{"@type":"AggregateRating","ratingCount":1,"ratingValue":"5.0000","reviewCount":0},"byArtist":{"@type":"MusicGroup","name":"Piosenki Religijne","url":"https:\/\/spiewnik.wywrota.pl\/piosenki-religijne"},"image":["https:\/\/moja.wywrota.pl\/storage\/media\/14556\/eeb12f63_jpg-lg"]}</script>
        <link rel="apple-touch-icon" sizes="180x180" href="https://spiewnik.wywrota.pl/assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://spiewnik.wywrota.pl/assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://spiewnik.wywrota.pl/assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="https://spiewnik.wywrota.pl/assets/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="https://spiewnik.wywrota.pl/assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="https://spiewnik.wywrota.pl/assets/img/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#212121">
    <meta name="msapplication-config" content="https://spiewnik.wywrota.pl/assets/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#212121">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link rel="stylesheet" href="https://spiewnik.wywrota.pl/songbook/css/songbook-app.css?id=85e712cede2979b78aa2"/>
    <link rel="stylesheet" href="https://spiewnik.wywrota.pl/songbook/css/songbook.css?id=6af3e994b418ee8b501a"/>
    <link rel="preload" href="https://spiewnik.wywrota.pl/fonts/vendor/font-awesome/fontawesome-webfont.woff2?af7ae505a9eed503f8b8e6982036873e" as="font" type="font/woff2" crossorigin>

            <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-1548426-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-1548426-1');
    </script>

        <script type="text/javascript">
            window._taboola = window._taboola || [];
            _taboola.push({article:'auto'});
            !function (e, f, u, i) {
                if (!document.getElementById(i)){
                    e.async = 1;
                    e.src = u;
                    e.id = i;
                    f.parentNode.insertBefore(e, f);
                }
            }(document.createElement('script'),
                document.getElementsByTagName('script')[0],
                '//cdn.taboola.com/libtrc/wywrota-spiewnikwywrota/loader.js',
                'tb_loader_script');
            if(window.performance && typeof window.performance.mark == 'function')
            {window.performance.mark('tbl_ic');}
        </script>

    

    <script>
        window.WYWROTA = {
            "user_logged_in" :   0 ,
            "lang_locale" : 'pl',
            "csrf" : "aEnhFH883ITE5Dw8UgcYSmeiWrBfjeIay3RJ3vrX",
                    };
    </script>
    <script>
    gtag('event', 'ad', {
        'event_category': 'anchor-vignete',
        'event_label': 'adrino', 
        'nonInteraction': true
    });
</script>
<script type="text/javascript" src="https://lib.wtg-ads.com/lib.min.js" async></script>


    
</head>


<body>

<nav class="navbar navbar-dark bg-dark p-1">
    <div class="container d-flex flex-row">

        <span class="navbar-toggler-icon d-md-none" data-toggle="offcanvas"></span>
        <div class="navbar-brand d-md-none" data-toggle="offcanvas">
            <span>Śpiewnik</span>
        </div>

                    <a class="navbar-brand ml-md-2 d-none d-md-block" href="#" data-toggle="collapse" data-target="#toggleMegaMenu"
               aria-controls="toggleMegaMenu" aria-expanded="false" aria-label="Toggle navigation">
                <img src="https://spiewnik.wywrota.pl/assets/img/wywrota-logo-truck.svg" alt="Śpiewnik Wywroty - chwyty na gitarę i teksty piosenek" class="d-none d-sm-block" />
                <span class="dropdown-toggle">Śpiewnik</span>
            </a>
        
        <div class="ml-auto d-block d-md-none">
            <i class="fa fa-search text-white mr-2" style="font-size: 140%" data-toggle="search"></i>
        </div>

        <form class="form-inline d-none d-md-block pb-2 pb-md-0"  id="main-search-form" action="https://spiewnik.wywrota.pl/search">
            <input type="text" class="form-control main-search-field" id="main-search-field" name="q" value=""
                   placeholder="Co chcesz dzisiaj zagrać?"/>
            <button type="submit" class="btn btn-primary ml-1">Szukaj</button>
        </form>

        <div class="ml-auto d-none d-md-block">

            

                            <div class="login-register">
                    <a href="https://moja.wywrota.pl/register">Zarejestruj się!</a>
                </div>
                    </div>

    </div>

</nav>




<div class=" collapse  bg-dark" id="toggleMegaMenu">
    <div class="container py-2">
        <div class="row">
            <div class="col-6 col-lg-3 pl-3 pl-md-5 pb-3">
                <ul class="list-unstyled">
                                        <li><a href="https://spiewnik.wywrota.pl">Strona Główna</a></li>
                                        <li><a href="https://spiewnik.wywrota.pl/top/popular">Lista Przebojów</a></li>
                    <li><a href="https://spiewnik.wywrota.pl/country">Polscy wykonawcy</a></li>
                    <li><a href="https://spiewnik.wywrota.pl/country/US/artists">Zagraniczni wykonawcy</a></li>
                    <li><a href="https://spiewnik.wywrota.pl/hall-of-fame">Śpiewnikowa Gwardia</a></li>
                    <li><a href="https://spiewnik.wywrota.pl/wskazowki">Zasady Kontrybucji</a></li>
                    <li><a href="https://spiewnik.wywrota.pl/decisions">
                            Poczekalnia                                                            <span class="badge badge-primary align-text-top">19</span>
                                                    </a>
                    </li>
                </ul>


            </div>
            <div class="col-6 d-none d-lg-block">

                <div class="text-muted">
                    <a href="https://spiewnik.wywrota.pl/piosenki-harcerskie" class="text-white">Piosenki Harcerskie</a> &middot;
                    <a href="https://spiewnik.wywrota.pl/piosenki-religijne" class="text-white">Piosenki Religijne</a> &middot;
                    <a href="https://spiewnik.wywrota.pl/piosenki-turystyczne" class="text-white">Piosenki Turystyczne</a> &middot;
                    <a href="https://spiewnik.wywrota.pl/piosenki-ludowe" class="text-white">Piosenki Ludowe</a> &middot;
                    <a href="https://spiewnik.wywrota.pl/piesni-patriotyczne" class="text-white">Pieśni Patriotyczne</a> &middot;
                    <a href="https://spiewnik.wywrota.pl/piosenki-dla-dzieci" class="text-white">Piosenki dla dzieci</a> &middot;
                    <a href="https://spiewnik.wywrota.pl/szanty" class="text-white">Szanty</a> &middot;
                    <a href="https://spiewnik.wywrota.pl/biesiadne" class="text-white">Biesiadne</a> &middot;
                    <a href="https://spiewnik.wywrota.pl/koledy" class="text-white">Kolędy</a>
                </div>


            </div>
            <div class="col-6 col-lg-3">

                <ul class="list-unstyled text-muted">
                    <li><a href="https://spiewnik.wywrota.pl/chwyty-gitarowe">Chwyty gitarowe</a></li>
                    <li><a href="https://spiewnik.wywrota.pl/blog/jak-czytac-chwyty"> Jak czytać chwyty?</a></li>
                    <li><a href="https://spiewnik.wywrota.pl/blog/wybrane-bicia-gitarowe"> Wybrane bicia gitarowe</a></li>
                    <li><a href="https://spiewnik.wywrota.pl/blog/chwyty-barrowe">Chwyty barowe</a></li>
                    <li><a href="https://spiewnik.wywrota.pl/blog/czemu-gitara-brzeczy">Czemu gitara brzęczy?</a></li>
                    <li><a href="https://spiewnik.wywrota.pl/blog/tonacje-transponowanie">Tonacje i transponowanie</a></li>

                </ul>

            </div>
        </div>

    </div>
</div>
<div class="navbar-collapse navmenu-mobile bg-dark">

  <ul class="navbar-nav mr-auto">
    <li class="bg-light py-3  pl-4  d-flex mb-3">

                             <div style="width: 80px;" class="position-relative  d-inline-block">

	
		<img src="https://spiewnik.wywrota.pl/assets/img/profile.png"
			 width="80"
			 height="80"
			 alt="anonim"
			 class="rounded-circle border">

	
</div>             <div class="my-auto ">
                <a style="font-size: 120%; color: black; font-weight: bold" class="px-3" href="https://moja.wywrota.pl/register">Zarejestruj się!</a>
             </div>
        
      <span class="font-weight-bold ml-auto px-3" data-toggle="offcanvas" style="font-size: 200%;">&times;</span>

    </li>

      
      <li><a class="dropdown-item" href="https://spiewnik.wywrota.pl">Strona Główna</a></li>
      <li><a class="dropdown-item" href="https://spiewnik.wywrota.pl/top/popular">Lista Przebojów</a></li>
      <li><a class="dropdown-item" href="https://spiewnik.wywrota.pl/country">Polscy wykonawcy</a></li>
      <li><a class="dropdown-item" href="https://spiewnik.wywrota.pl/country/US/artists">Zagraniczni wykonawcy</a></li>
      <li><a class="dropdown-item" href="https://spiewnik.wywrota.pl/hall-of-fame">Śpiewnikowa Gwardia</a></li>
      <li><a class="dropdown-item" href="https://spiewnik.wywrota.pl/wskazowki">Zasady Kontrybucji</a></li>
      <li><a class="dropdown-item" href="https://spiewnik.wywrota.pl/decisions">
              Poczekalnia                                <span class="badge badge-primary pull-right mr-2">19</span>
                        </a>
      </li>
      <li><a class="dropdown-item" href="https://spiewnik.wywrota.pl/chwyty-gitarowe">Chwyty gitarowe</a></li>

      <li><hr style="border-top: solid #333 1px; width: 90%;    margin: 0.5em auto;"></li>
      <li><a class="dropdown-item" href="https://spiewnik.wywrota.pl/submit">
          <i class="fa fa-plus-square text-muted mr-2"></i>
          Prześlij Opracowanie      </a></li>

      
  </ul>
</div>


<section class="content">
    <div class="container">
        
                    </div>

    

    <!-- ************************************************
         *   Song id: 118824
         *   Interpretation id: 117568
         *   Revision id: 106334
         *   Artist id: 1086
         ************************************************ -->


    <div class="container">

        <!-- /52555387/wywrota.pl_970x250_2 -->
<div class="my-2 d-print-none">
    <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
    <script>
        gtag('event', 'ad', {
            'event_category': 'billboard',
            'event_label': 'yieldbird', 
            'nonInteraction': true
        });

        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
    </script>
    <script>
        // GPT slots
        var gptAdSlots = [];
        googletag.cmd.push(function() {
            var _YB = _YB || {
                ab: function() {
                    return (_YB.dool ? 'b' : 'a' + Math.floor(Math.random() * 10));
                },
                dc: function() {
                    return (_YB.dool ? 'd' : 'c' + Math.floor(Math.random() * 20));
                },
                mx: function() {
                    return (!_YB.dool ? 'x' : 'm' + Math.floor(Math.random() * 180));
                },
                tt: function() {
                    return ('tt' + Math.floor(Math.random() * 10));
                },
                dool: Math.random() >= 0.1
            };
            var _yt = new Date(),
                yb_th = _yt.getUTCHours() - 8,
                yb_tm = _yt.getUTCMinutes(),
                yb_wd = _yt.getUTCDay();
            if (yb_th < 0) {
                yb_th = 24 + yb_th;
                yb_wd -= 1;
            };
            if (yb_wd < 0) {
                yb_wd = 7 + yb_wd
            };

            // Define a size mapping object. The first parameter to addSize is
            // a viewport size, while the second is a list of allowed ad sizes.
            var mapping = googletag.sizeMapping().

            // viewportX < 760
            addSize([0, 0], [300, 250]).

            // viewportX >= 760 && viewportX < 980
            addSize([760, 0], [750, 200]).

            // viewportX >= 980
            addSize([980, 0], [970, 250]).build();

            gptAdSlots[0] = googletag.defineSlot('/52555387/wywrota.pl_970x250_2', [[970, 250],[300, 250],[750, 200]], 'div-gpt-ad-1513766266790-0').setTargeting('yb_ab', _YB.ab()).setTargeting('yb_dc', _YB.dc()).setTargeting('yb_mx', _YB.mx()).setTargeting('yb_tt', _YB.tt()).setTargeting('yb_ff', String(Math.round(Math.random()))).setTargeting('yb_th', yb_th.toString()).setTargeting('yb_tm', yb_tm.toString()).setTargeting('yb_wd', yb_wd.toString()).defineSizeMapping(mapping).addService(googletag.pubads());
            googletag.pubads().collapseEmptyDivs();
            googletag.enableServices();
        });
    </script>
</div>
<div id='div-gpt-ad-1513766266790-0' class="d-print-none">
    <script>
        googletag.cmd.push(function() {
            googletag.display('div-gpt-ad-1513766266790-0');
        });
    </script>
</div>


    

        <div class="row">
            <div class="col-lg-9 ">

                <div class="grid-content p-sm-3">

                    <!-- lyrics actions -->
<div class="lyrics-actions py-2 d-flex d-print-none sticky-top bg-white" style="margin-top: -10px;">

    <div>

		<form class="form-inline favorite-button"
			   action="https://moja.wywrota.pl/login"			  			  data-favorite-id="117568"
			  data-favorite-type="7"
			  data-label-like="Do Ulubionych"
			  data-label-unlike="Usuń z Ulubionych"
			  data-is-favorite=""
		>
			<div class="btn-group">
				<button type="button" class="btn btn-sm btn-with-gradient js-toggler-target" aria-label="Do Ulubionych" title="Nie mam nic, co bym mógł Tobie dać - Piosenki Religijne"
						>
					<i class="fa fa-heart text-primary ml-1"></i>
					<span>
													Do Ulubionych											</span>
				</button>

				<a href="https://spiewnik.wywrota.pl/favorites/type/7/id/117568"
				   class="social-count btn" rel="nofollow" aria-label="0 osób ma to opracowanie w ulubionych">0</a>
			</div>
		</form>

	</div>


    <div class="ml-auto my-auto">

        
        <button class="btn btn-sm mr-1 btn-with-gradient" id="auroscroll-button" title="Automatyczne przewijanie" data-toggle="tooltip"><i class="fa fa-angle-double-down fa-lg font-weight-bold" ></i></button>


        
        <div class="btn-group" data-toggle="buttons">
            <button id="transpose-down-button" class="btn btn-sm btn-with-gradient" title="Transponuj pół tonu w dół" data-toggle="tooltip"><i class="fa fa-arrow-down"></i></button>
            <span class="text-center mr-1 d-none" id="transpose-state" style="width:1em">0</span>
            <button id="transpose-up-button" class="btn btn-sm mr-1 btn-with-gradient" title="Transponuj pół tonu w górę" data-toggle="tooltip"><i class="fa fa-arrow-up"></i></button>
        </div>


    </div>

</div>
                    
                    
                    
                    <div class="pull-right d-print-none">
        <a href="https://spiewnik.wywrota.pl/revision/106334/correction" class="ml-3 my-auto h3" rel="nofollow" title="Popraw Opracowanie" data-toggle="tooltip">
            <i class="fa fa-pencil"></i>
        </a>
    </div>


                    <h1>

                                                    <strong>Nie mam nic, co bym mógł Tobie dać</strong>
                            
                        

                        Piosenki Religijne
                    </h1>

                    <div class="text-muted mb-3 d-print-none">
                        <h2 class="text-muted lead d-inline">
                        Tekst piosenki i chwyty na gitarę
                        </h2>


                        
                    </div>

                    <div class="collapse" id="all-interpretations">
    <div class="bg-light p-1 p-md-3 mb-3 mr-md-5">
        <ul class="list-unstyled">
                            <li class="d-flex px-1">
                    <div class="my-auto text-nowrap pr-2 txt-sm"
             title="Doskonały   1 ocena" data-toggle="tooltip"
    >
                        <i class="fa fa-star text-primary" style="margin-right: -3px;"></i>
                                <i class="fa fa-star text-primary" style="margin-right: -3px;"></i>
                                <i class="fa fa-star text-primary" style="margin-right: -3px;"></i>
                                <i class="fa fa-star text-primary" style="margin-right: -3px;"></i>
                                <i class="fa fa-star text-primary" style="margin-right: -3px;"></i>
            </div>

                                                                <span class="text-muted">
                            Chwyty na gitarę
                        </span>
                                            
                                            <span class="pl-1 text-muted d-none d-md-block">jesienin </span>
                    

                    <div class="ml-auto my-auto pr-2 txt-sm text-muted ">
                                                                                    1 ocena
                                                    
                            
                            
                    </div>

                </li>
                    </ul>
    </div>
</div>


                    <pre class="interpretation-content">            <code  data-chord='D' data-suffix=''>D</code>          <code  data-chord='B' data-suffix='m'>h</code>          <code  data-chord='E' data-suffix='m'>e</code>    <code  data-chord='A' data-suffix=''>A</code>
1. Nie mam nic co bym mógł Tobie dać,
         <code  data-chord='E' data-suffix='m'>e</code>             <code  data-chord='A' data-suffix='7'>A7</code>          <code  data-chord='D' data-suffix=''>D</code>
nie mam sił, by przed Tobą Panie stać.

              <code  data-chord='D' data-suffix=''>D</code>       <code  data-chord='D' data-suffix='7'>D7</code>          <code  data-chord='G' data-suffix=''>G</code>              <code  data-chord='G' data-suffix='m'>g</code>
<span class='text-muted'>Ref.</span> / Puste ręce przynoszę przed Twój w niebie tron,
        <code  data-chord='D' data-suffix=''>D</code>      <code  data-chord='E' data-suffix='m'>e</code>      <code  data-chord='A' data-suffix=''>A</code>     <code  data-chord='D' data-suffix=''>D</code>
manną z nieba nakarm duszę mą.<span class='text-muted'> /x2

</span>
2. Pomaż maścią leczącą oczy me,
spraw by język mój też przemówić chciał.

<span class='text-muted'>Ref.</span>

3. Tak niegodny Twej łaski chylę skroń
i znów prosić przychodzę Panie mój.

<span class='text-muted'>Ref.</span>

4. W jaki sposób dziękować Tobie mam,
że mi ufasz i strzeżesz w każdy czas.

<span class='text-muted'>Ref.</span>

5. Przyjmij, Panie, dziękczynnej pieśni śpiew
za Twą miłość i dobroć w każdy dzień.

<span class='text-muted'>Ref.</span>

6. Tobie chcę ofiarować życie me.
Moje serce w ofierze Jezu weź.

</pre>

                    <div class="chord-boxes d-flex  flex-wrap"> <div class='chord-box-wrap' data-chord='D' data-suffix='' data-label='D' data-render='chord'></div><div class='chord-box-wrap' data-chord='B' data-suffix='m' data-label='h' data-render='chord'></div><div class='chord-box-wrap' data-chord='E' data-suffix='m' data-label='e' data-render='chord'></div><div class='chord-box-wrap' data-chord='A' data-suffix='' data-label='A' data-render='chord'></div><div class='chord-box-wrap' data-chord='A' data-suffix='7' data-label='A7' data-render='chord'></div><div class='chord-box-wrap' data-chord='D' data-suffix='7' data-label='D7' data-render='chord'></div><div class='chord-box-wrap' data-chord='G' data-suffix='' data-label='G' data-render='chord'></div><div class='chord-box-wrap' data-chord='G' data-suffix='m' data-label='g' data-render='chord'></div></div>

                    <!-- lyrics actions -->
<div class="lyrics-actions py-2 d-flex d-print-none ">


            <a href="https://spiewnik.wywrota.pl/revision/106334/correction" class=" btn btn-outline-primary" rel="nofollow">
            <i class="fa fa-pencil"></i>
            <span>Popraw Opracowanie</span>
        </a>
    

    


</div>

                    <div class="pt-3 d-print-none ">

                                                    <div id="rateYo"></div>
                            <div class="form-text text-muted">Oceń to opracowanie</div>
                                                <div class="text-muted">
                                                            Ocena czytelników:
                                <strong>Doskonały</strong>
                                1 głos
                            
                        </div>
                        <div id="ratingHelpBlock" class="form-text text-muted"></div>
                        <div id="ratingErrorBlock" class="form-text text-danger"></div>

                        <div class="song-metadata pt-2 text-muted">
                                                                                                                    Strojenie:     klasyczne (E A D G H e) <br/>
                                                                                                                                        </div>

                    </div>




                    <footer class="chord-modals" style="position: relative;"></footer>


                    

                </div>



                <div class="grid-content d-print-none bg-light py-3 px-3 border-top contributors">

        <div class="align-items-center dropup text-muted ">

            <div class="pr-1 d-inline-block">
                Kontrybucje:
            </div>

            
                                    <a href="https://spiewnik.wywrota.pl/user/jesienin" title="jesienin " class="mr-2 text-muted text-nowrap">
                        <div style="width: 32px;" class="position-relative  d-inline-block">

	
		<img
						 src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
			 data-src="https://moja.wywrota.pl/storage/media/15992/c/cFeNqN9NW7TCtfrA-px.jpg"
						 width="32"
			 height="32"
			 alt="jesienin "
			 title="jesienin "
			  data-toggle="tooltip" 			 class="rounded-circle border">

									<div class="badge-songbook-level-7 position-absolute text-center"
					 style="top: 20px; height: 16px;">
				</div>
					
	
</div>                        jesienin 
                    </a>
                

            
                    </div>
    </div>

            </div>


            <div class="col-lg-3 d-print-none">

                <div class="d-flex mb-3 mt-5 position-relative">
                    <span>
                         <img class="rounded-circle shadow mr-2" src="https://moja.wywrota.pl/storage/media/14556/c/eeb12f63_jpg-lg-sq.jpg" style="width: 90px; height: 90px" alt="Piosenki Religijne">
                    </span>
                    <h4 class="my-auto d-inline"><a href="https://spiewnik.wywrota.pl/piosenki-religijne" class="stretched-link">Piosenki Religijne</a></h4>
                </div>

                                                    
                
                <!-- video clips -->
                
                <div class="my-3">
                <a href="https://moja.wywrota.pl/login" rel="nofollow" title="Zaloguj się aby dodać nagranie" data-toggle="tooltip">
    
        <i class="fa fa-plus"></i>
        <span class="text-dark">Dodaj Klip Video</span>
    </a>
</div>


                
                <h3>Dodaj własne wykonanie</h3>
                Umiesz zagrać tą piosenkę? Nagraj swoje wykonanie i pokaż innym użytkownikom jak grasz!

                
            </div>
        </div>


        <div class="row d-print-none mt-2">
            <div class="col-lg-8">

                <div class="p-3">
                    <div class="add-comment-box d-flex flex-row ">

    
    
        <div class="pr-2">
            <div style="width: 40px;" class="position-relative  d-inline-block">

	
		<img src="https://spiewnik.wywrota.pl/assets/img/profile.png"
			 width="40"
			 height="40"
			 alt="anonim"
			 class="rounded-circle border">

	
</div>        </div>
        <form id="comment-form" class="w-75" action="#">
            <div class="form-group">
                <textarea class="form-control comment-box" name="comment" id="comment" required="required"
                          placeholder="Zaloguj się aby skomentować to opracowanie"></textarea>
            </div>
        </form>


        <script>
            window.addEventListener('DOMContentLoaded', function () {
                $('#comment').focus(function () {
                    window.location.href = "https://moja.wywrota.pl/login?to=https%3A%2F%2Fspiewnik.wywrota.pl%2Fpiosenki-religijne%2Fnie-mam-nic-co-bym-mogl-tobie-dac%2Fchwyty-tekst";
                });
            });
        </script>


    </div>
                </div>

                <div id="taboola-below-article-thumbnails" class="d-print-none"></div>
        <script type="text/javascript">
            window._taboola = window._taboola || [];
            _taboola.push({
                mode: 'thumbnails-a',
                container: 'taboola-below-article-thumbnails',
                placement: 'Below Article Thumbnails',
                target_type: 'mix'
            });
        </script>
        <script>
            gtag('event', 'ad', {
                'event_category': 'suggested-content',
                'event_label': 'taboola', 
                'nonInteraction': true
            });
        </script>

    

                
            </div>
            <div class="col-lg-4">
                

                <div class="my-2 d-print-none">
        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Śpiewnik - pod treścią -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-5039961815119955"
             data-ad-slot="1317344152"
             data-ad-format="auto"
             data-full-width-responsive="true"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
    <script>
        gtag('event', 'ad', {
            'event_category': 'fluid',
            'event_label': 'adsense', 
            'nonInteraction': true
        });

    </script>

            </div>
        </div>

    </div>


        <div class="js-cookie-consent container">

<div class="  alert alert-warning mt-3 " role="alert">

    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="40">
        <path d="M 16 4 C 9.373 4 4 9.373 4 16 C 4 22.627 9.373 28 16 28 C 22.627 28 28 22.627 28 16 C 28 15.514 27.96325 15.037406 27.90625 14.566406 C 27.50625 14.839406 27.022 15 26.5 15 C 25.422 15 24.511156 14.313422 24.160156 13.357422 C 23.536156 13.759422 22.798 14 22 14 C 19.791 14 18 12.209 18 10 C 18 9.265 18.212594 8.5861406 18.558594 7.9941406 C 18.538594 7.9951406 18.52 8 18.5 8 C 17.119 8 16 6.881 16 5.5 C 16 4.943 16.188094 4.4345313 16.496094 4.0195312 C 16.331094 4.0125312 16.167 4 16 4 z M 23.5 4 A 1.5 1.5 0 0 0 22 5.5 A 1.5 1.5 0 0 0 23.5 7 A 1.5 1.5 0 0 0 25 5.5 A 1.5 1.5 0 0 0 23.5 4 z M 14.052734 6.1894531 C 14.251734 7.4764531 14.998875 8.5864844 16.046875 9.2714844 C 16.015875 9.5134844 16 9.757 16 10 C 16 13.308 18.692 16 22 16 C 22.498 16 22.991703 15.936453 23.470703 15.814453 C 24.152703 16.438453 25.017125 16.850797 25.953125 16.966797 C 25.465125 22.029797 21.188 26 16 26 C 10.486 26 6 21.514 6 16 C 6 11.152 9.4677344 7.0984531 14.052734 6.1894531 z M 22 9 A 1 1 0 0 0 21 10 A 1 1 0 0 0 22 11 A 1 1 0 0 0 23 10 A 1 1 0 0 0 22 9 z M 14 10 A 1 1 0 0 0 13 11 A 1 1 0 0 0 14 12 A 1 1 0 0 0 15 11 A 1 1 0 0 0 14 10 z M 27 10 A 1 1 0 0 0 26 11 A 1 1 0 0 0 27 12 A 1 1 0 0 0 28 11 A 1 1 0 0 0 27 10 z M 11 13 A 2 2 0 0 0 9 15 A 2 2 0 0 0 11 17 A 2 2 0 0 0 13 15 A 2 2 0 0 0 11 13 z M 16 15 A 1 1 0 0 0 15 16 A 1 1 0 0 0 16 17 A 1 1 0 0 0 17 16 A 1 1 0 0 0 16 15 z M 12.5 19 A 1.5 1.5 0 0 0 11 20.5 A 1.5 1.5 0 0 0 12.5 22 A 1.5 1.5 0 0 0 14 20.5 A 1.5 1.5 0 0 0 12.5 19 z M 19.5 20 A 1.5 1.5 0 0 0 18 21.5 A 1.5 1.5 0 0 0 19.5 23 A 1.5 1.5 0 0 0 21 21.5 A 1.5 1.5 0 0 0 19.5 20 z"/>
    </svg>
        <span class="cookie-consent__message">
            Twoje doświadczenia w śpiewniku będą lepsze dzięki ciasteczkom.
        </span>

        <button class="js-cookie-consent-agree cookie-consent__agree btn btn-sm btn-primary">
            Zezwalaj na ciasteczka
        </button>

</div>
</div>
    <script>

        window.laravelCookieConsent = (function () {

            const COOKIE_VALUE = 1;
            const COOKIE_DOMAIN = '.wywrota.pl';

            function consentWithCookies() {
                setCookie('laravel_cookie_consent', COOKIE_VALUE, 7300);
                hideCookieDialog();
            }

            function cookieExists(name) {
                return (document.cookie.split('; ').indexOf(name + '=' + COOKIE_VALUE) !== -1);
            }

            function hideCookieDialog() {
                const dialogs = document.getElementsByClassName('js-cookie-consent');

                for (let i = 0; i < dialogs.length; ++i) {
                    dialogs[i].style.display = 'none';
                }
            }

            function setCookie(name, value, expirationInDays) {
                const date = new Date();
                date.setTime(date.getTime() + (expirationInDays * 24 * 60 * 60 * 1000));
                document.cookie = name + '=' + value
                    + ';expires=' + date.toUTCString()
                    + ';domain=' + COOKIE_DOMAIN
                    + ';path=/';
            }

            if (cookieExists('laravel_cookie_consent')) {
                hideCookieDialog();
            }

            const buttons = document.getElementsByClassName('js-cookie-consent-agree');

            for (let i = 0; i < buttons.length; ++i) {
                buttons[i].addEventListener('click', consentWithCookies);
            }

            return {
                consentWithCookies: consentWithCookies,
                hideCookieDialog: hideCookieDialog
            };
        })();
    </script>

    </section>

<footer class="footer bg-dark pt-3">

    <div class="container d-print-none">
            <div class="row">
                <div class="col-md-4">
                    <img class="d-block mw-100 pl-1 pl-md-4 pb-4 pt-3"
                                                  src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
                         data-src="https://spiewnik.wywrota.pl/songbook/images/world.png"
                                             >
                </div>

                <div class="col-md-8 col-xl-6 p-3 my-auto " style="color: #cccccc">


                    <h1 class="text-white h3">Największy polski Wiki-Śpiewnik tworzony przez gitarzystów dla gitarzystów!</h1>

                    Pierwsza wersja Śpiewnika Wywroty pojawiła się w Internecie ponad 20 lat temu.
                    Dzisiaj prezentujemy nową wersję witryny wzbogaconą o&nbsp;nowoczesny system Wiki.
                    Teraz możesz dodawać nowe piosenki z chwytami na gitarę lub ukulele i tworzyć swój własny śpiewnik!

                    <div class="mt-2">
                        <a href="https://moja.wywrota.pl/register" rel="nofollow" class="btn btn-primary text-white">Zarejestruj się!</a>
                        <a href="https://moja.wywrota.pl/login" rel="nofollow" class="btn btn-outline-secondary bg-white text-dark">Zaloguj się</a>
                    </div>


                </div>
            </div>
        </div>



    <div class="container py-3">
        

        <div class="text-muted txt-sm  d-print-none">
            <a href="https://www.wywrota.pl"><i class="fa fa-star"></i> Wywrota.pl</a>
            &middot;
            <a href="https://spiewnik.wywrota.pl/wskazowki">Zasady Kontrybucji</a>
            &middot;
            <a href="https://spiewnik.wywrota.pl/polityka-prywatnosci">Polityka Prywatności</a>
            &middot;
            <a href="https://spiewnik.wywrota.pl/feedback">Kontakt</a>

        </div>

        <div class="text-muted txt-sm mt-2 ">
            Utwory są własnością ich twórców i mogą być chronione prawem autorskim.<br>

            Śpiewnik Wywroty            &copy; Lune Systems B.V. <span class="text-muted d-none d-sm-inline"> 2020</span><br>
		</div>

        <div class="text-muted txt-sm mt-2 d-print-none">
            Wywrota.pl made with <i class="fa fa-heart text-primary"></i> in Amsterdam
        </div>

    </div>

</footer>
<!-- Chord Modal -->
<div class="modal fade" id="chordModal" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title text-center"></h1>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body mx-3">
            </div>
        </div>
    </div>
</div>


<script src="https://spiewnik.wywrota.pl/songbook/js/songbook-app.js?id=ecd98dacce1026c74d89"></script>
<script src="https://spiewnik.wywrota.pl/songbook/js/songbook.js?id=e111530c3f60cc290558"></script>


    <script src="https://spiewnik.wywrota.pl/songbook/js/songbook-chords.js?id=b87e3e4eeea4469edaa8" async defer></script>
    <script type="text/javascript">
            window._taboola = window._taboola || [];
            _taboola.push({flush: true});
        </script>

    

<script>
    $(function () {
        $("#rateYo").rateYo({
            rating: 0,
            readOnly: 0,
            fullStar: true,
            starWidth: "25px",
            spacing: "5px",
            normalFill: "#6c757d",
            ratedFill: "#db261e",
            onChange: function (rating, rateYoInstance) {
                switch (rating) {
                    case 1:
                        $(this).next().text('Fatalny');
                        break;
                    case 2:
                        $(this).next().text('Słaby');
                        break;
                    case 3:
                        $(this).next().text('Niczego sobie');
                        break;
                    case 4:
                        $(this).next().text('Dobry');
                        break;
                    case 5:
                        $(this).next().text('Doskonały');
                        break;
                    default:
                        $(this).next().text('Oceń to opracowanie');
                        break;
                }

            }
        });

        $("#rateYo").click(function () {
            var rating = $("#rateYo").rateYo().rateYo("rating");
            let data = {
                content_id: "117568",
                content_type: "7",
                rating: rating
            };

            $.post("https://spiewnik.wywrota.pl/api/rating", data)
                .done(function (response) {
                    $('#ratingHelpBlock').text(response.success);
                    $("#rateYo").rateYo("option", "readOnly", true);
                }).fail(function (response) {
                $('#ratingErrorBlock').text(response.responseJSON.error);
            });

        });
    });



    window.WYWROTA.instrument = "guitar";


</script>

<script>
    </script>

</body>
</html>
`);
});

var server = app.listen(3001, function() {});
