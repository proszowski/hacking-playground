# The Hacking Playground

## What is it?
This is a project written for Bachelor Thesis purpose.
Bachelor Thesis can be found here (only Polish version): https://drive.google.com/file/d/1f7EvLz7QDNwRWlpqdM7JpZKLhyquZmSE/view?usp=sharing

Best way to see its functionality is by watching videos: https://drive.google.com/drive/folders/1alKd1hZdF32wd0seeuFZOVlr6DKjImik?usp=sharing
or/and watching presentation (Polish language): https://docs.google.com/presentation/d/1nATWXmEdVSbQtD6pDw-BTtqSbHiM70mmNtR_JYGYdlk/edit?usp=sharing

It consists of three comopnents:
- The Hacking Playground API (this repository)
- The Hacking Playground Front End (this repository, only because I knew I'm gonna be single developer so it was little bit more comfortable)
- The Hacking Playground Config Server (https://bitbucket.org/proszowski/hacking-playground-config-server/src/master/)

TLDR; (or I don't know Polish, please gimme some more context):
Title of my Bachelor Thesis work is "Security of web services - vulnerabilities and the ways to eliminate them". The goal of this work was to create a tool which would help in creating safer applications by educating developers about security vulnerabilities and deliver some extra materials for penetration testers. That's why I built a [CTF]: https://blogs.cisco.com/perspectives/cyber-security-capture-the-flag-ctf-what-is-it platform based on Docker technology. It's different than other CTF platforms because games can be deployed incredibly fast, addtionally each game is independet for each player (changes in one CTF like erasing all data in database do not have any impact for other users).

## Sounds good! Why isn't it on production yet?
I started it with the thought about writing PoC which would prove that building something like that is possible. Now I know that it is. However, in current shape it won't be able to work properly for bigger amount of users. Now I know that the best way would be to have Kubernetes cluster with smart auto scaling groups instead of doing everything on single machine. Additionally a lot of work would need to be implemented like monitoring docker instances, policy of retention, limit of adding machines, better ranking, etc. Probably if I would like to create something like this I would start from scratch, but anyway, during the project I learnt some useful things so I don't think that I wasted time ;)
